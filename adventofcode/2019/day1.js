
const fs = require('fs')

fs.readFile('day1.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
  solve2(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    var total = 0;

    lines.forEach((line) => {
      var moduleWeight  = parseInt(line); 
      total += Math.floor(moduleWeight / 3) - 2;
    });

    console.log(total);
}

function solve2 (data) {

  const lines = data.split(/\r?\n/);
  var total = 0;

  lines.forEach((line) => {
    var moduleWeight  = parseInt(line); 
    var fuel = 0;
    var req = moduleWeight;
    while (true) {
      req = Math.floor(req / 3) - 2;
      if (req < 0) {
        break;
      } else {
        fuel += req;
      }
    }
    total += fuel;
  });

  console.log(total);
}

