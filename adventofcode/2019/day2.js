
const fs = require('fs')

fs.readFile('day2.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
  solve2(data);
});

function solve (data) {

  console.log("1st part");
  // load
  const lines = data.split(",");
  const program = [];
  lines.forEach(i => {
    program.push(parseInt(i));
  });
  
  // restore gravity
  program[1] = 12;
  program[2] = 2;

  // run!
  const result = run(program);
  console.log(result);
}

function solve2(data) {

  console.log("2nd part");
  // load inital state
  const lines = data.split(",");
  const memory = [];
  lines.forEach(i => {
    memory.push(parseInt(i));
  });
  
  for (var noun=0 ; noun <=99; noun++) {
    for (var verb=0; verb <=99; verb++) {
      // initialise memory
      const prog = structuredClone(memory);
      prog[1] = noun;
      prog[2] = verb;

      const res = run(prog);
      if (res == 19690720) {
        console.log(100*noun + verb);
        return;
      }
    }
  }
}

function run(prog) {
  var opIndex = 0;
  while (true) {
    const op = prog[opIndex];
    const op1 = prog[opIndex+1];
    const op2 = prog[opIndex+2];
    const dest = prog[opIndex+3];

    switch(op) {
      case 1:
        prog[dest] = prog[op1] + prog[op2];
        break;
      case 2:
        prog[dest] = prog[op1] * prog[op2];
        break;
      case 99:
        break;
      default:
        console.log("Wrong OpCode");
        return 0;
    }
    if (op == 99) {
      return prog[0];
    } else {
      opIndex += 4;
    }
    
  }
}


