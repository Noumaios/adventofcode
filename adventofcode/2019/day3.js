
const fs = require('fs')

fs.readFile('day3.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
  solve2(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    
    const wire1 = getPath(lines[0].split(","),[0,0,0]);
    const wire2 = getPath(lines[1].split(","),[0,0,0]);

    // console.log(wire1);
    // console.log(wire2);

    const crossings = getCrossing(wire1,wire2);
    console.log(crossings);
    console.log("Part 1 : " + minManathanDistance([0,0],crossings));

    console.log(minSignalDistance(crossings));


}

function solve2 (data) {

}
// return an array of all visited nodes from a starting point
function getPath(moves,start) {
  const arr = [];
  arr.push(start);
  console.log("Total moves " + moves);
  moves.forEach(move => {
    const direction = move.substr(0,1);
    const steps = move.substr(1);
    const moveStart = arr[arr.length-1];

    console.log("Moving " + direction + " by " + steps + " from " + moveStart);
    switch (direction) {
      case "U":
      case "D":
        for (var i=1;i<=steps; i++) {
          arr.push([moveStart[0],direction == "D"?moveStart[1]-i:moveStart[1]+i,moveStart[2]+i]);
        }
        break;
      case "R":
      case "L":
        for (var i=1;i<=steps; i++) {
          arr.push([direction == "L"?moveStart[0]-i:moveStart[0]+i,moveStart[1],moveStart[2]+i]);
        }
        break;  
    }
  });
  return arr;
}

function getCrossing(arr1,arr2) {

  console.log("Get crossing points ");
  console.log(" Arr 1 length " + arr1.length);
  console.log(" Arr 2 length " + arr2.length);
  const crossing = [];
  for (var i=1;i<arr1.length;i++) {
    for (var j=1;j<arr2.length;j++) {
      if (arr1[i][0] == arr2[j][0] && arr1[i][1] == arr2[j][1]) {
        crossing.push([arr1[i][0],arr1[i][1],arr1[i][2]+arr2[j][2]]);
      }
    } 
  }

  return crossing;
}

function minManathanDistance(start,arr) {

  var distances = [];
  arr.forEach(pt => {
    distances.push(Math.abs(pt[0])+Math.abs(pt[1]));
  });
  return distances.sort(function(a, b){return a - b})[0];
}

function minSignalDistance(arr) {

  
  arr.sort(function(a,b){ return a[2]-b[2]});

  return arr.sort(function(a,b){ return a[2]-b[2]})[0];
}