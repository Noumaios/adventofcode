
const fs = require('fs')

fs.readFile('day4.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
  solve2(data);
});

function solve (data) {

    const range = data.split("-");
    
    const minArr = range[0].split("").map(x => parseInt(x));
    const maxArr = range[1].split("").map(x => parseInt(x));

    const min = parseInt(range[0]);
    const max = parseInt(range[1]);

    console.log(min);
    console.log(max);

    var count =0;
    // brute force
    for (var i = min; i<=max;i++) {
        if (validPassword(i)) {
            console.log(i)
            count++;
        }
    }

    console.log("Part 1: " + count);
}

function solve2 (data) {

}

function validPassword(pass) {

    const passArr = pass.toString().split("").map(x => parseInt(x));
    var adj = false;
    for (var i=0;i<5;i++) {

        if (passArr[i] == passArr[i+1] && pass.toString().indexOf(""+passArr[i]+passArr[i]+passArr[i]) == -1) {
            adj = true;
        }
        if (passArr[i+1] < passArr[i]) {
            return false;
        }
    }
    return adj;

}
