
    const fs = require('fs')

    if (process.argv.length < 3) {
        console.log("Error: Wrong number of arguments");
        console.log("Usage: node <file.js> <input>");
        process.exit(1);
      }
      
    var input = process.argv[2];

      
    fs.readFile('day5.txt', 'utf8' , (err, data) => {
        if (err) {
            console.error(err)
            return
        }
        solve(data);    
    });

    function solve (data) {

        // load
        const lines = data.split(",");
        const program = [];
        lines.forEach(i => {
            program.push(parseInt(i));
         });

        // run!
        run(program,parseInt(input));
    }

    function run(prog,input) {
        var opIndex = 0;

        while (true && opIndex < prog.length) {
            var rawOp = prog[opIndex];
            
            // opCode aer the last 2 digits
            var op = rawOp%100;

            var op1Mode = Math.floor(rawOp/100)%10;
            var op2Mode = Math.floor(rawOp/1000)%10;
            var op3Mode = Math.floor(rawOp/1000)%10;
            
            const op1 = getOperand(prog,op,opIndex+1,op1Mode);
            const op2 = getOperand(prog,op,opIndex+2,op2Mode);
            const op3 = prog[opIndex+3];
            
            switch(op) {
            case 1:
                prog[op3] = op1 + op2;
                opIndex += 4;
                break;
            case 2:
                prog[op3] = op1 * op2;
                opIndex += 4;
                break;
            case 3:
                // read
                prog[op1] = input;
                opIndex += 2;
                break;
            case 4:
                // write
                console.log("Output : " + op1);
                opIndex += 2; 
                break;
            case 5:
                // jump if false
                if (op1 != 0) {
                    opIndex=op2
                } else {
                    opIndex += 3;
                } 
                break;
            case 6:
                // jump if false
                if (op1 == 0) {
                    opIndex=op2
                } else {
                    opIndex += 3;
                } 
                break;
            case 7:
                // less than
                if (op1 < op2) {
                    prog[op3] = 1;
                } else {
                    prog[op3] = 0;
                } 
                opIndex += 4;
                break;
            case 8:
                // equal
                if (op1 == op2) {
                    prog[op3] = 1;
                } else {
                    prog[op3] = 0;
                } 
                opIndex += 4;
                break;
            case 99:
                console.log("Halt");
                return;
            default:
                console.log("Wrong OpCode");
                return 0;
            }
        }
    }

    function getOperand(prog,op,address,mode) {
        if (mode == 0 && op !=3) {
            return prog[prog[address]];
        } else {
            return prog[address];
        }
    }
