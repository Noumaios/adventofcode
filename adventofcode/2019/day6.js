
const fs = require('fs')

const orbits = {};


fs.readFile('day6.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);

    lines.forEach(line => {
        const orbit = line.split(")");

        // add it to tree
        if (!orbits[orbit[0]]) {
            orbits[orbit[0]] = {
                next : [],
                prev: []
            }
        }
        if (!orbits[orbit[1]]) {
            orbits[orbit[1]] = {
                next : [],
                prev: []
            }
        
        }
        orbits[orbit[1]].prev.push(orbit[0]);
        orbits[orbit[0]].next.push(orbit[1]);
    });

    solve(data);
    solve2(data);
});

function solve (data) {

    console.log("Checksum is " + checksum('COM',0));

}

function checksum(planet,indirect) {
    if (orbits[planet].next.length == 0) {
        return indirect;
    } else {
        var sum = 0;
        orbits[planet].next.forEach (p => {
            sum += checksum(p,indirect+1)
        });
        return indirect + sum;
    }
}

function solve2 (data) {

    const queue = [];
    const visited = {};

    queue.push({planet :'YOU',steps:0,path:[]});

    while (queue.length > 0) {
        
        const node = queue.shift();

        // reached destination
        if (node.planet == "SAN") {
            console.log("Reached destination in " + node.steps);
            console.log("Orbital transfers = "+ (node.steps-2));
            break;
        }
        // if visited, check the steps
        if (visited[node.planet] && visited[node.planet].steps <= node.steps) {
            continue;
        }
        visited[node.planet] = node.steps;
        
        // add next steps to the queue
        orbits[node.planet].next.forEach(n => {
            if (!node.path.includes(n)) {
                queue.push({planet : n,steps :node.steps+1, path : [...node.path,node.planet]});
            }
        });
        orbits[node.planet].prev.forEach(n => {
            if (!node.path.includes(n)) {
                queue.push({planet : n,steps :node.steps+1,path:[...node.path,node.planet]});
            }
        });
    }
}
