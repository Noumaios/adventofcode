
const fs = require('fs')

fs.readFile('day1.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
  solve2(data);
});


function solve (data) {

    const lines = data.split(/\r?\n/);

    for (i=0;i<=lines.length/2; i++) {
      for (j=i+1; j< lines.length; j++) { 
        if (parseInt(lines[i])+parseInt(lines[j]) == 2020) {          
          console.log(lines[i] +"+"+lines[j] + "=2020");
          console.log(parseInt(lines[i])*parseInt(lines[j]))
        }
      }
    }
}
function solve2 (data) {

  const lines = data.split(/\r?\n/);

  for (i=0;i<=lines.length/2; i++) {
    for (j=i+1; j< lines.length-1; j++) {
      for (k=j+1;k< lines.length;k++) {
        if (parseInt(lines[i])+parseInt(lines[j]) +parseInt(lines[k])== 2020) {  
          console.log(lines[i] +"+"+lines[j] + "+"+lines[k]+ "=2020");
          console.log(parseInt(lines[i])*parseInt(lines[j])*parseInt(lines[k]))
        }
      }
    }
  }
}
