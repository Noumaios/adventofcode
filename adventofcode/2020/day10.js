
const fs = require('fs');
const map =[];
const newMap = [];

fs.readFile('day10.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  const lines = data.split(/\r?\n/);

  lines.forEach(line => {
      map.push(line.split(""));
  });
  solve(map);
  solve2(map);
});


function solve (map) {
    console.log("");
    //print(map);
    var newMap = [];
    var change = false;
    for (var y=0; y<map.length;y++) {
        var newLine = [];
        for (var x=0; x<map[0].length;x++) {
            var newState = "";
            if (map[y][x] == "L" && adjacentSeats(map,x,y) == 0) {
                newState = "#";
                change =true;
            } else if (map[y][x] == "#" && adjacentSeats(map,x,y) >= 4) {
                newState = "L";
                change = true;
            } else {
                newState = map[y][x];
            }
            newLine.push(newState);
        }
        newMap.push(newLine);
    }

    if (change) {
        solve(newMap);
    } else {
        // count 
        console.log("Done!");
        console.log(countSeats(newMap));
        return;
    }
}
function solve2 (map) {

} 



function adjacentSeats(map,x,y) {
    var adj =0;
    for (var i = Math.max(0,y-1); i<=Math.min(y+1,map.length-1);i++) {
        for (var j = Math.max(0,x-1); j<=Math.min(x+1,map[0].length-1);j++) {
            if (map[i][j] == "#" && !(i == y && j == x) ) {
                adj++;
            }
        }
    }
    return adj;
}

function viewableSeats(map,x,y) {
    var adj =0;
    for (var i = y; i<map.length-1 && y-i> 0;i++) {
        for (var j = x; j<=Math.min(x+1,map[0].length-1);j++) {
            if (map[i][j] == "#" && !(i == y && j == x) ) {
                adj++;
            }
        }
    }
    return adj;
}


function countSeats(map) {
    var seatCount = 0;
    map.forEach(line => {
        line.forEach(seat => {
            if (seat == "#") {
                seatCount++;
            }
        });
    });
    return seatCount;
}

function print(map) {
    map.forEach(line => {
        var s = "";
        line.forEach(seat => {
            s+=seat;
        });
        console.log(s);
    });
}