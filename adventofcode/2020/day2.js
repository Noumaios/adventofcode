
const fs = require('fs')

fs.readFile('day2.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
  solve2(data);
});


function solve (data) {
    const lines = data.split(/\r?\n/);
    var countCorrect = 0;

    lines.forEach(line => {
        if (checkPassword(line)) {
            countCorrect++;
        };
    });

    console.log(countCorrect);

    
}
function solve2 (data) {
    const lines = data.split(/\r?\n/);
    var countCorrect = 0;

    lines.forEach(line => {
        if (checkPassword2(line)) {
            countCorrect++;
        };
    });

    console.log(countCorrect);
 
}

function checkPassword(line) {
    
    const parsed = line.split(/: /);
    const password = parsed[1];
    const policy = parsed[0].split(/ /);
    const char = policy[1];
    const occurences = policy[0].split(/-/);
    const min = parseInt(occurences[0]);
    const max = parseInt(occurences[1]);
    
    if (min <= password.split(char).length -1 && max >= password.split(char).length -1) {
        console.log("Password is correct");
        return true;
    } else {
        console.log("Password is NOT correct");
        return false;
    }
}

function checkPassword2(line) {
    
    const parsed = line.split(/: /);
    const password = parsed[1];
    const policy = parsed[0].split(/ /);
    const char = policy[1];
    const occurences = policy[0].split(/-/);
    const one = parseInt(occurences[0])-1;
    const two = parseInt(occurences[1])-1;
    
    if ((password[one] == char || password[two] == char) &&
        !(password[one] == char && password[two] == char)) {
        console.log("Password is correct");
        return true;
    } else {
        console.log("Password is NOT correct");
        return false;
    }
}
