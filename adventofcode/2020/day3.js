
const fs = require('fs')

fs.readFile('day3.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  const lines = data.split(/\r?\n/);
  const map = [];

  lines.forEach(line => {
      map.push(line.split(""));
  });
  solve(map);
  solve2(map);
});


function solve (map) {

    var count = next(map,3,1,3,1);
    console.log(count);
}
function solve2 (map) {

    var count = 1;
    count*= next(map,1,1,1,1);
    count*= next(map,3,1,3,1);
    count*= next(map,5,1,5,1);
    count*= next(map,7,1,7,1);
    count*= next(map,1,2,1,2);

    console.log(count);
}   

function next(map,x,y,movex,movey) {
    var extendedx = x % map[0].length;
    
    if (y >= map.length) {
        // the end
        return 0;
    }

    if (map[y][extendedx] == "#") {
        return 1 + next(map,extendedx+movex,y+movey,movex,movey);
    } else {
        return next(map,extendedx+movex,y+movey,movex,movey);
    }
}