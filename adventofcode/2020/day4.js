
const fs = require('fs')

fs.readFile('day4.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  const lines = data.split(/\r?\n/);
  const passports = [];
  var passport = {};

  lines.forEach(line => {
    if (line == "") {
        passports.push(passport);
        passport = {};
    } else {
        const raw = line.split(" ");
        raw.forEach(keyValue => {
            const pair = keyValue.split(":");
            passport[pair[0]] = pair[1];
        });
    }
  });
  passports.push(passport);

  solve(passports);
  solve2(passports);
});


function solve (data) {
    var count = 0;

    data.forEach(pass => {
        console.log(pass);
        console.log(pass["cid"]);   
        if (Object.keys(pass).length == 7 && pass["cid"] == null) {
           // jsut missing cid
           count++;
        } else {
            if (Object.keys(pass).length == 8) {
                count++;
            }
        }
    });
    console.log(count);
}
function solve2 (data) {

    for (var i=0;i<data.length; i++) {
        const pass=data[i];
        console.log(pass);

        // not enough fields
        if (Object.keys(pass).length < 7) {
            continue;
        }
        // not missing cid
        if (Object.keys(pass).length == 7 && pass["cid"] != null) {
            // jsut missing cid
            continue;
        }


        //validate field
        if (!checkNumber(pass['byr',1920,2002])) {
            continue;
        }
        if (!checkNumber(pass['iyr',2010,2020])) {
            continue;
        }
        if (!checkNumber(pass['eyr',2020,2030])) {
            continue;
        }
        if (!checkNumber(pass['hgt',2010,2020])) {
            continue;
        }
        //boring....
    }
    console.log(count);
}   

function checkNumber(value,min,max) {
    const n = parseInt(value);
    if (n <=max && n >= min) {
        return true;
    } else {
        return false;
    }
}