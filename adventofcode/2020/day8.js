
const fs = require('fs')


fs.readFile('day8.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  const lines = data.split(/\r?\n/);
  const code = [];

  lines.forEach(line => {
    var instr = line.split(" ");
    var l = {};
    l['opCode'] = instr[0];
    l['sign'] = instr[1].substring(0,1);
    l['value'] = parseInt(instr[1].substring(1));
    code.push(l);
  });

  solve(code);
  solve2(code);
});


function solve (code) {
    
    var acc = 0;
    var current = 0;
    var run = [];

    while (true) {
        console.log(code[current]);

        if (run.indexOf(current) != -1) {
            break;
        }
        run.push(current);

        switch (code[current]['opCode']) {
            case 'acc' :
                if (code[current]['sign'] == "+") {
                    acc+=code[current]['value']; 
                } else {
                    acc-=code[current]['value']; 
                }
                current++
                break;
            case 'nop' :
                current++;
                break;
            case 'jmp' :
                if (code[current]['sign'] == "+") {
                    current+=code[current]['value']; 
                } else {
                    current-=code[current]['value']; 
                }
        }
    }
    console.log(code);
    console.log(acc);
}

function solve2 (code) {

    for (var i =0; i<code.length;i++) {
        line = code[i];

        if (line['opCode'] == 'nop') {
            line['opCode'] = 'jmp';
            var accumulator = runCode(code);
            if (accumulator != -1) {
                console.log(accumulator);
                return;
            } 
            line['opCode'] = 'nop';
        }
        if (line['opCode'] == 'jmp') {
            line['opCode'] = 'nop';
            var accumulator = runCode(code);
            if (accumulator != -1) {
                console.log(accumulator);
                return;
            } 
            line['opCode'] = 'jmp';
        }
    }
}  

function runCode(code) {
    var acc = 0;
    var current = 0;
    var run = [];

    while (true) {

        if (run.indexOf(current) != -1) {
            return -1;
        }
        if (current == code.length) {
            return acc;
        }
        run.push(current);

        switch (code[current]['opCode']) {
            case 'acc' :
                if (code[current]['sign'] == "+") {
                    acc+=code[current]['value']; 
                } else {
                    acc-=code[current]['value']; 
                }
                current++
                break;
            case 'nop' :
                current++;
                break;
            case 'jmp' :
                if (code[current]['sign'] == "+") {
                    current+=code[current]['value']; 
                } else {
                    current-=code[current]['value']; 
                }
        }
    }
}


