
const fs = require('fs')

fs.readFile('day1.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    var inc = 0;
    var i = 0;
    var prev = -1;
    
    lines.forEach((line) => {
        if (prev != -1) {
          if (parseInt(line)> parseIn(prev)) {
            inc++;
          }
        }
        prev = line;
        i++
    });
    console.log("checked " + i + " lines");
    console.log(inc);
}

function solve2 (data) {

  const lines = data.split(/\r?\n/);
  var inc = 0;
  var i = 1;
  var sum = 0;
  var prev =0;
  
  for (var i=1; i<lines.length-2; i++) {
    prev = parseInt(lines[i-1]) + parseInt(lines[i]) + parseInt(lines[i+1]);
    sum = parseInt(lines[i]) + parseInt(lines[i+1]) + parseInt(lines[i+2]);

    if (sum > prev) {
      inc++;
      console.log(prev + " Increase");
    } else {
      console.log(prev + " Decrease");
    }
  }
  console.log(inc);
}