
const fs = require('fs');

const startDelimiters = {
  "(" : "}",
  "[" : "]",
  "{" : "}",
  "<" : ">"
}

fs.readFile('day10.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    var errors=0;
    var i = 1;
    var illegalChar = [];
    var completions = [];

    lines.forEach((line) => {
      var comp = [];
      if (!syntaxCheck(line,i,illegalChar,comp)) {
        errors++;
      } else {
        completions.push(comp);
      };
      i++;

    });

    console.log("result " + errors);
    console.log(illegalChar);

    var score = 0;
    illegalChar.forEach((c) => {
      switch (c) {
        case "}":
          score += 1197;
          break;
        case ")" :
          score += 3;
          break;
        case "]" : 
          score += 57;
          break;
        case ">":
          score += 25137;
      }  
    });

    console.log("Score " + score );

    console.log("Completions ");
    console.log(completions);

    // score calculation
    var scores = [];
    completions.forEach((comp) => {
      var lineScore = 0;
      for (var i=0; i<comp.length; i++) {
        switch (comp[i]) {
          case ")" :
            s = 1
            break;
          case "]":
            s = 2;
            break;
          case "}":
            s = 3;
            break;
          case ">":
            s = 4;
            break;
        }
        lineScore =+ lineScore*5 + s;
      }
      scores.push(lineScore);
    });

    console.log(scores);

    var sortedScores = scores.sort(function(a, b) {
      return a - b;
    });

    console.log(sortedScores);
    console.log(Math.floor(scores.length/2));
    console.log(sortedScores[Math.floor(scores.length/2)]);
}

function syntaxCheck (line,k,illegalChar,completion) {

  var l = line.split('');
  var currentBlocks = [];

  for (var i=0; i < l.length; i++) {
    if (currentBlocks.length == 0) {
      // line start of all previous blocks closed
      currentBlocks.push(l[i]);
      //console.log(currentBlocks);
    } else {
      if (startDelimiters[l[i]]) {
        // new block
        currentBlocks.push(l[i]);
      } else {
        // closing block
        if (matchStart(currentBlocks[currentBlocks.length -1],l[i],k,i)) {
          currentBlocks.pop();
        } else {
          illegalChar.push(l[i]);
          return false;
        }
      }
    }
  }
 
  for (var i = currentBlocks.length -1; i >=0; i-- ) {
    var open = currentBlocks[i];
    switch (open) {
      case "{" :
        completion.push("}");
        break;
      case "[":
        completion.push("]");
        break;
      case "(":
        completion.push(")");
        break;
      case "<":
        completion.push(">");
        break;
    }
  }
  // now complete the lines
  


  return true;
}

function matchStart(start,char,k,pos) {

  var open = "";

  switch (char) {
    case "}" :
      open = "{";
      break;
    case "]":
      open = "[";
      break;
    case ")":
      open = "(";
      break;
    case ">":
      open = "<";
      break;
  }

  if (open == start) {
    return true;
  } else {
    console.log("Line " + k + ": Expected " + startDelimiters[start] + " found " + char + " at position " + pos);
    return false;
  }
}
