
const fs = require('fs');

fs.readFile('day11.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

function solve (data) {

    var octopus = parseData(data);
    var maxSteps = 1000;
    var flashes = 0;

    console.log("Inital Stage :");
    print(octopus);

    for (var step=1; step <= maxSteps; step++) {
      // add 1 to all
      addOne(octopus);
      
      explodeOctopus(octopus); 

      if (resetFlashedOctopus(octopus) == 100) {
        console.log("SYNC--------");
        break;
      }
      flashes += resetFlashedOctopus(octopus);
      
      console.log();
      console.log("After " + step + " steps");
      print(octopus);

      
    }




    // }

    console.log("result " + flashes);
    
}






function addOneAround(lines,line,col) {
 
   for (var i=Math.max(line-1,0);i<=Math.min(line+1,lines.length-1);i++) {
     for (var j=Math.max(col-1,0);j<=Math.min(col+1,lines[i].length-1); j++) { 
       //console.log(i,j);
       if (i ==  line && j == col) {
         continue;
       }
       lines[i][j]++;
     }
   }  
   return true;
  
 }

 function parseData(data) {

  const lines = data.split(/\r?\n/);
  var octo = [];
  lines.forEach((line) => {
    var octoLine = [];
    for (var i=0; i< line.length; i++) {
      octoLine.push(parseInt(line[i]));
    }
    octo.push(octoLine);
  });

  return octo;
 }

 function print(octopus) {

  for (var i=0; i< octopus.length; i++) {
    console.log("" + octopus[i]);
  } 

 }

 function addOne(arr) {
   
    for (var i = 0; i < arr.length; i++) {
      for (var j = 0; j < arr[i].length; j++) {
        arr[i][j] += 1;
      }
    }

 }

 function explodeOctopus(arr) {

    var flash = false;

    for (var i = 0; i < arr.length; i++) {
      for (var j = 0; j < arr[i].length; j++) {
        if (arr[i][j] > 9 && arr[i][j] < 100) {
          arr[i][j] = 100; // flash!
          addOneAround(arr,i,j);
          flash = true;
        }
      }
    }
    if (flash) {
      explodeOctopus(arr);
    } else {
      return;
    }
 }

 function resetFlashedOctopus(arr) {

  var flashes = 0;

  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < arr[i].length; j++) {
      if (arr[i][j] >= 100) {
        arr[i][j] = 0; 
        flashes++;
      }
    }
  }

  return flashes;
}