
const fs = require('fs');

fs.readFile('day12.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

var tree = {};
var paths = [];
var count = 0;

function solve (data) {

    parseData(data);
    printTree();

    crunchPath("start","");
    

    
    console.log(paths);
    console.log("result " + count);
}




function crunchPath(point,currentPath) {


  if (point.toLowerCase() == point && currentPath.indexOf(point) != -1 && tooManySmallCaves(currentPath)) {
    // been there already
    return; 
  }

    
  if (tree[point].descendants.lenght == 0) {
    // found a dead end
    return;
  }

  currentPath += point;
  if (point == "end") {
    // found a valid path
    paths.push(currentPath);
    count++;
    return;
  }

  tree[point].descendants.forEach((descendant) => {
  
    crunchPath(descendant.value,currentPath);
  });

}



 function parseData(data) {

  const lines = data.split(/\r?\n/);
  lines.forEach((line) => {
    var startEnd = line.split("-");
    
    // manage the order issue with start or end
    if (startEnd[1] == "start" || startEnd[0] == "end") {
      addToTree(startEnd[1],startEnd[0]);
    } else if (startEnd[0] == "start" || startEnd[1] == "end") {
      addToTree(startEnd[0],startEnd[1]);
    } else {
      addToTree(startEnd[0],startEnd[1]);
      addToTree(startEnd[1],startEnd[0]);
    }
  });
 }

function addToTree(a,b) {

  console.log("Adding " + a + "," + b);

  if (!tree[a]) {
    tree[a] = new TreeNode(a);
  }
  if (!tree[b]) {

    tree[b] = new TreeNode(b);
  }

  tree[a].descendants.push(tree[b]);  
}

function printTree() {

  for (var k in tree) {
    console.log ("Key " + k + " : ");
    console.log(tree[k]);
  }
}

function tooManySmallCaves(path) {
  // count the number of small caves 
  // allow for 1 small cave visited twice
  for (value in tree) {
    if (value == value.toLowerCase()) {
      if (path.split(value).length > 2) {
        return true;
      }
    }
  }
  return false;
}

 class TreeNode {
  constructor(value) {
    this.value = value;
    this.descendants = [];
  }
}