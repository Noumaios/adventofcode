
const fs = require('fs');

fs.readFile('day13.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});


function solve (data) {

    var input = parseData(data);
    var space = [];
    var maxX,maxY=0;

    // fill the space
    input.points.forEach((point) => {
      if (!space[point.y]) {
        space[point.y] = [];
      }
      space[point.y][point.x] = "1"
      if (point.x >= maxX) {
        maxX = point.x;
      }
      if (point.y >= maxY) {
        maxY = point.y;
      }
    });

    // complete the space
    completeSpace(space,maxX,maxY);
    //printSpace(space);

//    fold 
    var foldCount = input.folds.length;

    for (var i=0; i< foldCount; i++) {
      var f= input.folds[i].split("=");
      foldSpace(f[0],parseInt(f[1]),space);
      printSpace(space);
      console.log();
    }

    console.log();

    var result = printSpace(space);
    
    

    console.log("result " + result);
}


function foldSpace(axis,position,space) {
  var newSpace =[];
  if (axis == "y") {
    for (var i=1; i<=position && i< space.length-position ; i++){
      for (var j=0; j<space[i].length;j++) {
        space[position-i][j] = space[position-i][j]||space[position+i][j]?1:0;
      }
    }
    space.length = position;
  } else {
    for (var i=0; i< space.length ; i++){
      for (var j=1; j<=position && j < space[i].length-position;j++) {
        space[i][position-j] = space[i][position - j]||space[i][position + j]?1:0;
      }
      space[i].length = position;
    }
  }

}

function completeSpace(space,maxX, maxY) {

  for (var i=0; i<=maxY; i++) {
    if (!space[i]) {
      space[i]=[];
    }
    for (var j=0; j<=maxY; j++) {
      if (!space[i][j]) {
        space[i][j] = 0;
      }
    }
  }
}

function printSpace(s) {

  var count = 0;

  for (var i =0; i < s.length; i++) {
    if (!s[i]) {
      s[i] = [];
    }
    var line = "";
    for (var j=0; j<s[i].length;j++) {
      if (!s[i][j]) {
        line += ".";
      } else {
        count++;
        line += "#";
      }
    }
    console.log(line);
  }

  return count;
}

 function parseData(data) {

  var input = { 
    "points" : [],
    "folds" : [],
  }

  const lines = data.split(/\r?\n/);

  lines.forEach((line) => {
    
    if (line != "") {
      if (line.startsWith("fold",0)) {
        input.folds.push(line.split(" ")[2]);
      } else {
        var p = line.split(",");
        var pts = {
          "x":parseInt(p[0]),
          "y":parseInt(p[1])
        }
        input.points.push(pts);
      }
    } 
  });

  return input;
 }



