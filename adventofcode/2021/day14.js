
const fs = require('fs');
const { isNumberObject } = require('util/types');
var maxcount = 10;
var elements ={};

fs.readFile('day14.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve2(data);
});

function solve2(data) {

  var input = newParseData(data);
    
  console.log(input);
  console.log("rules size "+input.rules.length);

  var template = input.template.split('');

  var count = 0;
  for (var i=0; i<template.length; i++) {
    if (!elements[template[i]]) {
      elements[template[i]] = 1;
    } else {
      elements[template[i]]++;
    }
  }
  console.log(elements);
  console.log(input.template)
  ;
  var start = new Date();
  var pairs = {};

  // get the template in pairs
  for (var i=0;i<template.length - 1;i++) {
    
    var pair = template[i]+template[i+1];

    if (!pairs[pair]) {
      pairs[pair] = 1;
    } else {
      pairs[pair]++;
    }
  }

  nextStep(pairs,input.rules,1);
      
  var end = new Date();
  console.log("Time taken " + (end.getTime() - start.getTime()));
 
  console.log(elements);

  console.log("Result " + (max(elements) - min(elements)));



}

function min(dict) {
  var min = 0;

  for (var key in dict) {
    if (min == 0) {
      min = dict[key];
    } else {
      min = Math.min(dict[key],min);
    }
  }

  return min;

}

function max(dict) {
  var max = 0;

  for (var key in dict) {
    if (max == 0) {
      max = dict[key];
    } else {
      max = Math.max(dict[key],max);
    }
  }

  return max;

}
function nextStep(pairs,rules,step) {

  if (step > maxcount) {
    return;
  }
  
  console.log("Pairs at step " + step);
  //console.log(pairs);

  var newElements = {};

  for (var key in pairs) {

    var newElement = rules[key];
    if (newElement) {
      if (!elements[newElement]){
        elements[newElement] = parseInt(pairs[key]);
      } else {
        elements[newElement] += parseInt(pairs[key]);
      }
      
      var char = key.split('');
      addTo(newElements,char[0]+newElement,parseInt(pairs[key]));
      addTo(newElements,newElement+char[1],parseInt(pairs[key]));
    }
  }

  nextStep(newElements,rules,step+1);

  //console.log(newElements);

}

function addTo(dict,item,count) {
  if (!dict[item]) {
    dict[item] = count;
  } else {
    dict[item] += count;
  }
}

function solve (data) {

    var input = parseData(data);
    
    console.log(input);
    console.log("rules size "+input.rules.length);

    var template = input.template.split('');
    var count = 0;
    for (var i=0; i<template.length; i++) {
      if (!elements[template[i]]) {
        elements[template[i]] = 1;
      } else {
        elements[template[i]]++;
      }
    }
    console.log(elements);

    var newPolymer = [];
    var start = new Date();
    var totalTime = 0;
    for (var i=0;i<template.length - 1;i++) {
          
      console.log("Processing " + (i+1) + " of " + (template.length-1));

          
      applyTinyRules(template[i]+template[i+1],input.rules,1);
          
      var end = new Date();
      console.log("Time taken " + (end.getTime() - start.getTime()));
      totalTime += end.getTime() - start.getTime()
      start = end;

    }
    console.log("Total processing time " + totalTime);
    
    // while (count < maxcount) {
    //   var newPolymer = [];
    //   for (var i=0;i<template.length - 1;i++) {
    //     newPolymer += applyRules(template[i]+template[i+1],input.rules);
    //     if (i == template.length-2) {
    //       newPolymer += template[i+1];
    //     }
    //   }
    //   template = newPolymer;
    //   count++;
    //   console.log("Polymer after " + count + " steps :"); 
    // }
    //console.log("Polymer " + newPolymer);

    // template = newPolymer;
    // // count the elements
    // var elements ={};
    // for (var i=0; i<template.length; i++) {
    //   if (!elements[template[i]]) {
    //     elements[template[i]] = 1;
    //   } else {
    //     elements[template[i]]++;
    //   }
    // }
    console.log(elements);

    console.log("result " + 0);
}


function applyTinyRules(temp,rules,step) {
  if (step > maxcount) {
    return;
  }

  var newP = rules[temp]; //applyRules(temp,rules).split('');
  if (!elements[newP[1]]){
    elements[newP[1]] = 1;
  } else {
    elements[newP[1]]++;
  }
  for (var i=0;i<newP.length - 1;i++) {
    applyTinyRules(newP[i]+newP[i+1],rules,step+1);
  }
  return;
}

function applyRules(str,rules) {
  
  var merge = "";
  var arr = str.split('');
 
  // find the rule
  //var rule = rules.find(r => r.pattern == str);
  var rule = rules[str];
  if (rule) {
    if (!elements[rule]){
      elements[rule] = 1;
    } else {
      elements[rule]++;
    }
    merge = arr[0]+rule;
  } else {
    merge = arr[0];
  }
  
  return merge + arr[1]
}

 function parseData(data) {

  var input = { 
    "template" : [],
    "rules" : {},
  }

  const lines = data.split(/\r?\n/);

  input.template = lines[0]
  for (var i=2; i < lines.length; i++) {
    var k = lines[i].split(" -> ");
    var rule = {
      "pattern" : k[0],
      "insert" : k[1]
    }
    //input.rules.push(rule);
    var res = k[0].split('');
    input.rules[k[0]] = [res[0], k[1], res[1]];
  }

  return input;
 }

 function newParseData(data) {

  var input = { 
    "template" : [],
    "rules" : {},
  }

  const lines = data.split(/\r?\n/);

  input.template = lines[0]
  for (var i=2; i < lines.length; i++) {
    var k = lines[i].split(" -> ");
    var rule = {
      "pattern" : k[0],
      "insert" : k[1]
    }
    //input.rules.push(rule);
    var res = k[0].split('');
    input.rules[k[0]] = k[1];
  }

  return input;
 }


