
const fs = require('fs');
var maxSteps = 21;
var globalRisk = 999;
var mainPath = [];

fs.readFile('day15.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});


function solve (data) {

    var start = new Date();
    var space = parseData(data);
    var maxX = space.length;
    var maxY = space[0].length;
    var path = [];
  
    
    //console.log(space);

    nextMove(space,path,0,0,maxX,maxY);

    var end = new Date();
    console.log("Processing Time for " + maxSteps + " steps : " + (end.getTime() - start.getTime()) + "ms");
  
    var total = 0;
    for (var i=0; i< path.length; i++) {
      total += path[i];
    }
    console.log("path " + path);
    console.log(mainPath);
    console.log("risk " + total);
}

function nextMove(space,path,x,y,maxX,maxY) {
  //console.log("At (" + x + "," + y+")");
  // only moving to higher x or higher y
  if (x == (maxX -1) && y == (maxY-1)) {
    console.log("Done");
    return;
  } else {
    var nextX, nextY;

    if (x == (maxX -1)) {
      nextX = x;
      nextY = y+1;
    } else if (y == (maxY -1)) {
      nextX = x+1;
      nextY = y;
    } else {
      var rigth = minRisk(space, x+1, y);
      var down = minRisk(space, x, y+1);
      if (rigth <= down) {
        nextX = x+1;
        nextY = y;
      } else {
        nextY=y+1;
        nextX = x;
      }
    }
    path.push(space[nextX][nextY]);
    mainPath.push([x,y]);
    nextMove(space,path,nextX,nextY,maxX,maxY);
  }  
}

// return the paths possible for n steps
function possiblePaths(space,x,y,risk,path,step,maxStep) {
  var maxX = space.length;
  var maxY = space[0].length;

  risk += space[x][y];
  //console.log("risk " + risk + " step " + step);
  if (step == maxStep || (x == (maxX -1) && y == (maxY-1))) {
    
    var weightedRisk = risk / step;
    if (globalRisk > weightedRisk) {
      globalRisk = weightedRisk;
    }
    return;
  }
  var nextX,nextY;
  
  if (x < (maxX -1)) {
    possiblePaths(space,x+1,y,risk,path,step+1,maxStep);
  }
  if (y < (maxY -1)) {
    possiblePaths(space,x,y+1,risk,path,step+1,maxStep);
  }

}

//return the min risk for all path 2 steps ahead
function minRisk(space,x,y) {

  var maxX = space.length;
  var maxY = space[0].length;

  var arr = [999];
  globalRisk = 999;

  possiblePaths(space,x,y,space[x][y],arr,1,maxSteps);
 // console.log("Min risk at (" + x + "," + y + ") : " + globalRisk);
  return globalRisk;
  // // get next Move
  // // possiblePath [0] = (x,y), (x+1,y), (x + 2,y)
  // // path [1] = (x,y),  (x+1,y), (x+1,y+1)
  // // path [2] = (x,y),  (x,y+1), (x,y+2)
  // // path [3] = (x,y),  (x,y+1), (x+1,y+1)
  // var possiblePath =[99,99,99,99];

  // if (x < maxX-1) {
  //   if (x + 1 < maxX-1) {
  //     possiblePath[0] = space[x][y]+space[x+1][y] + space[x+2][y];
  //   } else {
  //     possiblePath[0]=99;
  //   }
  //   if (y<maxY-1) {
  //     possiblePath[1] = space[x][y]+space[x+1][y]+space[x+1][y+1];
  //     possiblePath[3] = space[x][y]+space[x][y+1]+space[x+1][y+1];
  //   } else {
  //     possiblePath[2] = 99;
  //   }
  // }
  // if (y + 1 < maxY-1) {
  //   possiblePath[2] = space[x][y]+space[x][y+1] + space[x][y+2];
  // }
  // var min=99;
  // possiblePath.forEach((risk) => {
  //   if (min > risk) {
  //     min = risk;
  //   }
  // });
  //return min;
}
function parseData(data) {

  var space = [];

  const lines = data.split(/\r?\n/);

  for (var y=0; y < lines.length; y++) {
    var arr = lines[y].split('');
    for (var x = 0; x < arr.length; x++) {
      if (!space[x]) {
        space[x] = [];
      }
      space[x][y] = parseInt(arr[x]);
    }
  }

  return space;
 }
