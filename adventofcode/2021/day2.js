
const fs = require('fs')

fs.readFile('day2.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve2(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    
    var forward = 0;
    var depth = 0;

    lines.forEach((line) => {

      var input = parseCommand(line);
      switch(input.command) {
        case("forward") :
          forward += input.param;
          break;
        case("up") :
          depth -= input.param;
          break;
        case("down") :
          depth += input.param;
          break;
      }
    });

    console.log("depth " + depth);
    console.log("forward " + forward);
    console.log("result " + depth*forward);
}

function solve2 (data) {

  const lines = data.split(/\r?\n/);
    
  var forward = 0;
  var aim =0;
  var depth = 0;

  lines.forEach((line) => {

    var input = parseCommand(line);
    switch(input.command) {
      case("forward") :
        forward += input.param;
        depth += aim*input.param;
        break;
      case("up") :
        aim -= input.param;
        break;
      case("down") :
        aim += input.param;
        break;
    }
  });

  console.log("depth " + depth);
  console.log("forward " + forward);
  console.log("result " + depth*forward);
}

function parseCommand(cmd) {
  var input = cmd.split(" ");
  return  {
    command : input[0],
    param : parseInt(input[1])
  }
}