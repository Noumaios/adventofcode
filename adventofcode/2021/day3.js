
const fs = require('fs')

fs.readFile('day3.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve2(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    
    var gamma = [];
    var epsilon;

    lines.forEach((line) => {

      for (var i=0; i < line.length; i++) {
        if (!gamma[i]) {
          gamma[i] = 0;
        }
        gamma[i] += parseInt(line[i]);
      }
    });

    for (var i=0; i < gamma.length; i++) {
      if ((gamma[i] / lines.length) > 0.5) {
        gamma[i] = 1;
      } else {
        gamma[i] = 0;
      } 
    }

    console.log("gamma " + gamma.join(''));
    console.log("epsilon " + xor(gamma).join(''));

    var g = parseInt(gamma.join(''), 2);
    var e = parseInt(xor(gamma).join(''),2);

    console.log("Power is " + e*g ) ;
    
}

function solve2 (data) {

  const lines = data.split(/\r?\n/);
  
  var o2rating = [];
  var co2rating = [];

  o2rating = findNext(lines,true,0);
  co2rating = findNext(lines,false,0);

  console.log("02rating " + o2rating);
  console.log("C02rating " + co2rating);
  
  var g = parseInt(o2rating.join(''), 2);
  var e = parseInt(co2rating.join(''),2);

  console.log("life support rating is " + e*g ) ;
  
}

function findNext(data,mostCommon,position) {

  if (data.length == 1) {
    var result = data;
    return result;
  }
  var sum = 0;
  var array1 = [];
  var array0 = [];

  data.forEach((line) => { 
      sum += parseInt(line[position]);
      if (parseInt(line[position]) == 1) {
        array1.push(line);
      } else {
        array0.push(line);
      }
  });
  if ((sum / data.length) >= 0.5) {
    // 1 is most common
    if (mostCommon) {
      return findNext(array1,mostCommon,position+1);
    } else {
      return findNext(array0,mostCommon,position+1);
    }
  } else {
    // 0 is most common
    if (mostCommon) {
      return findNext(array0,mostCommon,position+1);
    } else {
      return findNext(array1,mostCommon,position+1);
    }
  }

}


function xor (data) {

  var ret = [0,0,0,0,0];
  for (var i=0; i < data.length; i++ ) {
    ret[i] = parseInt(data[i]) ^ 1
  }

  return ret;
  
}