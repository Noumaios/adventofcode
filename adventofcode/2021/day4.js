
const fs = require('fs');
const { websecurityscanner } = require('googleapis/build/src/apis/websecurityscanner');

fs.readFile('day4.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve2(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    
    var numbers = parseNumbers(lines);
    var grids = parseGrids(lines);

    var win = false;
    numbers.every((number) => { 
      grids.every((grid) => {
        // mark numbers on grid and check if winner
        if (checkGrid(grid,parseInt(number))) {
          // we have a winner
          console.log("We have a winner!");
          console.log(grid);
          console.log("result " + sumUnmarked(grid) * number);
          win= true;
          return false;
        };
        return true;
      });

      if (win) {
        return false;
      } else {
        return true;
      }

    });
    
}

function solve2 (data) {

  const lines = data.split(/\r?\n/);
    
  var numbers = parseNumbers(lines);
  var grids = parseGrids(lines);
  var winningGrids = [];

  var totalGrids = grids.length;
  var allwin = false;
  var winningNumber = 0; 
  var indexesToRemove = [];

  console.log("working on " + grids.length + " grids");

  numbers.every((number) => { 

    var indexesToRemove = [];
    
    console.log("total grids " + totalGrids);
    console.log("grids left " + grids.length);

    for (var i=0; i<grids.length; i++) {
      // mark numbers on grid and check if winner
      if (checkGrid(grids[i],parseInt(number))) {
        // we have a winner
        console.log("We have a winner for " + number);
        console.log(grids[i]);

        winningNumber = number;
        winningGrids.push(grids[i]);
        indexesToRemove.push(i);
      }
    }
    
    console.log( " Removing " + indexesToRemove);
    console.log(grids);
    for (var i = indexesToRemove.length -1; i >=0; i--) {
      grids.splice(indexesToRemove[i],1);
    }
    console.log("After");
    console.log(grids);

    if (grids.length == 0) {
      return false;
    } else {
      return true;
    }

  });


  console.log("Winning number is " + winningNumber);
  console.log("Last Winning grid " + winningGrids[winningGrids.length-1]);
  console.log("result " + sumUnmarked(winningGrids[winningGrids.length-1]) * winningNumber);

  
}

function parseNumbers(data) {
  var line = data[0];

  return line.split(',');
}

function parseGrids(data) {
  var grids = [];
  
  for (var i=2; i<data.length; i=i+6) {
    var grid = [];
    for (var k=i;k<i+5;k++) {
      grid[k-i] = [];
      for (var j=0; j<=12; j=j+3) {
        grid[k-i].push(parseInt(data[k].slice(j,j+2)));
      }
    }
    grids.push(grid);
  }

  return grids;
}

function checkGrid(grid, num) {

  console.log("Checking for : " + num)
  console.log(grid);
  
  for (var i=0; i<grid.length; i++) {
    for (var j=0; j < grid[i].length; j++) {
      if (num == grid[i][j]) {
        grid[i][j] = -1;
      }
    }
  }
  if (isWinner2(grid)) {
    return true;
  } else {
    return false;
  }
}

function isWinner(grid,line,col) {

  var linesum = 0;
  var colsum = 0;
  for (var i=0;i<grid[line].length;i++) {
    linesum += grid[line][i];
  }
  for (var i=0;i<grid.length;i++) {
    colsum += grid[i][col];
  }
  
  if (colsum == -5 || linesum == -5) {
    return true;
  } else {
    return false;
  }

}

function isWinner2(grid,a,b) {

  var linesum = 0;
  var colsum = 0;

  for (var line =0 ; line < grid.length ; line ++) {
    var linesum = 0;
    for (var i=0;i<grid[line].length;i++) {
      linesum += grid[line][i];
    }
    if (linesum == -5) {
      return true;
    }
  }

  for (var col = 0; col < 5; col ++) {
    var colsum = 0;
    for (var i=0;i<grid.length;i++) {
      colsum += grid[i][col];
    }
    if (colsum == -5) {
      return true;
    }
  }
  return false;

}

function sumUnmarked(grid) {

  var sum = 0;

  for (var i=0; i<grid.length; i++) {
    for (var j=0; j < grid[i].length; j++) {
      if (grid[i][j] != -1) {
        sum += grid[i][j];
      }
    }
  }
  return sum;
}