const fs = require('fs');
const { start } = require('repl');

fs.readFile('day5.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

function solve (data) {

    const fileLines = data.split(/\r?\n/);
    var space = initSpace();
    var lines = [];



    fileLines.forEach((l) => {

      var line = parseLine(l);
      addPointsToSpace(space,line);
      
    });

    showspace(space);
    overlap(space);
    
}


function solve2 (data) {

 
}

function addPointsToSpace (s,line) {

  // horizontal and vertical done
  if (line.start.x == line.end.x) {
    if (line.start.y >= line.end.y) {
      for (var y=line.start.y ; y >= line.end.y; y-- ) {
        s[line.start.x][y]++;
      }
      return;
    } else {
      for (var y=line.start.y ; y <= line.end.y; y++ ) {
        s[line.start.x][y]++;
      }
      return;
    }
  }
  if (line.start.y == line.end.y) {
    if (line.start.x >= line.end.x) {
      for (var x=line.start.x ; x >= line.end.x; x-- ) {
        s[x][line.start.y]++;
      }
      return;
    } else {
      for (var x=line.start.x ; x <= line.end.x; x++ ) {
        s[x][line.start.y]++;
      }
      return;
    }
  }


  // diagonals;
  var a = (line.end.y - line.start.y) / (line.end.x - line.start.x);
  if (Math.abs(a) == 1) {
    // crunch y intercept
    // y = ax + b
    var b = line.start.y - a*line.start.x;

    if (line.start.x < line.end.x) {
      for (var x=line.start.x;x<=line.end.x; x++) {
        s[x][a*x+b]++;
      }
    } else {
      for (var x=line.start.x;x>=line.end.x; x--) {
        s[x][a*x+b]++;
      }
    }

  }
}

function showspace(s) {

    for (var i=0; i<s.length; i++) {
      console.log(i + " : " +s[i]);
    }
}

function overlap(s) {
  var count = 0;
  for (var i=0; i<s.length; i++) {
    for (var j=0; j<s[i].length; j++) {
      if (s[i][j] >= 2) {
        count++;
      }
    }
  } 
  console.log(count);
}

function parseLine(l) {

  var start = {};
  var end ={};
  var line={
    "start" : start,
    "end" : end
  }

  var pts = l.split(" -> ");

  var sp = pts[0].split(",");
  var ep = pts[1].split(",");

  line.start.x = parseInt(sp[0]);
  line.start.y = parseInt(sp[1]);
  line.end.x = parseInt(ep[0]);
  line.end.y = parseInt(ep[1]);

  return line;

}


function initSpace() {

  var space = [];
  for (var x=0; x<1000; x++) {
    space[x] = [];
    for (var y=0; y<1000; y++) {
      space[x].push(0);
    }
  } 

  return space;
}