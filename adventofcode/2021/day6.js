
const fs = require('fs');
const maxdays = 256;
var fishcount =0;
var content = [];

fs.readFile('day6.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve3(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    
    var lanternFishes = parseLine(lines[0]);

    console.log("Initial state : " + lanternFishes);

    for (var i=1; i<=maxdays; i++) {
      tick(lanternFishes); 
      console.log("State after " + i + " days");
    }

    // fs.writeFile('newData.txt', lanternFishes.toString(), err => {
    //   if (err) {
    //     console.error(err)
    //     return
    //   }
    //   //file written successfully
    // })

    console.log("Number of fishes " + lanternFishes.length);

}

function solve2 (data) {

  const lines = data.split(/\r?\n/);
    
  var lanternFishes = parseLine(lines[0]);

  //console.log("Initial state : " + lanternFishes);
  var start = new Date(); 
  var end;
  var total=0;
  var count = lanternFishes.length;
  
  for (var i=0; i<6; i++) {
    var c = 0;
    c = tick2(i,1); 
    console.log("Crunching " + i + " : " + c);
  }

  //console.log("Number of fishes " + total);

}

function solve3(data) {

  const lines = data.split(/\r?\n/);
  
  var total = 0;
  var lanternFishes = parseLine(lines[0]);

  for (var i=0; i<lanternFishes.length; i++) {
    switch (lanternFishes[i]) {
      case 0:
        total += 6703087164;
        break;
      case 1:
        total += 6206821033;
        break;
      case 2:
        total += 5617089148;
        break;
      case 3:
        total += 5217223242;
        break;
      case 4:
        total += 4726100874;
        break;
      case 5:
        total += 4368232009;
        break;
    }
  }

  console.log(total);

}


function parseLine(line) {
  var ages = line.split(",");
  var fishes = [];

  ages.forEach((age) => {
    fishes.push(parseInt(age));
  });

  return fishes;
}

function tick(fishes) {

  var newFishes = [];

  for (var i=0; i < fishes.length; i++ ) {
    if (fishes[i] == 0) {
      newFishes.push(8);
      fishes[i] = 6;
    } else {
      fishes[i]--;
    }
  }
  newFishes.forEach((fish) => {
    fishes.push(fish);
  });
}

function tick2(fishes,day) {
  if (day == maxdays+1) {
    return 1;
  }
  if (fishes == 0) {
    return tick2(6,day+1) + tick2(8,day+1);
  } else {
    return tick2(fishes-1,day+1);
  }
}

function printFishes(fishes) {
  var arr = [];
  fishes.forEach((fish) => {
    arr.push(fish.clock);
  });
  return arr;
}