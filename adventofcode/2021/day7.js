
const fs = require('fs')

fs.readFile('day7.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    
    var crabs = parseLine(lines[0]);
    var medianIndex = Math.abs(crabs.length/2);
    var minFuel = crabs[crabs.length-1]*crabs.length*crabs.length;
    var minIndex = 0;

    
    for (var i=0; i<=crabs[crabs.length-1]; i++) {
      var fuel = newFuelTo(i,crabs);
      console.log(fuel);
      if (fuel <= minFuel) {
        minFuel = fuel;
        minIndex = i;
      }
    }

    var fuelConsumed = minFuel;

    console.log("Median Index " + medianIndex);
    console.log("min index " + minIndex);
    console.log("fuel consumed " + fuelConsumed);
}

function solve2 (data) {

  const lines = data.split(/\r?\n/);
    
  var forward = 0;
  var aim =0;
  var depth = 0;

  lines.forEach((line) => {

    var input = parseCommand(line);
    switch(input.command) {
      case("forward") :
        forward += input.param;
        depth += aim*input.param;
        break;
      case("up") :
        aim -= input.param;
        break;
      case("down") :
        aim += input.param;
        break;
    }
  });

  console.log("depth " + depth);
  console.log("forward " + forward);
  console.log("result " + depth*forward);
}

function fuelTo(pos,crabs) {

  var fuel = 0;

  crabs.forEach((crab) => {
    fuel += Math.abs(pos-crab);
  });

  return fuel;
}

function newFuelTo(pos,crabs) {

  var fuel = 0;
  crabs.forEach((crab) => {
    fuel += sumTo(Math.abs(pos-crab));
  });

  return fuel;
}

function sumTo(n) {
  var total = 0;

  for (var i=0; i<=n; i++) {
    total += i;
  }
  return total;
}

function average(arr) {
  var total = 0;

  for (var i=0; i < arr.length; i++) {
    total += arr[i];
  }

  return total/arr.length;
}

function parseLine(line) {
  var input = line.split(",");
  var crabs = [];
  input.forEach((crab) => {
    crabs.push(parseInt(crab));
  });

  return crabs.sort(function(a, b){return a-b});
}