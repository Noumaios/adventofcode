
const fs = require('fs');

fs.readFile('day8.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve2(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    var count = [0,0,0,0];

    lines.forEach((line) => {
      
      var l = parseLine(line);
      var numbers = l.output.split(" ");

      numbers.forEach((n) => {
        console.log("num:" + n + ":");
        switch (n.length) {
          case 2:
            count[0]++;
            break;
          case 4:
            count[1]++;
            break;
          case 3:
            count[2]++;
            break;
          case 7:
            count[3]++;
            break;
        }
      });
    });
    var total = count[0]+count[1]+count[2]+count[3];
    console.log(count);
    console.log("result " + total);
    
}

function solve2 (data) {

  const lines = data.split(/\r?\n/);
  var count = [0,0,0,0];
  var tot = 0;
  var ind = 0;

  lines.forEach((line) => {
    
    var l = parseLine(line);
    var codes = {};

    var numbers = l.input.split(" ");
    var reverseStone = ["","","","","","","","","","",""];
    
    while(Object.keys(codes).length < 10) {
    
      numbers.forEach((p) => {

        var n = sortStr(p);

        switch (n.length) {
          case 2 :
            if (!codes[n]) {
              codes[n] = 1; 
              reverseStone[1] = n;
            }
            break;
          case 4:
            if (!codes[n]) {
              codes[n] = 4; 
              reverseStone[4] = n;
            }
            break;
          case 3:
            if (!codes[n]) {
              codes[n] = 7; 
              reverseStone[7] = n;
            }
            break;
          case 7:
            if (!codes[n]) {
              codes[n] = 8; 
              reverseStone[8] = n;
            }
            break;
          case 5:
            if (reverseStone[7] != "" && contains(n,reverseStone[7])) {
              if (!codes[n]) {
                codes[n] = 3; 
                reverseStone[3] = n;
              }
            } else {
              if(reverseStone[6] != "" && reverseStone[4]) {
                if (contains(n,remove(reverseStone[6],reverseStone[4]))) {
                  if (!codes[n]) {
                    codes[n] = 2; 
                    reverseStone[2] = n;
                  }
                } else {
                  if (!codes[n]) {
                    codes[n] = 5; 
                    reverseStone[5] = n;
                  }
                }
              }
            }
            break;
          case 6:
            if (reverseStone[3] != ""){
              if (contains(n,reverseStone[3])) {
                if (!codes[n]) {
                  codes[n] = 9; 
                  reverseStone[9] = n;
                }
              } else if (contains(n,reverseStone[7])) {
                if (!codes[n]) {
                  codes[n] = 0; 
                  reverseStone[0] = n;
                }
              } else {
                if (!codes[n]) {
                  codes[n] = 6; 
                  reverseStone[6] = n;
                }
              }
          }
            break;
        }
      });
    }
    //console.log(codes);

    var num = l.output.split(" ");
    var txt = "";
    //now decode;
    num.forEach((segment) => {
      //console.log("Segment " + segment);
      if (segment != "") {
        txt += codes[sortStr(segment)]; 
      }
    });

    console.log(ind + 1 + "result for " + num + " : " + txt);
    tot += parseInt(txt);
    ind++;
  });

  console.log("Result : " + tot);
  
}


function parseLine(line) {

  var res = {};
  var arr = line.split("|");

  res = {
    "input" : arr[0],
    "output" : arr[1]
  }

  return res;

}

function remove(str1,str2) {

  var res = "";

  for (var i=0; i < str1.length; i++) {
    if (str2.indexOf(str1[i]) == -1) {
      res += str1[i];
    }
  }
  return res;
}

function sortStr(str) {
  
return str.split('').sort().join('');

}

function contains(str1,str2) {
  var target =str2.length;
  var count = 0;
  
  for (var i=0; i<str2.length; i++ ){
    if (str1.indexOf(str2[i]) != -1) {
      count++;
    }
  }

  if (count == target) {
    return true;
  } else {
    return false;
  }

}