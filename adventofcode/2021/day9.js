
const fs = require('fs');

fs.readFile('day9.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve2(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    var lows = 0;

    var numLines = lines.length;

    for (var i=0; i<numLines; i++) {
      for (var j=0; j <lines[i].length; j++) {
       
        if (checkpoint(lines,i,j,lines.length,lines[i].length)) {
          console.log("Found Low " + lines[i][j]);
          lows += 1  + parseInt(lines[i][j]);
        }

      }

      // Middle of the array



    }

    console.log("result " + lows);
    
}

function solve2 (data) {

  const space = data.split(/\r?\n/);

  var lows = [];

  // Get the lows
  for (var i=0; i<space.length; i++) {
    for (var j=0; j <space[i].length; j++) {     
     if (checkpoint(space,i,j,space.length,space[i].length)) {
        console.log("Found Low " + space[i][j]);
        lows.push([i,j]);
      }
    }
  }
  // Find the bassins
  var bassins = [];
  var size = [];
  lows.forEach((low) => {
    var bassin = {};
    addPoint(low[0],low[1],bassin);
    findPointsAround(low[0],low[1],bassin,space);
    console.log(bassin);
    bassins.push(bassin);
    size.push(Object.keys(bassin).length);
  });

  var sortedSizes = size.sort(function(a, b) {
    return b - a;
  });

  console.log(bassins);
  console.log(sortedSizes);
  console.log(parseInt(sortedSizes[0]) * parseInt(sortedSizes[1])  * parseInt(sortedSizes[2]) )
}


function addPoint(line,col,set) {

  var idx = line.toString().padStart(3,"0") + col.toString().padStart(3,"0");
  if (!set[idx]) {
    set[idx] = 0;
  }

}

function findPointsAround(line,col,bassin,space) {
  console.log("Checking " + line + "," + col);
  for (var i=Math.max(line-1,0);i<=Math.min(line+1,space.length-1);i++) {
    for (var j=Math.max(col-1,0);j<=Math.min(col+1,space[i].length-1); j++) { 
      console.log(i,j);
      if (i ==  line && j == col) {
        continue;
      }

      if (col != j && Math.abs((line - i) / (col - j)) == 1) {
        continue;
      }

      if (parseInt(space[i][j]) == 9) {
        
        continue;
      }

      if (parseInt(space[line][col]) < parseInt(space[i][j])) {
        console.log("New");
        addPoint(i,j,bassin);
        findPointsAround(i,j,bassin,space);
      }
    }
  } 
  return; 
}


function checkpoint(lines,line,col,lmax,cmax) {

  console.log("Checkin " + lines[line][col]);
 
   for (var i=Math.max(line-1,0);i<=Math.min(line+1,lmax-1);i++) {
     for (var j=Math.max(col-1,0);j<=Math.min(col+1,cmax-1); j++) { 
       //console.log(i,j);
       if (i ==  line && j == col) {
         continue;
       }
       if (col != j && Math.abs((line - i) / (col - j)) == 1) {
         continue;
       }
       if (parseInt(lines[line][col]) >= parseInt(lines[i][j])) {
         //console.log("False");
         return false;
       }
     }
   }  
   return true;
  
 }