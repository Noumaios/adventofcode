
const fs = require('fs')

fs.readFile('day1.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);

    const elvesCalories = [];
    var calories = 0;

    lines.forEach((line) => {
        if (line == "") {
          elvesCalories.push(calories);
          calories = 0;
        } else {
          calories += parseInt(line);
        }
    });
    elvesCalories.push(calories);

    const sortedElves = elvesCalories.sort(function(a, b){return b - a});
    const result1 = sortedElves[0];
    const result2 = sortedElves[0]+sortedElves[1]+sortedElves[2];

    console.log(result1);
    console.log(result2);
}
