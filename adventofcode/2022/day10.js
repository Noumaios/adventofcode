
const fs = require('fs');
const { deflateRaw } = require('zlib');

fs.readFile('day10.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

var signalStrength = 0;
const screen = [];
var currentLine = "";
var cycle = 1;
var register = 1;

function solve (data) {

    const lines = data.split(/\r?\n/);
    
    lines.forEach((line) => {     

      const l = line.split(' ');
      switch(l[0]) {
        case "noop": 
          tick(); 
          break;
        case "addx":     
          tick();
          tick();
          register += parseInt(l[1]);
          break;
      }
    });
    console.log(signalStrength);
    screen.forEach((l) => {
      console.log(l);
    });
}

function tick () {

  if (cycle == 20 || (cycle-20) % 40 == 0) {
    signalStrength += cycle*register;
    console.log(cycle*register);
  }
  
  position = register+1;
  if ((cycle%40) >= position-1 && (cycle%40) <=position+1) {
    currentLine += "#";
  } else {
    currentLine += ".";
  }

  if (cycle %40 == 0) {
    screen.push(currentLine);
    currentLine = "";
  }
  cycle++;
}
