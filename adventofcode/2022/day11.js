
const fs = require('fs');

const monkeys = [];
const maxRound = 10000;
var globalMod = 1;

fs.readFile('day11.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

function solve (data) {

  // parse monkey file
  parseFile(data);
  console.log(monkeys);

  for (var i = 0; i < maxRound; i++) {
    //play a round

    monkeys.forEach((monkey) => {

      monkey.items.forEach((item)=> {
        monkey.inspectCount++;
  
        item = monkey.op(item);        
        //item = Math.floor(item/3);
        item %= globalMod;

        if (monkey.test(item)) {
          monkeys[monkey.true].items.push(item);
        } else {
          monkeys[monkey.false].items.push(item);
        }
      }); 
      monkey.items = [];
    });

  }

  // console.log(monkeys);
  monkeys.sort((a,b) => {
   if (a.inspectCount > b.inspectCount) {
     return -1;
   } else {
     return 1;
   }
  });

  console.log(monkeys[0].inspectCount*monkeys[1].inspectCount);
   
}


function parseFile(data) {

  const lines = data.split(/\r?\n/);
  var monkey = {
    items : [],
    inspectCount : 0
  };


  lines.forEach((line) => {  

    if (line == "") {
      monkeys.push(monkey);
      monkey = {
        items : [],
        inspectCount : 0
      };
    } 

    const l = line.split(':');

    if (l[0].indexOf("Monkey") != -1){
      monkey.id = l[0].split(' ')[1];
    } else if (l[0].indexOf("Operation") != -1) {
      monkey.op = buildOp(l[1].split(' ')); 
    } else if (l[0].indexOf("Starting items") != -1) {
      const items = l[1].split(",");
      items.forEach((item) => {
        monkey.items.push(parseInt(item));
      });
    } else if (l[0].indexOf("Test") != -1) {
      monkey.test = buildTest(l[1].split(' ')); 
    } else if(l[0].indexOf("If true") != -1) {
      monkey.true = l[1].split(' ')[4];
    } else if(l[0].indexOf("If false") != -1) {
     monkey.false = (l[1].split(' ')[4]);
    }
  });

  monkeys.push(monkey);
}


function buildOp(op) {

  // assume op[3] always old, operation at op[4] 
  if( op[5] == "old") {
    if (op[4] == "*") {
      return function(worry) { return worry*worry};
    } 
  } else {
    if (op[4] == "*") {
      return function(worry) { return worry*parseInt(op[5])};
    } else if (op[4] == "+") {
      return function(worry) { return worry+parseInt(op[5])};
    }
  } 
}


function buildTest(test) {

  // part 2 : create a modulus to keep worry level manageable
  globalMod *= parseInt(test[3]);

  return function (worry) { 
    if (worry % parseInt(test[3]) == 0) { 
      return true 
    } else {
      return false 
    }
  };

}



