
const { reverse } = require('dns');
const fs = require('fs');

fs.readFile('day12.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
  console.log("part2");
  solve2(data);
});

const map = [];

var graph = {};

function solve (data) {

    const lines = data.split(/\r?\n/);

    var s = new Date();
    
    var visited = [];
    var start = {
      position : [20,0],
      elevation : 'S',
      parents : "",
      from : [],
      distance : 0
    }
    // create the matrix
    for (var row=0;row<lines.length;row++) {
      const l = lines[row].split('');
      var arr =[];
      for (var col=0;col<l.length;col++) {
        arr.push(l[col]);
        if (l[col] == "S") {
          start.position = [row,col];
        }
      }
      map.push(arr);
    }



    var queue = [start];
    var step = 0;

    while (queue.length > 0) {

      step++;
      var node = queue.shift();
      
      // if visited
      var idx = visited.findIndex((p) => p.position[0] == node.position[0] && p.position[1] == node.position[1]);
      if (idx != -1) {
        continue;
      } 

      if (node.elevation == "E") {
        console.log(node);
        break;
      }

      var next = findNeighbour(node.position);

      next.forEach((n) => {

        n.distance = node.distance+1;
        n.parents += node.parents + node.elevation;  
        n.from = node.from.slice();
      
        var beenThere = node.from.indexOf(n.position[0]*1000+n.position[1])
        if (beenThere == -1) {
            n.from.push(n.position[0]*1000+n.position[1]);
            queue.push(n);
          }
      });
      visited.push(node);
  }

  //console.log(best);
  console.log("step " + step);

  var e = new Date();
  console.log("elapsed " + (e-s));

}

function solve2 (data) {

  const lines = data.split(/\r?\n/);

  var s = new Date();
  var visited = [];
  var start = {
    position : [20,68],
    elevation : 'E',
    parents : "",
    from : [],
    distance : 0
  }
  
  // create the matrix
  for (var row=0;row<lines.length;row++) {
    const l = lines[row].split('');
    var arr =[];
    for (var col=0;col<l.length;col++) {
      arr.push(l[col]);
      if (l[col] == "E") {
        start.position = [row,col];
      }
    }
    map.push(arr);
  }



  var queue = [start];
  var step = 0;

  while (queue.length > 0) {

    step++;
    var node = queue.shift();
    
    // if visited
    var idx = visited.findIndex((p) => p.position[0] == node.position[0] && p.position[1] == node.position[1]);
    if (idx != -1) {
      continue;
    } 

    if (node.elevation == "a"  || node.elevation == "S") {
      console.log(node);
      break;
    }

    var next = findNeighbour2(node.position);

    next.forEach((n) => {

      n.distance = node.distance+1;
      n.parents += node.parents + node.elevation;  
      n.from = node.from.slice();
    
      var beenThere = node.from.indexOf(n.position[0]*1000+n.position[1])
      if (beenThere == -1) {
          n.from.push(n.position[0]*1000+n.position[1]);
          queue.push(n);
        }
    });
    visited.push(node);
}

console.log("step " + step);

var e = new Date();
console.log("elapsed " + (e-s));

}

function findNeighbour(pos) {

  var row = pos[0];
  var col = pos[1];

  let n = [];

  if (map[row][col] == "E") {
    return [];
  }

  if (map[row-1] && map[row-1][col]) {
    n.push([row-1,col]);
  }
  if (map[row+1] && map[row+1][col]) {
    n.push([row+1,col]);
  }
  if (map[row][col-1]) {
    n.push([row,col-1]);
  }
  if (map[row][col+1]) {
    n.push([row,col+1]);
  }
  
  const arr = [];

  n.forEach((p) => {
    node = {
      position : p,
      elevation : map[p[0]][p[1]],
      parents : "",
      from : [],
      distance : 0
    }
    if (node.elevation == "E") {
      if (map[row][col] == "z") {
        arr.push(node);
      }
    } else {
      if (((map[p[0]][p[1]].charCodeAt(0) - map[row][col].charCodeAt(0)) <= 1)) {
        arr.push(node);
      } else if (map[row][col] == "S") {
        arr.push(node);
      }
    }
  })
  return arr;
}

function findNeighbour2(pos) {

  var row = pos[0];
  var col = pos[1];

  let n = [];
  if (map[row][col] == "a" || map[row][col] == "S") {
    return [];
  }

  if (map[row-1] && map[row-1][col]) {
    n.push([row-1,col]);
  }
  if (map[row+1] && map[row+1][col]) {
    n.push([row+1,col]);
  }
  if (map[row][col-1]) {
    n.push([row,col-1]);
  }
  if (map[row][col+1]) {
    n.push([row,col+1]);
  }
  
  const arr = [];

  n.forEach((p) => {
    node = {
      position : p,
      elevation : map[p[0]][p[1]],
      parents : "",
      from : [],
      distance : 0
    }
    if (map[row][col] == "E") {  
      if (node.elevation == "z") {
        arr.push(node);
      }
    } else {
      if ((map[p[0]][p[1]].charCodeAt(0) - map[row][col].charCodeAt(0)) >= -1) {
        arr.push(node);
      } 
    }
  })
  return arr;
}
