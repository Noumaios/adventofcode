const fs = require('fs');

fs.readFile('day13.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});


function solve (data) {
  
  const pairs = [];
  var packets = [];
  var pair = []
  
  const lines = data.split(/\r?\n/);
  lines.forEach((line) => {     
    if (line == "") {
      pairs.push(pair);
      pair = [];
    } else {
      packets.push(eval(line));
      pair.push(eval(line));
    }
  });
  pairs.push(pair);

  // part 1
  var sum=0;
  for (var i=0;i<pairs.length;i++) {
    console.log("Pair " + (i+1) + "====================");
    var res = compare(pairs[i][0],pairs[i][1]," ");
    if (typeof(res) != "boolean" || res) {
      console.log("Right Order");
      sum += i+1;
    } else {
      console.log("Not Right Order");
    }
  }

  // part 2
  packets = packets.concat([[[2]],[[6]]]);

  packets.sort((a,b) => {
    var res = compare(a,b," ");
    if (typeof(res) != "boolean" || res) {
      return -1
    } else {
      return 1
    }
  });
  
  var key = 1;
  for (var i=0;i<packets.length;i++) {
    if (JSON.stringify(packets[i]) == "[[2]]") {
      key *= i+1;
    }
    if (JSON.stringify(packets[i]) == "[[6]]") {
      key *= i+1;
    }
  }

  console.log("Part1 " +  sum);
  console.log("Part2 " +  key);

}

function compare(left,right,buff){

  console.log(buff + "comparing " + JSON.stringify(left) + " vs " +  JSON.stringify(right));

  for (var i=0; i<left.length; i++) {

    if (!right || (i >= right.length)) {
      console.log(buff + " ran of item on right side");
      return false;
    }
    
    if (typeof(left[i]) == "number") {
     if (typeof(right[i]) == "number"){
        console.log(buff + " comparing " + left[i] + " vs " + right[i]);
        if (left[i] > right[i]) {
          console.log(buff + " Right side is smaller");
          return false;
        } else if (left[i] < right[i]) {
          console.log(buff + " Left side is smaller");
          return true;
        }
      } else {
        var res = compare([left[i]],right[i],buff+"  ");
        if (typeof(res) == "boolean") {
          return res;
        }
      }
    } else {
      var res;
      if (typeof(right[i]) == "number"){
        res = compare(left[i],[right[i]],buff+"  ");        
      } else {
        res = compare(left[i],right[i],buff+"  ");
      }
      if (typeof(res) == "boolean") {
        return res
      }
    }
  }

  if (left.length < right.length) {
    console.log(buff + "ran of item on left side");
    return true
  }
}
