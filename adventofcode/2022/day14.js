
const fs = require('fs');

fs.readFile('day14.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
  solve2(data);
});

var minx = Infinity;

function solve (data) {

  const map = buildMap(data);
  var unit = 0;

  while(true) {
    const rest = restPoint(map);
    if (!rest) {  
      break;
    } else {
      unit++;
    }
  }
  printMap(map);
  console.log("units " + unit);
}

function solve2 (data) {

  const map = buildMap(data);
  var unit = 0;

  // add 2 more rows for the floor
  const maxy = map.length;
  map[maxy] = [];
  map[maxy+1] = [];
  for (var i=0;i<map[0].length;i++) {
    map[maxy][i] = ".";
    map[maxy+1][i] = "#"
  }

  while(true) {
    const rest = restPoint2(map);
    if (!rest) {  
      break;
    } else {
      unit++;
    }
  }
  console.log("units for part 2 " + (unit+1));
}

function restPoint(map) {

  var x = 500-minx;
  var rest;

  for (var y=0;y<map.length-1;y++) {
     
    if (y == map.length-1) {
      // on the floor
      break;
    }
    if (map[y+1][x] != '.') { 
      // diag left
      if (!map[y+1][x-1]) {
        //abyss on the left
        break;
      } else {
        if (map[y+1][x-1] == ".") {
          x = x-1;
          continue;
        } 
      }
      if (!map[y+1][x+1]) {
        //abyss on right
        break;
      } else {
          if (map[y+1][x+1] == ".") {
            x = x + 1;
            continue;
          } else {
            //rest
            rest = [y,x];
            map[y][x] = "o";
            break;
          }
      }
    } 
  }
  return rest;
}
 

function restPoint2(map,goright) {

  var x = 500-minx;
  var rest;

  if (map[1][x-1] == "o" && map[1][x] == "o" && map[1][x+1] == "o" ) {
    return null;
  }

  for (var y=0;y<map.length-1;y++) {

    if (y == map.length-2) {
      // on the floor
      map[y][x] = "o";
      return [y,x];
    }
    if (map[y+1][x] != '.') {
         // diag left
      if (!map[y+1][x-1]) {
        growLeft(map);
        minx--;
        continue;
      } else {
        if (map[y+1][x-1] == ".") {
          x = x-1;
          continue;
        } 
      }
      if (!map[y+1][x+1]) {
        growRight(map);
        x=x+1
        continue;
      } else {
          if (map[y+1][x+1] == ".") {
            x = x + 1;
            continue;
          } else {
            //rest
            rest = [y,x];
            map[y][x] = "o";
            break;
          }
      } 
    } 
  }
  return rest;
}


function buildMap(data) {
  const lines = data.split(/\r?\n/);
  
  var maxy=0;
  var maxx=0;
  const hash = {};
  
  lines.forEach((line) => {     

    const l = line.split(' -> ');
    var prev; 
    

    l.forEach((p) => {
      var ps = p.split(',');
      var point = [parseInt(ps[0]),parseInt|(ps[1])];

      
      if (point[0] < minx) {
        minx = point[0];
      }
      if (point[0] > maxx) {
        maxx = point[0];
      }
      if (point[1] > maxy) {
        maxy = point[1];
      }

      if (prev) {
        if (point[0] == prev[0]) {
          for (var i=prev[1]+1;i<point[1];i++) {
            hash[[point[0],i]]=1;
          }
          for (var i=prev[1]-1;i>point[1];i--) {
            hash[[point[0],i]]=1;
          }
        } else {
          // vertical line
          for (var i=prev[0]+1;i<point[0];i++) {
            hash[[i,prev[1]]]=1;
          }
          for (var i=prev[0]-1;i>point[0];i--) {
            hash[[i,prev[1]]]=1;
          }
        }
      }
      hash[point]=1;
      prev = point;
    });
  });
  
  const map=[];

  for (var y=0;y<=maxy;y++) {
    map[y]=[];
    for (var x=0;x<=maxx-minx;x++) {
      if (hash[[[x+minx],y]]) {
        map[y][x] = '#';
      } else {
        map[y][x]='.'
      }
    }
  }
  return map;
}

function printMap(map) {
  map.forEach((l)=> {
    var line = "";
    l.forEach((p)=>{
      line +=p;
    })
    console.log(line);
  });
}

function growLeft(map) {

  for (var i=0;i<map.length;i++) {
    if (i == map.length -1) {
      map[i].unshift("#");
    } else {
      map[i].unshift(".");
    }
  }
}

function growRight(map) {

  for (var i=0;i<map.length;i++) {
    if (i == map.length -1) {
      map[i].push("#");
    } else {
      map[i].push(".");
    }
  }
}