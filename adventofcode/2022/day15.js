
const fs = require('fs');

fs.readFile('day15.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve2(data);
});
var minx = miny = Infinity;
var maxx = maxy = 0;
var minbound=0
var maxbound=4000000;

const beacons = {};
const sensors = {};
const noBeacons = {};
const possibleBeacons = {};

function solve (data) {
  
  buildMap(data,beacons,sensors);

  console.log(beacons);
  console.log(sensors);
  console.log(minx,maxx);

  var count =0;
  var y=2000000;
  
  // iterate through sensors 
  for (key in sensors) {
    const sensor = sensors[key];
    
    // find all points where distance from sensors = distance from beacon
    var startx = 0;
    var endx = 0;


    startx = sensor.coord[1] - sensor.distance + distance(sensor.coord,[y,sensor.coord[1]]);
    endx = sensor.coord[1] + sensor.distance- distance(sensor.coord,[y,sensor.coord[1]]);
    console.log(sensor);
    console.log(startx,endx);

    for (var x=startx;x<=endx;x++) {
      if (noBeacons[x]) {
        continue;
      }
      if (distance(sensor.coord,[y,x])<=sensor.distance) {
        if (!sensors[[y,x]] && !beacons[[y,x]]) {
          if (!noBeacons[x]) {
            count++;
            noBeacons[x] = 1;
          } 
        }
      }
    } 
  }

  console.log(count);

}

function solve2 (data) {

  
  
  buildMap(data,beacons,sensors);

  console.log(beacons);
  console.log(sensors);
  console.log(minx,maxx);

  var count =0;
  //var y=2000000;

  var minbound=0
  var maxbound=4000000;
  

  // iterate through sensors 
  for (key in sensors) {
    const sensor = sensors[key];
    
    // find all points where distance from sensor = distance from beacon + 1
    // With Manathan distance = diamond around sensor
    var startx = sensor.coord[1] - sensor.distance - 1;
    var endx = sensor.coord[1] + sensor.distance + 1;

    var y = sensor.coord[0];
    
    addPoint(possibleBeacons,[startx,y]);
    addPoint(possibleBeacons,[endx,y]);
  
    for (var x=startx+1;x<=sensor.coord[1];x++) {
      addPoint(possibleBeacons,[y + x-startx,x]);
      addPoint(possibleBeacons,[y - x + startx,x]);
      addPoint(possibleBeacons,[y + x-startx,endx-(startx-x)]);
      addPoint(possibleBeacons,[y - x + startx,endx-(startx-x)]);
    }
  }

  console.log(possibleBeacons);

  
  var max = {};
  for (key in possibleBeacons) {
    max = { 
      value:possibleBeacons[key],
      coord:key.split(',')
    }
  }
  console.log(max);
  console.log((parseInt(max.coord[0]) + 4000000*parseInt(max.coord[1])));
}

function buildMap(data,beacons,sensors) {

  const lines = data.split(/\r?\n/);

  // create the map with the right shift
  lines.forEach(line => {
    const l= line.split(":");

    // beacon
    const b = l[1].slice(24).replace(' y=','').split(',');
    const beacon = [parseInt(b[1]),parseInt(b[0])];
    beacons[beacon] = 1;
    // sensor
    const s = l[0].slice(12).replace(' y=','').split(',');
    const sensor = [parseInt(s[1]),parseInt(s[0])];
    sensors[sensor] = {
                        coord: sensor,
                        beacon:beacon,
                        distance:distance(sensor,beacon)
                      } 
    //smaxy = Math.max(maxy,beacon[0],sensor[0]);
    minx = Math.min(minx,beacon[1],sensor[1]);
    maxx = Math.max(maxx,beacon[1],sensor[1]);
  });

}


function distance(a,b) {
  return Math.abs(a[0]-b[0]) + Math.abs(a[1]-b[1])
}

function addPoint(map,point) {
  if (point[0] >= minbound && point[0]<=maxbound && point[1]>=minbound && point[1] <= maxbound) {
    for (key in sensors) {
      const sensor = sensors[key];
      if (distance(sensor.coord,point)<=sensor.distance) {
        return;
      }
    }
    if (!map[point]) {
      map[point] = 1;
    } else {
      map[point]++;
    }
  }
}