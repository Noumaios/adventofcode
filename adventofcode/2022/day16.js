
const fs = require('fs');

fs.readFile('day16.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  valves = buildGraph(data);
  solve();
  //solve2();

});

var valves;

function solve2 () {
  
  
  const start = "AA";
  var open = [];
  var maxPressure = 0;

  var all = [];
  var left = 0;
  for (var key in valves) {
    if (valves[key].rate > 0) {
      all.push(key);
      left += valves[key].rate;
    }
  }
  var current = valves[start];


  var stack = [];
  var node = {
    id : current.id,
    
    from : {}
  }
  var teamNode = {
    me : "AA",
    elephant : "AA",
    score : 0,
    currentFlow : 0,
    remaining:26,
    left : left,
    lefttoOpen : all,
    opened : {},
  }

  var max = 0;
  var maxTeamNode = {};

  stack.push(teamNode);
  var visited = {};

  console.log(stack);

  while(stack.length > 0) {
 
    var c = stack.shift();

    // check for better route
    var arr = [c.me,c.elephant].sort();
    var seen = visited[arr[0] + " - " + arr[1] + " - " + c.remaining];
    var heur = c.score;
    
    if (seen && seen >= heur ) {
      continue;
    } else {
      visited[arr[0] + " - " + arr[1] + " - " + c.remaining] = heur;
    }
    
    // out of time
    if (c.remaining == 0) {
      if (c.score > max) {
        max = c.score;
        maxTeamNode = c;
      }
      continue;
    }

    // nothing left to open
    if (c.lefttoOpen.length == 0) {
      // stay and loop     
      stack.push(nextTeamNode(c,c.me,c.elephant,false,false));
      continue;
    }  
    
    if(c.lefttoOpen.indexOf(c.me) != -1) {
      if (c.elephant != c.me && c.lefttoOpen.indexOf(c.elephant) != -1) {
        stack.push(nextTeamNode(c,c.me, c.elephant,true,true));
      } else {
        // find next for elephants and me
        const nextNodes = valves[c.elephant].next;
        nextNodes.forEach((next) => {
          // just move
          stack.push(nextTeamNode(c,c.me,next,true,false));
        });
      }
    } else if (c.lefttoOpen.indexOf(c.elephant) != -1) {
        // find next for elephants and me
        const nextNodes = valves[c.me].next;
        nextNodes.forEach((next) => {
          // just move
          stack.push(nextTeamNode(c,next,c.elephant,false,true));
        });
      }

    const nextMeNodes = valves[c.me].next;
    nextMeNodes.forEach((nextMe) => {
      // just move
      const nextElephantNodes = valves[c.elephant].next;
      nextElephantNodes.forEach((nextElephant) => {
        stack.push(nextTeamNode(c,nextMe,nextElephant,false,false));
      })
    });
  }

  console.log(max);
  console.log(maxTeamNode);
}

function solve () {
  
  
  const start = "AA";
  var open = [];
  var maxPressure = 0;
  var left = 0;

  var all = [];
  for (var key in valves) {
    if (valves[key].rate > 0) {
      all.push(key);
      left += valves[key].rate;
    }
  }
  
  var most = left*30;
  var current = valves[start];

  var stack = [];
  var node = {
    id : current.id,
    score : 0,
    currentFlow : 0,
    remaining:30,
    left : left,
    opened : {},
    lefttoOpen : all,
    from : {}
  }

  var max = 0;
  var maxnode = {};

  stack.push(node);
  var visited = {};

  console.log(stack);
  while(stack.length > 0) {
 
    var c = stack.shift();

    // check for better route
    var seen = visited[c.id + " - " + c.remaining];
    var heur = c.score;
    
    if (seen && seen >= heur) {
      continue;
    } else {
      visited[c.id + " - " + c.remaining] = heur;
    }
    
    // out of time
    if (c.remaining == 0) {
      if (c.score >= max) {
        max = c.score;
        maxnode = c;
      }
      continue;
    }

    // nothing left to open
    if (c.lefttoOpen.length == 0) {
      // stay and loop     
      stack.push(nextNode(c,c.id,false));
      continue;
    }  
    
    if (c.lefttoOpen.indexOf(c.id) != -1) {
      // open valve
      stack.push(nextNode(c,c.id,true));
    } 

    const nextNodes = valves[c.id].next;
    nextNodes.forEach((next) => {
      // just move
      stack.push(nextNode(c,next,false));
    });
  }

  console.log(max);
  console.log(maxnode);
}

function nextNode(current,next,open) {

  var o = {...current.opened};
  var flow = current.currentFlow;
  var score = current.score + current.currentFlow;
  var remaining = current.remaining-1;
  var lefttoOpen = current.lefttoOpen.slice();
  var left = current.left;

  if (open) {
    o[current.id] = current.remaining;
    flow += valves[current.id].rate; 
    var idx = lefttoOpen.indexOf(current.id);
    if (idx != -1) {
      lefttoOpen.splice(idx,1);
    } 
    left -= valves[current.id].rate;
  }
  var n = {
    id : next,
    score : score,
    currentFlow : flow, 
    remaining :remaining,
    opened : o,
    lefttoOpen :  lefttoOpen,
    left : left,
    from : {...current.from}
  }
  n.from[current.id + " - " + n.id + " - " + n.score + " - " + current.remaining] = current.currentFlow;

  return n;

}


function nextTeamNode(current,meNext, elephantNext, meOpen,elephantOpen) {

  
  var o = {...current.opened};
  var flow = current.currentFlow;
  var score = current.score + current.currentFlow;
  var remaining = current.remaining-1;
  var lefttoOpen = current.lefttoOpen.slice();
  var netIncrease = 0;
  var left = current.left;

  if (meOpen) {
    o[current.me] = current.remaining;
    netIncrease = valves[current.me].rate; 
    var idx = lefttoOpen.indexOf(current.me);
    if (idx != -1) {
      lefttoOpen.splice(idx,1);
    } 
    left -= valves[current.me].rate;
  }
  if (elephantOpen) {
    o[current.elephant] = current.remaining;
    netIncrease = valves[current.elephant].rate; 
    var idx = lefttoOpen.indexOf(current.elephant);
    if (idx != -1) {
      lefttoOpen.splice(idx,1);
    } 
    left -= valves[current.elephant].rate;
  }

  flow += netIncrease;

  var n = {
    me : meNext,
    elephant : elephantNext,
    score : score,
    currentFlow : flow, 
    remaining :remaining,
    opened : o,
    left : left,
    lefttoOpen :  lefttoOpen,
    from : {...current.from}
  }
  n.from[current.me + " - " + n.me + " - " + current.elephant + " - " + n.elephant + " - " + n.score + " - " + current.remaining] = current.currentFlow;

  return n;

}

function buildGraph(data) {

  const lines = data.split(/\r?\n/);
  const graph = {};
  lines.forEach(line => {
    const l= line.split(";");

    const c = l[0].split(' ');
    const current = {
      id : c[1],
      rate : parseInt(c[4].replace('rate=','')),
      next : l[1].replace(' tunnels lead to valve','').replace(' tunnel leads to valve','').replace('s ','').replace(/ /g,'').split(',')
    }

    graph[current.id] = current;
    
  });

  return graph;
}

