
const fs = require('fs');

fs.readFile('day17.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

const rocks =[
  [[1,1,1,1]],
  [
    [0,1,0],
    [1,1,1],
    [0,1,0]
  ],
  [
    [0,0,1],
    [0,0,1],
    [1,1,1]
  ],
  [
    [1],
    [1],
    [1],
    [1]
  ],
  [
    [1,1],
    [1,1]
  ]
];
var jets = [];
var jetId = 0;

// Game Object
const game = {
  spritesAtRest : [],
  currentSprite : {},
  grid : [],
  max : 0,
  min : Infinity,
  offset : 0,
  maxHeight : function() {
    var max = -1;
    this.spritesAtRest.forEach((s) => {
      max = Math.max(max,s.topLeft[0]+1)
    });
    return max==-1?0:max;
  },
  generateGrid : function generate() {
    this.grid = [];
    var gridHeight = 7;
    if (game.max!=0) {
      gridHeight = game.max + 7;
    }
    for (var i = 0;i < gridHeight-game.offset;i++) {
      this.grid[i]=[];
      for (var j=0;j<7;j++) {
        this.grid[i][j] = 0;
      }
    }
    this.spritesAtRest.forEach((s) => {
      for (var i=0;i<s.rock.length;i++) {
        for (var j=0;j<s.rock[i].length;j++) {
          if (s.rock[i][j] == 1) {
            this.grid[s.topLeft[0]-i-game.offset][j+s.topLeft[1]] = 1;
          }
        }
      }
    })
  }
}



function solve (data) {

  
  jets = data.split(/\r?\n/)[0].split('');

  var steps = 0;
  const max = 2022;
  game.generateGrid();
  while (steps < max) {

    var rock = rocks[steps%5];    
    var startPosition = [game.maxHeight()+3+rock.length-1,2];

    // move to rest position
    moveToRest(game,rocks[steps%5],startPosition);
    //printGame(game);
    steps++;
    if (steps%(5*jets.lenght) == 0) {
      console.log(steps);
    }
    //printGame(game);
  }
  //printGame(game);
  console.log(game.max);
}

function moveToRest(game,rock,p) {
  // handle gas
  var current = p;
  if (jets[jetId] == ">") {
    if (canMoveRight(game,rock,current)) {
      current[1] += 1;
    }
  } else {
    if (canMoveLeft(game,rock,current)) {
      current[1] -= 1;
    }
  }
  jetId = (jetId+1)%jets.length; 

  // Can move down?
  if (!canMoveDown(game,rock,current)) {
    game.spritesAtRest.push({rock : rock,topLeft : current});
    game.max = Math.max(game.max,current[0]+1);
    game.min = Math.min(game.min,current[0]+1);
    if (game.spritesAtRest.length > 30) { 
      game.spritesAtRest.shift();
      game.offset = Math.max(0,game.min);
    } 
    game.generateGrid();
    return;
  } 
  //move down
  moveToRest(game,rock,[current[0]-1,current[1]]);
}

function printGame(game) {

  // grid
  game.generateGrid()

  for (var row = game.grid.length-1;row >=0; row--) {
    var l = "|";
    line = game.grid[row];
    line.forEach((c) => {
      l += c==0?".":"#";
    });
    console.log(l + "|");
  }
  console.log("+-------+");
}

function canMoveLeft(game,rock,p) {
    //analyse col on the left
    if (p[1] ==  0) {
      // Wall
      return false;
    } else {
      for (var i=0;i<rock.length;i++) {
        for (var j=0;j<rock[i].length;j++) {
          if (rock[i][j] + game.grid[p[0]-i-game.offset][p[1]-1+j] == 2) {
            return false;
          }
        }
      }
      return true;
    }
}

function canMoveRight(game,rock,p) {
  if (p[1]+rock[0].length >= 7) {
      // wall
      return false;
    } else {
        for (var i=0;i<rock.length;i++) {
          for (var j=0;j<rock[i].length;j++) {
            if (rock[i][j] + game.grid[p[0]-i-game.offset][p[1]+1+j] == 2) {
              return false;
            }
          }
        }
        return true;
    }
}

function canMoveDown(game,rock,p) {

  //analyse row underneath bottom of the rock
  if (p[0]-rock.length < 0) {
    // floor
    return false;
  } else {
    for (var i=0;i<rock.length;i++) {
      for (var j=0;j<rock[i].length;j++) {
        if (rock[i][j] + game.grid[p[0]-1-i-game.offset][p[1]+j] == 2) {
          return false;
        }
      }
    }
    return true;
  }
}