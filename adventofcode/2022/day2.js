
const fs = require('fs')

fs.readFile('day2.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});


function solve (data) {

    const lines = data.split(/\r?\n/);
    var total = 0;

    lines.forEach((line) => {
      const items = line.split(" ");
      console.log(score2(items[0],items[1]));
      total += score2(items[0],items[1]);

    });

    console.log("Total " + total);

}

function score(opponnent,player) {

  var scoreTmp = 0;

  switch(opponnent) {
    case "A":
      //Rock
      switch(player) {
        case "X":
          scoreTmp = 3 + 1;
          break;
        case "Y":
          scoreTmp = 6 + 2;
          break;
        case "Z":
          scoreTmp = 0 + 3;
          break;
      }
      break;
    case "B":
      //Paper
      switch(player) {
        case "X":
          scoreTmp = 0 + 1;
          break;
        case "Y":
          scoreTmp = 3 + 2;
          break;
        case "Z":
          scoreTmp = 6 + 3;
          break;
      }
      break;
    case "C":
      //Scissors
      switch(player) {
        case "X":
          scoreTmp = 6 + 1;
          break;
        case "Y":
          scoreTmp = 0 + 2;
          break;
        case "Z":
          scoreTmp = 3 + 3;
          break;
      }
      break;
  }

  return scoreTmp;
}

function score2(opponnent,output) {

  var scoreTmp = 0;

  switch(opponnent) {
    case "A":
      //Rock
      switch(output) {
        case "X":
          scoreTmp = 0 + 3;
          break;
        case "Y":
          scoreTmp = 3 + 1;
          break;
        case "Z":
          scoreTmp = 6 + 2;
          break;
      }
      break;
    case "B":
      //Paper
      switch(output) {
        case "X":
          scoreTmp = 0 + 1;
          break;
        case "Y":
          scoreTmp = 3 + 2;
          break;
        case "Z":
          scoreTmp = 6 + 3;
          break;
      }
      break;
    case "C":
      //Scissors
      switch(output) {
        case "X":
          scoreTmp = 0 + 2;
          break;
        case "Y":
          scoreTmp = 3 + 3;
          break;
        case "Z":
          scoreTmp = 6 + 1;
          break;
      }
      break;
  }

  return scoreTmp;
}