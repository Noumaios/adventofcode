
const fs = require('fs');

fs.readFile('day20.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});



function solve (data) {

  const lines = data.split(/\r?\n/);
  const file = [];
  
  lines.forEach((line) => {
    file.push(parseInt(line));
  });

  console.log(file);
  console.log(file.length);
}




