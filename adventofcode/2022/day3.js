
const fs = require('fs')

fs.readFile('day3.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve2(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    const common = [];
    var score = 0;

    lines.forEach((line) => {

          const mid = Math.ceil(line.length / 2);
          const rs1 = line.slice(0,mid);
          const rs2 = line.slice(mid,line.length);
         
          console.log(rs1);
          console.log(rs2);

          const commonChars = commonItems(rs1,rs2);

          commonChars.split('').forEach(function(c) {
            code = priority(c);
            console.log(c + " " + code);
            score+=code;
          });

    });
    console.log("final score " + score); 
}

function solve2 (data) {

  const lines = data.split(/\r?\n/);
  var groupRucksacks = [];
  var score = 0;
  var group = 0;

  lines.forEach((line) => {
       
    if (group == 2) {
      // deal with the group
      groupRucksacks.push(line);
      const com = findBadge(groupRucksacks);
      score += priority(com);
      
      group = 0;
      groupRucksacks = [];
    } else {
      group++;
      groupRucksacks.push(line);
    }

  });
  console.log("final score " + score); 
}

function commonItems(rs1,rs2) {

  var str = "";
  rs1.split('').forEach(function(item) {
    if (rs2.indexOf(item) != -1) {
      if (str.indexOf(item) == -1) {
        str += item;
      }
    }
  });

  return str
}

function findBadge(group) {

  console.log(group);
  var str = "";
  group[0].split('').forEach(function(item) {
    if (group[1].indexOf(item) != -1 && group[2].indexOf(item) != -1) {
      if (str.indexOf(item) == -1) {
        str+=item;
      }
    }
  });

  return str;
}

function priority(c) {
  var code = c.charCodeAt(0);
  if (code > 96) {
    code = code-96;
  } else {
    code = code-38;
  }
  return code;
}