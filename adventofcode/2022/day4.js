
const fs = require('fs')

fs.readFile('day4.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    var count =0 ;
    var countOverlap=0;

    lines.forEach((line) => {
        const sections = line.split(',');

        const section1 = sections[0].split('-'); 
        const section2 = sections[1].split('-');

        if (contains(section1,section2) || contains(section2,section1)) {
          count++;
        } 
        if (overlap(section1,section2) || overlap(section2,section1)) {
          countOverlap++
        }

    });

    console.log(count);
    console.log(countOverlap);
}

// retunr trun if sec1 is contained in sec2
function contains(sec1,sec2) {
  console.log("testing [" + sec1 + "] and [" + sec2 + "]");
  if (parseInt(sec1[0]) >= parseInt(sec2[0])  && parseInt(sec1[1])  <= parseInt(sec2[1])){
    return true;
  } else {
    return false;
  }
}

function overlap(sec1,sec2) {

  if (parseInt(sec1[0])>=parseInt(sec2[0]) && parseInt(sec1[0])<=parseInt(sec2[1])) {
    console.log("Overalap");
    return true;
  } else if (parseInt(sec1[1])>=parseInt(sec2[0]) && parseInt(sec1[1])<=parseInt(sec2[1])) {
    console.log("Overalap");
    return true;
  } else {
    return false;
  }
}