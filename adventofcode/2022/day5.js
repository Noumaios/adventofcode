
const fs = require('fs')

fs.readFile('day5.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    var cratesReady = false;
    const crates = {};

    lines.forEach((line) => {

        if (line == "") {
          // finish with crates
          cratesReady=true;
          return;
        }

        if (!cratesReady) {
          parseCrateLine(crates,line);
        } else {
          // Move crates
          move(crates,line);
        }
    });

    var str = "";
    for (idx in crates) {
      str += crates[idx].pop();
    }

    console.log(str);

}

function parseCrateLine(crates,line) {

  const l = line.split('');
  var col = 1;
  
  for (var i=0; i<l.length;i++) {
    if (l[i] == "[") {
      if (!crates[i/4+1]) {
        crates[i/4+1] = [];
      }
      crates[i/4+1].splice(0,0,l[i+1]);
    }
  }
} 

function move(crates,line) {

   const l = line.split(' ');
   var num = 0;
   var from = 0;
   var  to = 0;
   var type = "";
   
   console.log(crates);

   l.forEach(function(c) {
    switch (type) {
      case "move":
        num = parseInt(c);
        break;
      case "from":
        from = c;
        break;
      case "to":
        to = c;  
    }
    type = c;
   });

   console.log("will move " + num + " crates from " + from + " to " + to);
  //  for (var j=0; j<num;j++) {
  //   crates[to].push(crates[from].pop());
  //  }
  // add the array
  crates[to] = crates[to].concat(crates[from].slice(crates[from].length - num,crates[from].length));
  //remove elements
  crates[from].splice(crates[from].length - num,num);
  console.log(crates);
  console.log("-------------");
}