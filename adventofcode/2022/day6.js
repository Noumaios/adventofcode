
const fs = require('fs')

fs.readFile('day6.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    var markerPosition = 0;

    lines.forEach((line) => {
      
      const l = line.split('');
      
      for (var i=0; i<l.length; i++) {
    
        if (i >= 13) {

          if (isMarker(line.substring(i-13,i+1))) {
            console.log("Found marker");
            markerPosition = i+1;
            return;
          }

        }
      }
    });

    console.log(markerPosition);
}

function isMarker(sequence) {

  console.log("testing " + sequence);
  for (var i=0; i<sequence.length-1;i++) {
    if (sequence.substring(i+1,sequence.length).indexOf(sequence[i]) != -1) {
      return false;
    }
  }

  return true;
}