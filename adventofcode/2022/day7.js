
const { dir } = require('console');
const fs = require('fs');

var currentNode = {
  name : "/",
  size:0,
  type:"dir",
  nodes:[]
};
var ext2 = currentNode;
var pwd = ["/"];
var total = 0;
var dirToDelete = [];

fs.readFile('day7.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});


function solve (data) {

    const lines = data.split(/\r?\n/);

    buildTree(lines);

    printNode(ext2,"");
    // part 1
    find100kNode(ext2);
    console.log("100k max dirs size : " + total);

    // part 2
    var free = 70000000 - ext2.size;
    var needed = 30000000 - free;

    console.log("free space " + free);
    console.log("need to find : " + needed);

    
    findSpace(ext2,needed);
    dirToDelete.sort(function(a,b) {
      if (a.size < b.size) {
        return -1;
      } else {
        return 1;
      }
    })

    console.log(dirToDelete[0].size);

}
 
function buildTree(lines) {

  var lineNum = 0;
  while (true) {
    // eof
    if (lineNum == lines.length) {
      break;
    }
    
    // interpret line
    const l = lines[lineNum].split(' ');
   
    if (l[0] = "$") {
      const cmd = l[1];
      const attr = l[2];

      if (cmd === "cd") {
        cd(attr);
        lineNum++;
      } else if (cmd === "ls") {
        lineNum++;
        
        // interpret output from ls
        while (true) {
          if (lineNum === lines.length) {
            break;
          }
          const l = lines[lineNum].split(' ');
          if (l[0] == "$") {
            break;
          } else {
            addFile(currentNode,l);
            lineNum++; 
          }
        }

      }

    }
  }
}


function findSpace(node,need) {

  if (node.type == "dir") {
    if (node.size >= need) {
      dirToDelete.push(node);
    }
  }
  node.nodes.forEach(n => {
    findSpace(n,need);
  });
}


function printNode(node,tabs) {

  console.log(tabs + " - " + node.name + " (" + node.type + ",size=" + node.size + ")");
 
  node.nodes.forEach(n => {
    printNode(n,tabs + "  ");
  });

}

function find100kNode(node) {

  if (node.type == "dir") {
    if (node.size < 100000) {
      total += node.size;
    }
  } 

  node.nodes.forEach(n => {
    find100kNode(n);
  });

}

function cd(attr) {

  if (attr == "/" ) {
    currentNode = ext2;
    pwd = ["/"];
  } else if (attr == "..") {
    currentNode = currentNode.parent;
    pwd.pop();
  } else {
    var next = currentNode.nodes.find(c => c.name === attr)
    if (!next) {
      next = {
        name : attr,
        type : "dir",
        parent : currentNode,
        nodes : [],
        size : 0
      }
    }
    currentNode = next;
    pwd.push(attr);
  }

}

function addFile(currentnode,l) {

  // If node doesn't exist, add it
  var node = currentNode.nodes.find(c => c.name === l[1])
  if (!node) {
    if(l[0] === "dir") {    
      node =  {
        name : l[1],
        type : "dir",
        parent : currentNode,
        nodes : [],
        size : 0
      }
    } else {
      node =  {
        name : l[1],
        type : "file",
        parent : currentNode,
        nodes : [],
        size : parseInt(l[0])
      }
    }

    // update size of current dir + all parents
    currentNode.size += node.size;
    var n = currentNode.parent
    while (n) {
      n.size += node.size;
      n = n.parent;
    }
    currentNode.nodes.push(node);
  }

}

