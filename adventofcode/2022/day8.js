
const fs = require('fs');

fs.readFile('day8.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

const forest = [];

function solve (data) {

    const lines = data.split(/\r?\n/);
   
    var x,y=0;

    lines.forEach((line) => {      
      const l = line.split('');
      var arr =[];
      l.forEach((tree) => {
        arr.push(parseInt(tree));
      });
      forest.push(arr);
    });

    console.log(forest);
    var count=0;
    var max = 0;
    var tmp = 0;
    for (var x=0; x < forest.length; x++ ) {
      for (var y=0; y< forest[x].length; y++) {
        console.log("testing " + x+" " + y);
        if (isVisible(x,y)) {
          count++;
        }
        tmp = crunchScore(x,y);
        if (tmp >= max) {
          max = tmp;
        }
        console.log(x+","+y+ "  " + tmp)
      }
    }

    console.log(count);
    console.log(max);

}

function isVisible(x,y) {

  // border tree
  if (x == 0 ||  y == 0 || x == forest.length-1 || y == forest[0].length - 1) {
    return true;
  }

  // brute force it
  const height = forest[x][y];
  var visible = true;

  for (var i=x-1;i>=0;i--) {
    if (forest[i][y] >= height) {
      visible = false;
    }
  }
  if (visible) {
    return true;
  }

  visible = true;
  for (var i=x+1;i<forest.length;i++) {
    if (forest[i][y] >= height) {
      visible = false;
    }
  }
  if (visible) {
    return true;
  }

  visible = true;
  for (var i=y-1;i>=0;i--) {
    if (forest[x][i] >= height) {
      visible = false;
    }
  }
  if (visible) {
    return true;
  }

  visible = true;
  for (var i=y+1;i<forest[x].length;i++) {
    if (forest[x][i] >= height) {
      visible = false;
    }
  }

  if (visible) {
    return true;
  }
  return false;
}


function crunchScore(x,y) {

  var score = 1;
  var trees = 0;
  // go up
   
  for (var i=x-1;i >= 0; i--) {
    trees++;
    if (forest[i][y] >= forest[x][y]) {
      break;
    }
    if (i == 0) { 
      break;
    } 
  }
  score *= trees;
  trees=0;

  // go down
  for (var i=x+1;i<forest.length; i++) {
    trees++;
    if (forest[i][y] >=  forest[x][y]) {
      break;
    }
    if (i == forest.length-1) {
      break;
    } 
  }
  score *= trees;
  trees=0;

  // go left
  for (var i=y-1;i >=0; i--) {
    trees++;
    if( forest[x][i] >=  forest[x][y]) {
      break;
    }
    if (i== 0) {
      break;
    } 
  }
  score *= trees;
  trees=0;

  // go right
  for (var i=y+1;i<forest[x].length; i++) {
    trees++;
    if(forest[x][i] >=  forest[x][y]) {
      break;
    }
    if (i== forest[x].length-1) {
      break;
    } 
  }
  score *= trees;

  return score;

}

