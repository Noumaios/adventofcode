
const fs = require('fs');

fs.readFile('day9.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve2(data);
});

const moves = {};
var headPosition = [0,0];
var tailPosition = [0,0];
var snake = [[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]

function solve (data) {

    const lines = data.split(/\r?\n/);
    
    moves[tailPosition[0]+","+tailPosition[1]] = 1;
    lines.forEach((line) => {
      
      const l = line.split(' ');
      
      moveRope(l[0],l[1]);

    });
    console.log(Object.keys(moves).length);

}

function solve2(data) {

  const lines = data.split(/\r?\n/);
    
  moves[snake[9][0]+","+snake[9][1]] = 1;
  lines.forEach((line) => {
    
    const l = line.split(' ');
    
    moveSnake(l[0],l[1]);

  });
  console.log(Object.keys(moves).length);
}

function moveSnake(direction,steps) {
  
  console.log("moving to " + direction + " steps " + steps);
  
  for (var i=0; i< steps; i++) {
    switch (direction) {
      case "L":
        snake[0][1]--;
        break;
      case "R":
        snake[0][1]++;
        break;
      case "U":
        snake[0][0]++;
        break;
      case "D":
        snake[0][0]--;
        break;
    }
    // move tail if needed
    // i.e. manathan distance > 2
    for (var j=1;j<snake.length;j++) {
      hpos = snake[j-1];
      tpos = snake[j];

      if (!touching(hpos,tpos)) {
        console.log("move tail");
        if (sameLine(hpos,tpos)) {
            tpos[1] += (hpos[1]-tpos[1]) / Math.abs(hpos[1]-tpos[1]);      
  
        } else if (sameColumn(hpos,tpos)) {
            tpos[0] += (hpos[0]-tpos[0]) / Math.abs(hpos[0]-tpos[0]);      
        } else {
          tpos[0] += (hpos[0]-tpos[0]) / Math.abs(hpos[0]-tpos[0])
          tpos[1] += (hpos[1]-tpos[1]) / Math.abs(hpos[1]-tpos[1])
        }
      }
    }
    
    if (!moves[snake[9][0]+","+snake[9][1]]) {
      moves[snake[9][0]+","+snake[9][1]] = 1
    }
  }
}

function moveRope(direction,steps) {

  console.log("moving to " + direction + " steps " + steps);
  
  for (var i=0; i< steps; i++) {
    switch (direction) {
      case "L":
        headPosition[1]--;
        break;
      case "R":
        headPosition[1]++;
        break;
      case "U":
        headPosition[0]++;
        break;
      case "D":
        headPosition[0]--;
        break;
    }
    
    console.log(headPosition);
  
    if (!touching(headPosition,tailPosition)) {
      console.log("move tail");
      if (sameLine(headPosition,tailPosition)) {
          tailPosition[1] += (headPosition[1]-tailPosition[1]) / Math.abs(headPosition[1]-tailPosition[1]);      

      } else if (sameColumn(headPosition,tailPosition)) {
          tailPosition[0] += (headPosition[0]-tailPosition[0]) / Math.abs(headPosition[0]-tailPosition[0]);      
      } else {
        tailPosition[0] += (headPosition[0]-tailPosition[0]) / Math.abs(headPosition[0]-tailPosition[0])
        tailPosition[1] += (headPosition[1]-tailPosition[1]) / Math.abs(headPosition[1]-tailPosition[1])
      }
      if (!moves[tailPosition[0]+","+tailPosition[1]]) {
        moves[tailPosition[0]+","+tailPosition[1]] = 1
      }
    }
    console.log(tailPosition);
  }

}




function distance(p1,p2) {
  return (Math.abs(p2[0]-p1[0]) + Math.abs(p2[1]-p1[1]));
}

function sameLine(p1,p2) {
  if (p1[0] == p2[0]) {
    return true;
  }
  return false;
}
function sameColumn(p1,p2) {
  if (p1[1] == p2[1]) {
    return true;
  }
  return false;
}


function touching(p1,p2) {
  
  if (sameLine(p1,p2) || sameColumn(p1,p2)) {
    if (distance(p1,p2) <= 1) {
      return true;
    } else {
      return false
    }
  }  else {
    if (distance(p1,p2) <= 2) {
      return true;
    } else {
      return false
    }
  }
}
