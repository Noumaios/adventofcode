
const fs = require('fs')

fs.readFile('day1.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

const digits = ["one","two","three","four","five","six","seven","eight","nine"];
const digitsR = ["eno","owt","eerht","ruof","evif","xis","neves","thgie","enin"];

function solve (data) {

    const lines = data.split(/\r?\n/);
    const values = [];

    lines.forEach((line) => {
        calibrationLine = line.split("");
        var first,last=0;
        
        first = findFirstDigit(calibrationLine);
        calibrationLineReversed = calibrationLine.reverse();
        last = findFirstDigit(calibrationLineReversed);
      
        values.push(first*10 + last);
    });

    var sum =0;
    values.forEach((val)=> {
      sum+=val;
    })
    console.log(sum);
}

function findFirstDigit(arr) {
  var str = "";

  for (var i=0; i< arr.length; i++) {
    var val = arr[i];
    str += arr[i];

    for (var j=0; j<digits.length;j++) {
      if (str.indexOf(digits[j]) != -1) {
        return j+1;
      }
    }
    for (var j=0; j<digitsR.length;j++) {
      if (str.indexOf(digitsR[j]) != -1) {
        return j+1;
      }
    }

    if (parseInt(val)) {
      return parseInt(val);
    }
  }
}
