
const fs = require('fs')

fs.readFile('day10.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});


function solve (data) {

  console.time('1');

  const lines = data.split(/\r?\n/);
  const game = loadGames(lines);


  const steps1 = findSteps(game);
  game.loop = [...game.visited];

  //showGame(game);
  

  const steps2 = findInnerTiles(game);
  showGame(game);
  

  console.log("Part 1:" + steps1);
  console.log("Part 2:" + steps2);
  
  console.timeEnd('1');
}

function showGame(game) {


  game.zones.forEach((zone) => {
      zone.tiles.forEach(tile => {
        game.map[tile.line][tile.col] = zone.outside?"O":"I";
      });
  });
  game.map.forEach((line,l) => {
    let str = "";
    line.forEach((c,col) => {
      if (game.loop && game.loop.findIndex(p => p.line == l && p.col == col) != -1) {
        str+="\x1b[92m"+c+ "\x1b[39m"+"\x1b[0m";
      } else {
        str+=c;
      }
    })
    console.log(str);
  })
}

function findSteps(game) {

  game.visited = [];
  let prev = game.start;
  
  game.visited.push(prev);

  // get the pipes from S 
  const queue = getPipesFromPoint(game.start,game);
  console.log(queue);

  while(queue.length > 0) {

    const pos = queue.shift();
    
  
    // been there?
    let idx = game.visited.findIndex(p => p.line == pos.line && p.col == pos.col);
    if (idx != -1) {
      return pos.steps;
      break;
    }

    game.visited.push(pos);

    const next = goNext(pos,game);
    next.steps++;
    queue.push(next);
  }


}

function getPipesFromPoint(p,game) {
  const pipes = [];

  // UP
  if (p.line > 0) {
    const pos = game.map[p.line-1][p.col];
    if (pos == "|" || pos == "F" || pos == "7") {
      pipes.push({line:p.line-1,col:p.col,prev:p,steps:1});
    }
  }
  //Down
  if (p.line < game.map.length -1) {
    const pos = game.map[p.line+1][p.col];
    if (pos == "|" || pos == "J" || pos == "L") {
      pipes.push({line:p.line+1,col:p.col,prev:p,steps:1});
    }
  }
   //Left
   if (p.col > 0) {
    const pos = game.map[p.line][p.col-1];
    if (pos == "-" || pos == "F" || pos == "L") {
      pipes.push({line:p.line,col:p.col-1,prev:p,steps:1});
    }
  }
   //Right
   if (p.col < game.map[0].length -1) {
    const pos = game.map[p.line][p.col+1];
    if (pos == "-" || pos == "J" || pos == "7") {
      pipes.push({line:p.line,col:p.col+1,prev:p,steps:1});
    }
  }
  //game.map[p.line][p.col]="|";

  return pipes;
}

function goNext(point,game) {

  const pos = game.map[point.line][point.col];
  const prev = point.prev;
  const next = {...point};

  next.prev = {line:point.line,col:point.col};

  switch(pos) {
    case "|":
      next.line = point.line + (point.line-prev.line);
      next.col = point.col;
      break;
    case "-":
      next.line = point.line;
      next.col = point.col + (point.col - prev.col);
      break;
    case "L":
      if (point.line ==  prev.line) {
        next.line = point.line-1;
        next.col = point.col;
      } else {
        next.line = point.line;
        next.col = point.col+1;
      }
      break;
    case "7":
      if (point.line ==  prev.line) {
        next.line = point.line+1;
        next.col = point.col;
      } else {
        next.line = point.line;
        next.col = point.col-1;
      }
      break;
    case "J":
      if (point.line ==  prev.line) {
        next.line = point.line-1;
        next.col = point.col;
      } else {
        next.line = point.line;
        next.col = point.col-1;
      }
      break;
    case "F":
      if (point.line ==  prev.line) {
        next.line = point.line+1;
        next.col = point.col;
      } else {
        next.line = point.line;
        next.col = point.col+1;
      }
      break;  
  } 
  return next;
}

function loadGames(lines) {

  const map = [];
  const start = {};
  
  for (let i=0;i<lines.length;i++) {
    const l = lines[i].split("");
    const c = l.indexOf("S");
    if (c != -1) {
      start.line = i;
      start.col = c; 
    }
    map.push(l);
  }

  return {map:map,
          start:start,
        zones : []};
} 

function findInnerTiles(game) {

  const queue = game.map;
  let insideTiles = 0;
  game.nodeVisited = [];

  // while we still have points to explore
  game.map.forEach((line,l) => {
    line.forEach((col,c) => { 
      // been there?
      let idx = game.nodeVisited.findIndex(p => p.line == l && p.col == c);
      if (idx == -1) {
        insideTiles += getZoneStatus({line:l,col:c},game);
      }
    });
  });

  return insideTiles;
}

// return the number of tiles + status (outside / inside)
function getZoneStatus(pos,game) {

  const verticalArr = ["||","|L",,"|F","J|", "JL","JF","7|","7F","7L"];
  const horizontalArr = ["--","-7","-F","J-","JF","J7","L-","LF","L7"];

  const VERTICAL = 1;
  const HORIZONTAL = 2;

  let debug = false;
  

  const queue = [];
  const zone ={
    outside : false,
    tiles : []
  }

  queue.push(pos);

  while (queue.length > 0) {
  
    let pathStart;
    const curr = queue.shift();
    
    let pipeIdx = game.nodeVisited.findIndex(p => curr.line == p.line && curr.col == p.col);
    if (pipeIdx != -1) {
      continue;
    }

    game.nodeVisited.push(curr);

    //been there stop AND not squeezing through pipes
    let idx = game.loop.findIndex(p => curr.line == p.line && curr.col == p.col);
    let path = false;
    let direction = 0;
    
    if (curr.line == 106 && curr.col == 130) {
      console.log(curr);
      console.log(game.map[curr.line][curr.col]);
      console.log("idx" + idx);
     // console.log(zone);
    }

    if (idx != -1) {
      if (curr.prev && curr.prev.line != curr.line && curr.prev.col == curr.col) {  
          let startCol = curr.pathStart?curr.pathStart:Math.max(0,curr.col-1);

          for (let j=startCol;j<=Math.min(game.map[0].length-2,curr.col);j++) {
            let pos = game.map[curr.line][j]+game.map[curr.line][j+1];
           // if (debug) console.log(pos);
           if (curr.line == 106 && curr.col == 130) {
            console.log(pos);
          }
            if (verticalArr.indexOf(pos) != -1) {
          
              path = true;
              pathStart=j;
              direction = VERTICAL;
              break;
            } 
          }

      } else if (curr.prev && curr.prev.line == curr.line && curr.prev.col != curr.col) {  
        let startLine = curr.pathStart?curr.pathStart:Math.max(0,curr.line-1);

        for (let j=startLine;j<=Math.min(game.map.length-2,curr.line);j++) {
          let pos = game.map[j][curr.col]+game.map[j+1][curr.col];
         // if (debug) console.log(pos);
          if (horizontalArr.indexOf(pos) != -1) {
            
            path = true;
            pathStart=j;
            direction = HORIZONTAL;
            break;
          }
        }
      }

      if (!path) {
        if (curr.line == 106 && curr.col == 130) {
          console.log("continue");
        }
        continue;
      }
    }

    if (!path) {
      zone.tiles.push(curr);
    }

    if (curr.line == 0 || curr.line == game.map.length-1 || curr.col==0 || curr.col == game.map[0].length-1) {
      
      zone.outside = true;
      zone.curr = curr;
    }

    game.nodeVisited.push(curr);

    if (curr.line == 106 && curr.col == 130) {
      console.log("path " +path);
    }
    // find next
    for (let i=Math.max(0,curr.line-1);i<=Math.min(game.map.length-1,curr.line+1);i++) {
      for (let j=Math.max(0,curr.col-1);j<=Math.min(game.map[0].length-1,curr.col+1);j++) {
        if ((i!= curr.line || j != curr.col)) {

          if (path) {
            if (direction == VERTICAL) {
              
              let idx1 = game.loop.findIndex(p => i == p.line && pathStart == p.col);
              let idx2 = game.loop.findIndex(p => i == p.line && pathStart+1 == p.col);

              if (i == curr.line) {
                continue;
              }
              if (idx1 != -1) {
                if (idx2 != -1) {
                  if (j!= pathStart) {
                    continue;
                  }
                } else {
                  if (j <pathStart) {
                    continue;
                  }
                }
              } else {
                if (idx2 != -1) {
                  // if (j < pathStart) {
                  //   continue;
                  // }
                } 
              }

            } else if (direction == HORIZONTAL) {
              let idx1 = game.loop.findIndex(p => pathStart == p.line && j == p.col);
              let idx2 = game.loop.findIndex(p => pathStart+1 == p.line && j == p.col);
              if (j == curr.col) {
                continue;
              }
              if (idx1 != -1) {
                if (idx2 != -1) {
                  if (i!= pathStart) {
                    continue;
                  }
                } else {
                  if (i <pathStart) {
                    continue;
                  }
                }
              } else {
                if (idx2 != -1) {
                  // if (i > pathStart) {
                  //   continue;
                  // }
                } 
              }
            }
          }
          if (curr.line == 106 && curr.col == 130) {
            console.log(i + " " + j + " " + pathStart);
          }
          queue.push({
            line:i,
            col :j,
            pathStart : pathStart,
            prev : {line:curr.line,col:curr.col}
          });
        }
      }
    } 
  }
  

  
  game.zones.push(zone);
  // if (zone.tiles.length > 0) {
  // console.log("Zone done")
  // console.log(zone.outside);
  // zone.tiles.forEach(t => {
  //   console.log(t);
  // })
  // if (zone.outside) {
  // console.log("Border");
  // console.log(zone.curr);
  // }
//}

  if (!zone.outside) {
   // console.log(zone.tiles.length);
    return zone.tiles.length;
  } else {
    return 0;
  }

}


// return the number of tiles + status (outside / inside)
function getZoneStatus2(pos,game) {

  const verticalArr = ["||","|L",,"|F","J|", "JL","JF","7|","7F","7L"];
  const horizontalArr = ["--","-7","-F","J-","JF","J7","L-","LF","L7"];

  const VERTICAL = 1;
  const HORIZONTAL = 2;

  let debug = false;
  

  const queue = [];
  const zone ={
    outside : false,
    tiles : []
  }

  queue.push(pos);

  while (queue.length > 0) {
  
    let pathStart;
    const curr = queue.shift();
    
    // been there already.
    let pipeIdx = game.nodeVisited.findIndex(p => curr.line == p.line && curr.col == p.col);
    if (pipeIdx != -1) {
      continue;
    }
  
    let loopIdx = game.loop.findIndex(p => curr.line == p.line && curr.col == p.col);
    if (loopIdx != -1) {
      continue;
    }

    if (true) {
      zone.tiles.push(curr);
    }

    if (curr.line == 0 || curr.line == game.map.length-1 || curr.col==0 || curr.col == game.map[0].length-1) {
      zone.outside = true;
      zone.curr = curr;
    }

    game.nodeVisited.push(curr);


    // find next
    for (let i=Math.max(0,curr.line-1);i<=Math.min(game.map.length-1,curr.line+1);i++) {
      for (let j=Math.max(0,curr.col-1);j<=Math.min(game.map[0].length-1,curr.col+1);j++) {

        if (canMove(game,curr,i,j)) {
          queue.push({
            line:i,
            col :j,
            prev : {line:curr.line,col:curr.col}
          });
        }
      }
    }
  }
  
  if (zone.tiles.length > 0) {
     console.log(zone);
     game.zones.push(zone);
  }

  if (!zone.outside) {
    return zone.tiles.length;
  } else {
    return 0;
  }

}

function canMove(game,current,nextline,nextCol) {

  // can be different from current position
  if (current.line == nextline && current.col == nextCol) {
    return false
  }

  let nextOnLoop = game.loop.findIndex(p => nextline == p.line && nextCol == p.col);
  let currOnLoop = game.loop.findIndex(p => curr.line == p.line && curr.col == p.col);
  if (currOnLoop ==-1 && nextOnLoop == -1) {
    return true;
  } else {
  // Direction 
    const direction = (next.line - curr.line)*10 + (next.col - curr.col);
    switch (direction) {
      case -9 :
        //NW
      case -1 : 
        //W
      case 9 : 
        //SW
      case -10:
        // N
      case -11:
        //NE
      case 1:
        //E
      case 11:
        //SE
      case 10:
        //S
    }


  }

  

}