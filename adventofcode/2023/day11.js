
const fs = require('fs')

fs.readFile('day11.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});


function solve (data) {

  console.time('1');

  const lines = data.split(/\r?\n/);
  const game = loadGames(lines);
  
  showGame(game);

  const one = getSumShortestPath(game,2);
  const two = getSumShortestPath(game,1000000);;

  console.log("Part 1:" + one);
  console.log("Part 2:" + two);
  
  console.timeEnd('1');
}

function showGame(game) {

  game.universe.forEach((line,l) => {
    let str = "";
    line.forEach((c,col) => {
      if (c.point != '.') {
        str+="\x1b[92m"+c.point+ "\x1b[39m"+"\x1b[0m";
      } else {
        str+=c.point;
      }
    })
    console.log(str);
  })
}

function loadGames(lines) {

  const map = [];
  const galaxies = [];
  
  lines.forEach((line,i) => {
    const l = line.split("");
    const points = [];
    l.forEach((point,c) => {
      if (point!= '.') {
        galaxies.push({line:i,col:c});
      }
      points.push({point:point,
                  distanceTo:[]});
    });
    map.push(points);
  });

  // empty rows
  const emptyCols = [];
  const emptyLines = [];

  // find empties
  for (let i=0;i<map[0].length; i++) {
    const gal = galaxies.filter(p => p.col == i);
    if (gal.length == 0) {
      emptyCols.push(i);
    }
  }
  for (let i=0; i<map.length;i++) {
    const gal = galaxies.filter(p => p.line == i);
    if (gal.length == 0) {
      emptyLines.push(i);
    }
  }

  return {universe:map,
        galaxies:galaxies,
        emptyLines : emptyLines,
        emptyCols : emptyCols
      };
}

function getSumShortestPath(game,factor) {

  let sum = 0;

  for(let i=0; i<game.galaxies.length-1;i++) {
    for (let j=i+1; j<game.galaxies.length;j++) {

      let ret = pathBetweenPoints(game,game.galaxies[i],game.galaxies[j],factor);
      sum += ret;
    }
  }
  return sum;
}

function pathBetweenPoints(game,startPoint,destination,factor) {

  const expLines = game.emptyLines.filter(v => (v>startPoint.line && v<destination.line) ||
                                               (v<startPoint.line && v>destination.line));
  const expCols = game.emptyCols.filter(v => (v>startPoint.col && v<destination.col) || 
                                              (v<startPoint.col && v>destination.col));

  const lineDistance = Math.abs(startPoint.line - destination.line) + expLines.length*(factor - 1);
  const colDistance = Math.abs(startPoint.col - destination.col) + expCols.length*(factor - 1);

  return lineDistance+colDistance;
  
}
