
const fs = require('fs')

fs.readFile('day12.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});


function solve (data) {

  console.time('1');

  const lines = data.split(/\r?\n/);
  const game = loadGames(lines);
  game.results = [];

  console.log(game);

  const one = getBrokenReport(game);
  const two = getUnfoldedBrokenReport(game);

  console.log("Part 1:" + one);
  console.log("Part 2:" + two);
  
  console.timeEnd('1');
}

function getBrokenReport(game) {

  let sum = 0;
  game.map.forEach((line,l) => {
    console.log("Calculation for " + line + " : " + game.damaged[l]);
    let arrangement = arrangementsForLine([...line],game.damaged[l],0,0);
    game.results.push(arrangement);

    console.log(arrangement)
    console.log("    ");

    sum += arrangement;
  });

  return sum;

}

function getUnfoldedBrokenReport(game) {

  let sum = 0;
  
  game.map.forEach((line,l) => {
    
   // console.log("" +line);
    
    const unfoldedMap = [...line,"?",...line];
    const unfoldedReport = [...game.damaged[l],...game.damaged[l]]; 
    const unfoldedMap2 = [...line,"?"];
    const unfoldedReport2 = [...game.damaged[l]]; 
    const unfoldedMap3 = [...line,"?",...line,"?",...line];
    const unfoldedReport3 = [...game.damaged[l],...game.damaged[l],...game.damaged[l]]; 
    const unfoldedMap4 = [...line,"?",...line,"?",...line,"?",...line];
    const unfoldedReport4 = [...game.damaged[l],...game.damaged[l],...game.damaged[l],...game.damaged[l]]; 
    const unfoldedMap5 = [...line,"?",...line,"?",...line,"?",...line,"?",...line];
    const unfoldedReport5 = [...game.damaged[l],...game.damaged[l],...game.damaged[l],...game.damaged[l],...game.damaged[l]]; 
    console.log("Unfoled Calculation for " + unfoldedMap + " : " + unfoldedReport);
    
    let res = arrangementsForLine(unfoldedMap,unfoldedReport,0,0);
    let res3 = arrangementsForLine(unfoldedMap3,unfoldedReport3,0,0);
    let res4 = arrangementsForLine(unfoldedMap4,unfoldedReport4,0,0);
    let res5 = arrangementsForLine(unfoldedMap5,unfoldedReport5,0,0);

    console.log(game.results[l] + "  " + res +  " " + res3+  " " + res4+  " " + res5);
    let arrangement = game.results[l]*Math.pow(res/game.results[l],4);
    console.log(arrangement);
    console.log("    ");

    sum += arrangement;
  });
  // let i= 5;
  // console.log(" Cal " + game.map[i] + " : " + game.damaged[i]);
  // console.log(arrangementsForLine(game.map[i],game.damaged[i],0,0));

  return sum;

}

function arrangementsForLine(line,rep,lineIndex,repIndex) {

  // console.log("Line " + line + " : (" + rep + ") " + lineIndex + " : " + repIndex);

  const block = [];
  let idx = 0;
  let satisfied = false;

  // find next block
  for (let i=lineIndex ; i<line.length; i++) {
    source = line[i];
    idx=i;

    // build next block
    if (source == "#" || source == "?") {
      block.push(source);
    }      
    if (source == "." && block.length > 0) {
      break;
    }
    if (rep[repIndex] && block.length > rep[repIndex]) {
      break;
    }
  }

  if (block.length == 0) {
    return 0;
  }

  if ((block.length == rep[repIndex] && !block.includes("?"))) {
    //console.log("Satisfied");
    satisfied = true;
  } else {
    if (block.includes("#")) { 
      if (block.length < rep[repIndex]) {
        // not enough ? to make it
        return 0;
      } else {
        if (!block.includes("?") && block.length > rep[repIndex]) {
          return 0;
        }
      }
    } 
  }


  // If valid and Done checking
  // i.e. no # left 
  if (repIndex == rep.length-1 && satisfied) {
    if (line.includes("#",idx+1)) {
      return 0;
    } else {
      //console.log("WIN " + line);
      return 1;
    }
  }
  
  
  if (satisfied) {
    lineIndex = idx+1;
    repIndex++;
  } 

  const i = line.indexOf("?");
  if (i == -1) {
    return arrangementsForLine(line,rep,lineIndex,repIndex) ;
  }

  line.splice(i,1,"#");
  const newBroken = [...line];
  line.splice(i,1,".");
  const newWorking = [...line];  
  
  return arrangementsForLine(newBroken,rep,lineIndex,repIndex) + arrangementsForLine(newWorking,rep,lineIndex,repIndex)

}


function loadGames(lines) {

  const map = [];
  const damaged = [];
  
  lines.forEach((line,i) => {
    const l = line.split(" ");
    
    const points = l[0].split("");
    map.push(points);
    
    const report = l[1].split(",").map(v => parseInt(v));
    damaged.push(report);
  });

  return {map : map,damaged:damaged}
}

