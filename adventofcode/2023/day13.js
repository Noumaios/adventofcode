
const fs = require('fs')

fs.readFile('day13.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});


function solve (data) {

  console.time('1');

  const lines = data.split(/\r?\n/);
  const game = loadGames(lines);

  //console.log(game);

  const one = getMirrors(game);
  const two = getSumdgeAndMirrors(game);

  console.log("Part 1:" + one);
  console.log("Part 2:" + two);
  
  console.timeEnd('1');
}

function getMirrors(game) {
  
  let total = 0;

  game.forEach((map,i) => {

    const transposedMap = transpose(map);

    let r = getSymetricLinesCount(map);
    let c = getSymetricLinesCount(transposedMap);
  
    console.log("Map " + i + "\tline : "+r+" \tcol : " +c);
    total += r*100 + c;
    
  });

  return total;
}

function getSumdgeAndMirrors(game) {
  
  let total = 0;

  
  game.forEach((grid,i) => {

    const newMirror = fixSmudge(grid);

    if (newMirror.smudge) {
      let r = newMirror.line;
      total += r*100;
    } else {
      let transposedMap = fixSmudge(transpose(grid));
      let c =transposedMap.line;
      total += c
    }
    
  });

  return total;
}

function getSymetricLinesCount(map) {

  for (let i=0; i<map.length-1;i=i+1) {
    if(compareLines(map,i,i+1)) {
      // if border
      if (i==0 || i+1 == map.length-1) {
        return i+1;
      }
      //expand until finding a border
      for (let j =1;(i-j) >=0 && (i+j+1) < map.length ;j++) {
        if (compareLines(map,i-j,i+j+1)) {
            if (i-j == 0 || i+j+1 == map.length-1) {
              return i+1;
            }
          } else {
            break;
          }
        }
      }
    }

  return 0;
} 


function compareLines(map,i,j) {
  for (let l=0;l<map[i].length; l++){
    if (map[i][l] != map[j][l]) {
      return false;
    }
  }
  return true;
}

// return the col of the smudge, if exists, on line i
function getSmudgeOnLines(map,i,j) {

  let diffCount = 0;
  let diffCol = -1;

  for (let l=0;l<map[i].length; l++){
    if (map[i][l] != map[j][l]) {
      if(diffCount > 0) {
        return -1;
      } else {
        diffCount++;
        diffCol = l;
      }
    }
  }

  if (diffCount == 1) {
    console.log("comparing "+i+" : "+j);
    console.log(""+ map[i]);
    console.log(""+ map[j]);
    console.log("Index " +diffCol);
    
    return diffCol;
  } else {
    return -1;
  }
} 

// function compareColumns(map,i,j) {
//   for (let l=0;l<map.length; l++){
//     if (map[l][i] != map[l][j]) {
//       return false;
//     }
//   }
//   return true;
// }


function loadGames(lines) {

  const maps = [];
  let map = [];

  lines.forEach((line,i) => {
    if (line == "") {
      maps.push(map);
      map = [];
    } else {
      const l = line.split("");
      map.push(l);
    }
  });

  maps.push(map);

  return maps
}

function fixSmudge(grid) {

  //const grid = structuredClone(map);
  let smudge = false;
  let line = 0;

  for (let i=0; i<grid.length-1;i=i+1) { 
    for (var j=i+1; j<grid.length-1;j=j+2) {
      const l = getSmudgeOnLines(grid,i,j);
      if (l != -1) {
        smudge = true;
        line = Math.floor((j-i)/2) + i + 1 ;
        if (grid[i][l] == "#") {
          grid[i][l] = ".";
        } else {
          grid[i][l] = "#";
        }
      }
    }
  }

  return {new:grid,smudge:smudge,line:line};
}

function transpose(grid) {
  const transposed = [];

  for (let line=0;line < grid.length;line++) {
    for (let col=0; col < grid[line].length; col++) {
      if (!transposed[col]) {
        transposed[col] = [];
      }
      transposed[col].push(grid[line][col]);
    }
  }

  return transposed;
}

