
const fs = require('fs')

fs.readFile('day17.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});


function solve (data) {

  console.time('1');

  const lines = data.split(/\r?\n/);
  const game = loadGames(lines);

  console.log(game);

  const one = part1(game);
  const two = 1;

  console.log("Part 1:" + one);
  console.log("Part 2:" + two);
  
  console.timeEnd('1');
}

function part1(map) {

  const queue =[];
  const visited = [];
  const start = {
    line:0,
    col:0,
    lineMoves:0,
    colMoves:0,
    heatLoss : 0,
    steps : 0,
    prev : {
      line:0,
      col:0
    },
    path : ""
  }

  queue.push(start);

  while (queue.length > 0) {

    const current = queue.shift();
    //console.log(current);

    // if bottom right stop
    if (current.line == map.length-1 && current.col == map[0].length-1) {
      console.log("Done!")
      console.log(current);
      return current.heatLoss;
    }

    if (current.line == 1 && current.col == 3) {
      console.log(current);
    }
    // Visited?
    const beenThere = visited.findIndex(pt => 
      pt.line == current.line && pt.col == current.col 
    );
    

    if (beenThere != -1) {
      // Want to minimise Heat Loss
      if (current.heatLoss >= visited[beenThere].heatLoss) {
        continue;
      } else {
        visited[beenThere] = current;
        if (current.line == 1 && current.col == 3) {
          console.log("Check 2");
        }
      }
    } else {
      visited.push(current);
    }

    //Go left
    if (current.col-1 >= 0) {
      let next = {
        line:current.line,
        col:current.col-1,
        lineMoves : 0,
        colMoves : current.colMoves+1,
        prev : current,
        path : current.path+"<"
      }
      next.steps = current.steps+1;
      next.heatLoss = current.heatLoss + map[next.line][next.col];
      if (next.colMoves <= 3 && !samePlace(current.prev,next)) {
        queue.push(next)
      }
    } 

    //Go Right
    if (current.col+1 <= map[0].length-1) {
      let next = {
        line:current.line,
        col:current.col+1,
        lineMoves : 0,
        colMoves : current.colMoves+1,
        prev : current,
        path : current.path+">"
      }
      next.heatLoss = current.heatLoss + map[next.line][next.col];
      next.steps = current.steps+1;
      if (next.colMoves <= 3 && !samePlace(current.prev,next)) {
        queue.push(next)
      }
    } 
    //Go Down
    if (current.line+1 <= map.length-1) {
      let next = {
        line:current.line+1,
        col:current.col,
        lineMoves : current.lineMoves+1,
        colMoves : 0,
        prev : current,
        path : current.path+"V"
      }
      next.steps = current.steps+1;
      next.heatLoss = current.heatLoss + map[next.line][next.col];
      if (next.lineMoves <= 3 && !samePlace(current.prev,next)) {
        queue.push(next)
      }
    } 
    //Go up
    if (current.line-1 >= 0) {
      let next = {
        line:current.line-1,
        col:current.col,
        lineMoves : current.lineMoves+1,
        colMoves : 0,
        prev : current,
        path : current.path+"^"
      }
      next.steps = current.steps+1;
      next.heatLoss = current.heatLoss + map[next.line][next.col];
      if (next.lineMoves <= 3 && !samePlace(current.prev,next)) {
        queue.push(next)
      }
    } 
  }
}

function samePlace(p1,p2) {
  if (p1.line == p2.line && p1.col == p2.col) {
    return true;
  } else {
    return false
  }
}
function loadGames(lines) {

  const map = [];
  
  
  lines.forEach((line,i) => {
    const l = line.split("");    
    map.push(l.map(k => parseInt(k)));
  });

  return map
}

