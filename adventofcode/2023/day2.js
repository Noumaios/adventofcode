
const fs = require('fs')

fs.readFile('day2.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});


function solve (data) {

    const lines = data.split(/\r?\n/);
    
    const games = loadGame(lines);

    var validIds = 0;
    var power = 0;

    games.forEach((game) => {
      if (game.cubesMax.red <= 12 && game.cubesMax.green <= 13 && game.cubesMax.blue <= 14 ) {
        validIds += game.id;
      }
      power += game.cubesMax.red*game.cubesMax.green*game.cubesMax.blue;
    });
    console.log(validIds);
    console.log(power);
}

function loadGame(input) {

  const games = [];
  input.forEach((line) => {

    var game = {
      id:0,
      subset:[],
      cubesMax : {
        red : 0,
        blue:0,
        green:0
      }
    }

    const g = line.split(":");
    
    game.id = parseInt(g[0].split(" ")[1]);
    game.subset = g[1].split(";");

    game.subset.forEach((s) => {

      const sub = s.split(",");

      for (var i=0; i< sub.length;i=i+1) {
        const c = sub[i].split(" ");
        game.cubesMax[c[2]]=Math.max(parseInt(c[1]),game.cubesMax[c[2]]);
      }
    });

    games.push(game);
  });

  return games;

} 
