
const fs = require('fs')

fs.readFile('day3.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    const engine = loadEngine(lines);

    const realParts = [];

    // part 1
    engine.symbols.forEach((symbol) => {

      // iterate backward to prevent issues while modifying array
      for (var i=engine.parts.length-1; i>=0; i--) {
       
        const part = engine.parts[i];

        if (isAdjacent(symbol,part)) {
          engine.parts.splice(i,1);
          realParts.push(part);
        }
      }
    });

    var sum = 0;
    realParts.forEach((part)=> {
      sum+=parseInt(part.num);
    });

    console.log("part 1 : " + sum);

    //part 2
    var totalRatio = 0;
    engine.symbols.forEach((symbol) => {

      if (symbol.char == "*") {

        const cogs = [];

        realParts.forEach((part) => {
          if (isAdjacent(symbol,part)) { 
            cogs.push(part.num);
          }
        });
        
        if (cogs.length == 2) {
          var ratio = parseInt(cogs[0])*parseInt(cogs[1]);
          totalRatio+=ratio;
        }
      }
    });
    
    console.log("part 2 : "+ totalRatio);
}

function loadEngine(lines) {

  const parts = [];
  const symbols = [];

  for (var i=0;i<lines.length;i++) {
  
    const line = lines[i].split("");
    var partNumber={
      num:"",
      line:0,
      col:0
    };

    for (var j=0; j<line.length;j++) {
      
      const char = line[j];
      
      if (char == "0" || parseInt(char)) {
        //part Number
        if (partNumber.num == "") {
          partNumber.line=i;
          partNumber.col=j;
        }
        partNumber.num+=char;
      } else if (char != ".") {
        //symbol
        symbols.push({
          char : char,
          line : i,
          col : j
        });
        partNumber = addPart(parts,partNumber);
      } else {
        partNumber = addPart(parts,partNumber);
      }

      if (j == line.length-1) {
        addPart(parts,partNumber);
      }
    }
  }

  console.log("engine parts " + parts.length)
  return { parts : parts,
          symbols : symbols}
} 

function addPart(arr,part) {
  if (part.num != "") {
    arr.push(part);
  }
  part =  {
    num:"",
    line:0,
    col:0
  }
  return part;
}

function isAdjacent(x,y) {

  if (Math.abs(x.line - y.line) <=1) {

    if (Math.abs(x.col - y.col)<=1 || 
        Math.abs(x.col - (y.col + y.num.length-1))<=1 ||
        (x.col <  y.col + y.num.length-1 && x.col > y.col)) {
            return true;
    } 
  }
  return false;
}