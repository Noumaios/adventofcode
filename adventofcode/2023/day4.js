
const fs = require('fs')

fs.readFile('day4.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    const games = loadGames(lines);

    var total=0;

    games.forEach((game) => {
      var score = 0;
      game.numbers.forEach((num) => {
        if (game.winningNumbers.includes(num)) {
          game.wins++;
        }
      });
      score = game.wins>0?Math.pow(2,game.wins-1):0;
      total+=score;
    });
    console.log(games);
    console.log("total score " + total);   

    for (i=0; i<games.length; i++) {
      var game = games[i];
      if (game.wins > 0) {
        for (var j=i+1; j<games.length && j<=i+game.wins;j++) {
          games[j].numbersOfCards+= game.numbersOfCards;
        }
      } 
    }
    
    var totalCards = 0;
    games.forEach((game) => {
      totalCards += game.numbersOfCards;
    });

    console.log("total cards " + totalCards);
    
}

function loadGames(lines) {

  const games = [];

  lines.forEach((line) => {
    const game = {
      id:"",
      winningNumbers:[],
      numbers:[],
      wins:0,
      numbersOfCards:1
    }

    const gameLine = line.split(":");
    game.id=gameLine[0];
    
    const num = gameLine[1].split("|");
    game.winningNumbers = splitNumbers(num[0]);
    game.numbers = splitNumbers(num[1]);

    games.push(game);
  });
  
  return games;

} 

function splitNumbers(nums) {
  const s = nums.split(" ");
  const arr = [];
  s.forEach((num) => {
    if (num == "0" || parseInt(num)) {
      arr.push(parseInt(num));
    }
  });

  return arr;
}
