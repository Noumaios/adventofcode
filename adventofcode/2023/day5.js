
const fs = require('fs')

fs.readFile('day5.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

function solve (data) {

    const lines = data.split(/\r?\n/);
    
    const game = loadGames(lines);
    console.log(game);

    // part 1
    var minLocation = Number.MAX_VALUE;
    // find location for all seeds
    game.seeds.forEach((seed) => {
      var loc = findLocationForSeed(seed.num,game.maps);
      minLocation=Math.min(loc,minLocation);
    });

    console.log("Min Location is : " +minLocation);
    
    // part 2
    var minLocation = Number.MAX_VALUE;
    // find location for all seeds
    for (var i=0;i<game.seeds.length-1;i=i+2) {
      var max = game.seeds[i].num + game.seeds[i+1].num;
      console.log("range " + game.seeds[i].num + " - " + max)
      for (var j=game.seeds[i].num; j<game.seeds[i].num+game.seeds[i+1].num;j++) {
        var loc = findLocationForSeed(j,game.maps);
        minLocation=Math.min(loc,minLocation);
      }
    }

    console.log("Min Location for part 2 is : " +minLocation);

}

function findLocationForSeed(seed,maps) {

  var next=seed;

  for (var i=0; i<maps.length; i++) {
    var map = maps[i];
    for (var j=0;j<map.ranges.length;j++) {
      var range = map.ranges[j];
      if (next >=range.sourceStart && next < range.sourceStart +range.range) {
        next = range.destinationStart + next - range.sourceStart; 
        break;
      }
    }
  }
  return next;
}


function loadGames(lines) {

  const seeds = [];
  const maps = [];

  // Seeds
  seedsLine = lines[0].split(":")[1].split(" ");
  seedsLine.forEach((seed) => {
    if (seed =="0" || parseInt(seed)) {
      const newSeed={
        num:parseInt(seed)
      }
      seeds.push(newSeed);
    }
  });

  // Maps
  var newMap = {
  }
  for (var i=2; i<lines.length;i++) {
    line = lines[i];
    // new Map
    if (line.indexOf("map") != -1 ) {
      newMap = {
        name : line.split(":")[0],
        ranges : []
      }
      continue;
    }

    // Save map
    if (line == "" || i == lines.length-1) {
      maps.push(newMap);
      continue;
    }

    const mapData = line.split(" ");
    const newRange = {
      sourceStart:parseInt(mapData[1]),
      destinationStart:parseInt(mapData[0]),
      range: parseInt(mapData[2])
    }
    
    newMap.ranges.push(newRange);

  }

  return {seeds : seeds, maps : maps};

} 
