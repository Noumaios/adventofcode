
const fs = require('fs')

fs.readFile('day6.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});

function solve (data) {

    console.time('1');
    const lines = data.split(/\r?\n/);
    const game = loadGames(lines);
    let total = 1;
    //console.log(game);

    // part 1
    game.forEach(race => {
      let currWins = getPossibleWinsWithMath(race);
      total*=currWins;
    })

    console.log("Part 1:" + total);
    console.timeEnd('1');
}

function getPossibleWins(race) {

  let wins=0;
  let prev=0;

  for (let i=0; i<=race.time;i++) {
    let dist = i*(race.time-i);
    if (dist > race.top_distance) {
      wins++;
    }
    if (prev > dist && dist <= race.top_distance) {
      break;
    }
    prev=dist;
  }
  return wins;
}

function getPossibleWinsWithMath(race) {

  let min,max=0;
  let d = Math.sqrt(race.time*race.time - 4*race.top_distance);

  min = Math.floor(((race.time - d ) / 2) +1);
  max = Math.ceil(((race.time + d ) / 2)-1);
  return max-min+1;
}


function loadGames(lines) {

  const games = [];
  const times = [];
  const distances = [];

  console.log(lines[0]);
  const t = lines[0].split(":")[1].split(" ");
  const s = lines[1].split(":")[1].split(" ");

  t.forEach(time => {
    if (time == "0" || parseInt(time)) {
      times.push(parseInt(time));
    }
  })
  s.forEach(distance => {
    if (distance == "0" || parseInt(distance)) {
      distances.push(parseInt(distance));
    }
  })

  for (let i=0; i<times.length;i++ ) {
    let game = {
      time : times[i],
      top_distance : distances[i]
    }
    games.push(game);
  }

  return games;

} 
