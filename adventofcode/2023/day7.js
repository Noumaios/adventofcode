
const fs = require('fs')

const HIGH_CARD = 1;
const ONE_PAIR = 2;
const TWO_PAIR = 3;
const THREE_OF_A_KIND = 4;
const FULL_HOUSE = 5;
const FOUR_OF_A_KIND = 6;
const FIVE_OF_A_KIND = 7;

fs.readFile('day7.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});


function solve (data) {

    console.time('1');

    const lines = data.split(/\r?\n/);
    const game = loadGames(lines);
  

    // part 1
    let total = 0;
    const sortedGame = game.sort((a,b) => {
      return b.score-a.score;
    })

    for (let i=0; i<sortedGame.length; i++) {
      total += (sortedGame.length-i) * sortedGame[i].bid;
    }

    console.log("part 1: " + total);

    // part 2
    total = 0;
    const sortedGameWithJoker = game.sort((a,b) => {
      return b.scoreWithJoker-a.scoreWithJoker;
    })
    for (let i=0; i<sortedGameWithJoker.length; i++) {
      total += (sortedGameWithJoker.length-i) * sortedGameWithJoker[i].bid;
    }

    console.log("part 2 : " + total);

    console.timeEnd('1');
}


function loadGames(lines) {

  const hands = [];
  
  lines.forEach(line => {
    let l = line.split(" ");
    let hand = parseHand(l[0],l[1]);
    hands.push(hand);
  })

  return hands;
} 


function parseHand (hand,bid) {

  let cards = hand.split("");
  let groups = {};
  let parsed = {};

  parsed.score = 0;
  parsed.scoreWithJoker = 0;

  for (let i=0; i<cards.length;i++) {
    // group cards in hand 
    if (groups[cards[i]]) {
      groups[cards[i]]++;
    } else {
      groups[cards[i]] = 1;
    }

    parsed.score += cardScore(cards[i],false)*Math.pow(15,4-i);
    parsed.scoreWithJoker += cardScore(cards[i],true)*Math.pow(15,4-i);
  }

  let group = Object.values(groups);

  switch (group.length) {
    case 5:
      //High card
      parsed.type = "High card";
      parsed.handScore = HIGH_CARD;
      break;
    case 4:
      //1 pair
      parsed.type = "One pair";
      parsed.handScore = ONE_PAIR;
      break;
    case 3:
      // 2 pairs or 3 of a kind
      if (group.includes(3)) {
        parsed.type = "Three of a kind";
        parsed.handScore = THREE_OF_A_KIND;
        break;
      }
      if (group.includes(2)) {
        parsed.type = "Two pairs";
        parsed.handScore = TWO_PAIR;
        break;
      }
      break;
    case 2:
      //full house or four of a kind
      if (group.includes(4)) {
        parsed.type = "Four of a kind";
        parsed.handScore = FOUR_OF_A_KIND;
      } else {
        parsed.type = "Full House";
        parsed.handScore = FULL_HOUSE;
      }
      break;
    case 1:
      //five of a kind
      parsed.type = "Five of a kind";
      parsed.handScore = FIVE_OF_A_KIND;
      break;
  }
  
  // Now boost all this with jokers
  parsed.handScoreWithJoker = scoreWithJoker(groups['J']?groups['J']:0,parsed);
  
  parsed.scoreWithJoker += parsed.handScoreWithJoker*Math.pow(15,5);
  parsed.score += parsed.handScore*Math.pow(15,5);
  parsed.cards = hand;
  parsed.bid = parseInt(bid);

  return parsed;
}

function scoreWithJoker(num,parsed) {

  parsed.typeWithJoker = parsed.type;

  switch (parsed.type) {
    case "High card":
      if (num > 0) {
        // Joker -> 1 pair
        parsed.typeWithJoker = "One pair";
        return ONE_PAIR;
      } else {
        return HIGH_CARD;
      }
      break;
    case "One pair":
      if (num > 0) {
        parsed.typeWithJoker = "Three of a Kind";
        return THREE_OF_A_KIND;
      } else {
          return ONE_PAIR;
      }
      break;
    case "Two pairs":
      if (num > 0) {
        if (num == 1) {
          parsed.typeWithJoker = "Full House";
          return FULL_HOUSE
        } else if (num == 2) {
          parsed.typeWithJoker = "Four of a kind";
          return FOUR_OF_A_KIND;
        }
      } else {
        return TWO_PAIR;
      }
      break;
    case "Three of a kind":
      // 2 pairs or 3 of a kind
      if (num > 0) {
        parsed.typeWithJoker = "Four of a kind";
        return FOUR_OF_A_KIND;
      } else {
        return THREE_OF_A_KIND;
      }
      break;
    case "Full House":
      if (num > 0) {
        parsed.typeWithJoker = "Five of a kind";
          return FIVE_OF_A_KIND;
      } else {
         return FULL_HOUSE;
      }
      break;
    case "Four of a kind":
      if (num > 0) {
        parsed.typeWithJoker = "Five of a kind";
        return FIVE_OF_A_KIND;
      } else{
        return FOUR_OF_A_KIND;
      }
      break;
    case "Five of a kind":
      return FIVE_OF_A_KIND;   
  }
}


function cardScore(c,withJoker) {

  const defaultSort = "23456789TJQKA";
  const jokerSort = "J23456789TQKA";

  return(withJoker?jokerSort.indexOf(c):defaultSort.indexOf(c));
  
}
