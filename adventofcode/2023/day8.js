
const fs = require('fs')

fs.readFile('day8.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});


function solve (data) {

    console.time('1');

    const lines = data.split(/\r?\n/);
    const game = loadGames(lines);

    console.log("part 1: " + part1(game));
    console.log("part 2: " +  part2(game));

    console.timeEnd('1');
}

function part1(game){

  let steps = 0;
  
  let here = game.map.find(node => {
    return node.position == "AAA";
  });

  if (!here) {
    return -1;
  }

  while (true) {

    if (here.position == "ZZZ") {
      break;
    }
    const i = steps % game.directions.length;
    here = game.map.find(node => {
      return node.position == here[game.directions[i]]
    });
    steps++;

  }

  return steps

}


function part2(game){

  let steps = 0;

  let routes = game.map.filter(node => {
    return node.position.endsWith("A");
  });

  while (true) {

    let ends = routes.filter(node => {
      return node.steps;
    });
    if (ends.length == routes.length) {
      break;
    }

    const i = steps % game.directions.length;

    routes.forEach(route => {

      // find next
      const next = game.map.find(node => {
        return node.position == route[game.directions[i]]
      });
      route.position = next.position;
      route.L = next.L;
      route.R = next.R;

      if (route.position.endsWith("Z")) {
          route.steps = steps+1;
      }
    });
    steps++;
  }

  console.log(routes);

  // now get the lower common multiple
  let routeSteps = [];
  routes.forEach(node => {
    routeSteps.push(node.steps);
  });

  const sortedSteps = routeSteps.sort((a,b) => {
    return a.steps - b.steps;
  })
  
  return leastCommonMultiple(sortedSteps);

}

function leastCommonMultiple(arr) {

  function gcd(a, b) {
      return !b ? a : gcd(b, a % b);
  }

  function lcm(a, b) {
      return (a * b) / gcd(a, b);   
  }

  var multiple = arr[0];
  arr.forEach(function(n) {
      multiple = lcm(multiple, n);
  });

  return multiple;
}

function loadGames(lines) {

  const map = [];
  
  const directions = lines[0].split("");

  for (let i=2; i<lines.length;i++) {
    const clean = lines[i].replace(/ /g,"").replace(/\(/g,"").replace(/\)/g,"");    
    
    const node = {
      position : clean.split("=")[0],
      L:clean.split("=")[1].split(",")[0],
      R:clean.split("=")[1].split(",")[1]
    }
    map.push(node);
  }

  return {
    directions : directions,
    map : map
  };
} 


