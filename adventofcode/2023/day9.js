
const fs = require('fs')

fs.readFile('day9.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
});


function solve (data) {

    console.time('1');

    const lines = data.split(/\r?\n/);
    const game = loadGames(lines);
    let total1 = 0;
    let total2 = 0;
    console.log(game);

    game.forEach(reading => {
      console.log(reading);
      let next = nextValue(reading);
      let prev = prevValue(reading);

      console.log(next);
      console.log(prev);

      total1+=next;
      total2+=prev;
    })

    console.log("Part 1:" + total1);
    console.log("Part 2:" + total2);
    
    console.timeEnd('1');
}

function nextValue(arr) {
  
  if (arr.every(v => v == 0)) {
    return 0;
  }

  const nextArr = []; 
  for (var i=0;i<arr.length-1;i+=1) {
    nextArr.push(arr[i+1]-arr[i]);
  }

  return arr[arr.length-1] + nextValue(nextArr);
}

function prevValue(arr) {

  if (arr.every(v => v == 0)) {
    return 0;
  }

  const nextArr = []; 
  for (var i=0;i<arr.length-1;i+=1) {
    nextArr.push(arr[i+1]-arr[i]);
  }

  return arr[0] - prevValue(nextArr);
}

function loadGames(lines) {

  const readings = [];
  
  lines.forEach(line => {
    const l = line.split(" ");
    
    const reading = l.map(value => {
      return parseInt(value);
    });
    readings.push(reading);
  });

  return readings;
} 


