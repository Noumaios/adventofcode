
const fs = require('fs')

fs.readFile('day1.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }

  const lines = data.split(/\r?\n/);
  const first = [];
  const second = [];

  lines.forEach((line) => {
    const pair = line.split("   ");
    first.push(parseInt(pair[0]));
    second.push(parseInt(pair[1]));
  });

  // sort
  first.sort(function(a,b) { return a-b});
  second.sort(function(a,b) {return a-b});

  solve(first,second);
  solve2(first,second);

});

function solve (first,second) {

    var distance = 0;
 
    for (var i=0;i<first.length;i++) {
      distance += Math.abs(first[i] - second[i]);
    }
    
    console.log("Part 1:" + distance);
}

function solve2 (first,second) {

    var similarity = 0;

    first.forEach(n => {
      const occurence = second.filter(x => x == n);
      similarity += occurence.length*n;
    })

    console.log("Part 2:" + similarity);

}

