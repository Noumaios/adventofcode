const fs = require('fs')

var maxRow = 0;
var maxCol = 0;

const moves = [
    [-1,0],
    [1,0],
    [0,1],
    [0,-1]
]

fs.readFile('day10.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const map = [];

    for (var i=0;i<lines.length;i++) {
        const line = lines[i].split("").map(x=>parseInt(x));
        const l = [];
        for (var j=0;j<line.length;j++) {
            l.push({height : line[j],next:[],id:[i,j].toString()});
        }
        map.push(l);
    }

    maxRow = map.length;
    maxCol = map[0].length;

    const paths = [];

    // build a tree with the path
    for (var r=0;r<maxRow;r++) {
        for (var c=0;c<maxCol;c++) {
            if (map[r][c].height == 0) {
                paths.push(map[r][c]);
            }
            moves.forEach(move=> {
                if (inBounds(move[0]+r,move[1]+c)) {
                    if (map[move[0]+r][move[1]+c].height - map[r][c].height == 1) {
                        map[r][c].next.push(map[move[0]+r][move[1]+c]);
                    }
                }
            })
        }
    }

    solve(paths);
    solve2(paths);

});

function solve (paths) {
    var total=0;

    paths.forEach(path => {
        const score = scoreForPath(path,[],false);
        total+=score;
    });
    
    console.log("-------- Part 1:" + total);
}

function solve2 (paths) {
    var total = 0;

    paths.forEach(path => {
        const score = scoreForPath(path,[],true);
        total+=score;
    });

    console.log("-------- Part 2:" + total);
}


function inBounds(r,c) {
    if (r < maxRow && r >=0 && c >=0 && c < maxCol) {
        return true;
    } else {
        return false;
    }
}

function scoreForPath(path,visited,all) {

    if (path.height == 9 ){
        if (!visited.includes(path.id) || all) {
            visited.push(path.id);
            return 1;
        } 
    } 
    if (path.next.length == 0) {
        return 0;
    }
    var t=0;
    path.next.forEach(n => {
        t+=scoreForPath(n,visited,all);
    })
    return t;
}

