const fs = require('fs')

fs.readFile('day11.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const stones = data.split(" ");

    solve([...stones]);
    solve2([...stones]);

});

function solve (stones) {
    
    const start = Date.now(); 
    const steps = 25;
    var total =0;
    stones.forEach(stone => {
        total+=numberOfElements(stone,steps);
    });
    const end = Date.now();
    console.log("-------- Part 1("+(end-start) +"ms):" + total);
}

function solve2 (stones) {

    const start = Date.now(); 
    const steps = 75;
    var total =0;
    stones.forEach(stone => {
        total+=numberOfElements(stone,steps);
    })
    const end = Date.now();
    console.log("-------- Part 2 ("+(end-start) +"ms):" + total);
}

function blink(stones) {

    for (var i=stones.length-1;i>=0;i--) {
        if (stones[i] === "0") {
            stones[i] = '1';
        } else if (stones[i].length % 2 == 0) {
            const newStone = parseInt(stones[i].substring(stones[i].length / 2)).toString();
            stones[i] = stones[i].substring(0,stones[i].length / 2);
            stones.splice(i+1,0,newStone);
        } else {
            stones[i] = (parseInt(stones[i])*2024).toString();
        }
    }
}

const done = {};

function numberOfElements(stone,steps) {

    var total=0;

    if (steps == 0) {
        return 1;
    }

    if (done[steps + " - " + stone]) {
        return done[steps + " - " + stone];
    }

    const stones = [stone];
    blink(stones);
    stones.forEach(s => {
        total += numberOfElements(s,steps-1);
    });

    done[steps + " - " + stone] = total;
    
    return total;
}


