const fs = require('fs')

var maxRow = 0;
var maxCol = 0;

const moves = [
    [-1,0],
    [1,0],
    [0,1],
    [0,-1]
]

fs.readFile('day12.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const map = [];

    for (var i=0;i<lines.length;i++) {
        map.push(lines[i].split(""));
    }

    maxRow = map.length;
    maxCol = map[0].length;

    const regions = [];
    const visited = {};
    // create the list for region
    for (var r=0;r<maxRow;r++) {
        for (var c=0;c<maxCol;c++) {
            if (visited[r+"-"+c]) {
                continue;
            }
            // if not visited, new region
            regions.push(createRegion(visited,map,r,c));
        
        }
    }

    
    solve(regions);
    solve2(regions);

});

function solve (regions) {
    var total=0;

    regions.forEach(r => {
        total += perimeter(r)*r.plots.length;
    });
    
    console.log("-------- Part 1:" + total);
}

function solve2 (regions) {
    var total = 0;

    regions.forEach(r => {
        total += sides(r)*r.plots.length;
    });
    console.log("-------- Part 2:" + total);
}


function inBounds(r,c) {
    if (r < maxRow && r >=0 && c >=0 && c < maxCol) {
        return true;
    } else {
        return false;
    }
}

function createRegion(visited,map,r,c) {
    const region = {
        crop : map[r][c],
        plots : []
    }

    const queue = [];
    queue.push([r,c]);

    while (queue.length > 0) {

        const curr = queue.shift();
        
        if (visited[curr[0]+"-"+curr[1]]) {
            continue;
        }

        visited[curr[0]+"-"+curr[1]] = true;
        region.plots.push(curr);

        moves.forEach(move => {
            const nextR = curr[0]+move[0];
            const nextC = curr[1]+move[1];    
            if (inBounds(nextR,nextC) && !visited[nextR + "-" +nextC] && map[r][c]==map[nextR][nextC]) {       
                queue.push([nextR,nextC]);
            }   
        });
    }

    return region;
}

function perimeter(region) {

    var p = 0;

    // perimeter = sum of all free side for each plot
    region.plots.forEach(plot => {
        var adj = 0;
        moves.forEach(move => {
            const adjR = plot[0]+move[0];
            const adjC = plot[1]+move[1];
            if (adjacent(region,[adjR,adjC])) {
                
                // found adjacent plot on this side    
                adj++;
            }
        })
        
        p += 4-adj;
    })

    return p;

}

function sides(region) {

    var p = 0;

    const edges = [];
    const sides = [];
    
    // get all the possible edges for each plot
    region.plots.forEach(plot => {
        
        moves.forEach(move => {
            const adjR = plot[0]+move[0];
            const adjC = plot[1]+move[1];

            if (!adjacent(region,[adjR,adjC]) || !inBounds(adjR,adjC)) {
                // no adjacent on this side    
                // add to the edges collection
                edges.push({plot :plot, side : move});
            }
        })
    })

    // create groups with aligned edges
    edges.forEach(p => {

        // retreive existing line of sides if exists or create a new one
        var exist = sides.filter(x=>
            x.side[0] == p.side[0] && 
            x.side[1] == p.side[1] && 
            x.line.findIndex(a => a[0] == p.plot[0] && a[1] == p.plot[1]) != -1
        )
        var side;
        if (exist.length == 0) {
            side = {
                side : p.side,
                line : [p.plot]
            }
            sides.push(side);
        } else {
            side = exist[0];
        }
        

        // retreive adjacent plots with a side aligned with current one
        const adjPlot = edges.filter(x => aligned(p,x));
        
        if (adjPlot.length >0) {
            adjPlot.forEach(p=> {
                if (side.line.indexOf(p.plot) == -1) {
                    side.line.push(p.plot);
                }
            })
        }
    })

    // return the number of unique lines
    return sides.length;

}

// return true if edge a and b are aligned
function aligned(a,b) {

    if (a.side[0] == b.side[0] && a.side[1] == b.side[1]) {
        if ((a.plot[0] + a.side[1] == b.plot[0] && a.plot[1] + a.side[0] == b.plot[1]) ||
            (a.plot[0] - a.side[1] == b.plot[0] && a.plot[1] - a.side[0] == b.plot[1])) {
                return true;
            }
    }
    return false;
}

function adjacent(region,b) {
    if (inBounds(b[0],b[1]) && 
                region.plots.find(a => {
                    if (a[0] == b[0] && a[1] == b[1]) {
                        return a;
                    }
                })) {
                return true;
    } else {
        return false;
    }

}