const fs = require('fs')

fs.readFile('day13.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const machines = [];
    var machine = {};

    lines.forEach(line => {
        if (line === "") {
            machines.push(machine);
            machine = {};
        }
        if (line.indexOf("Button A: ") != -1) {
            machine["A"] = extractMoves(line.split("Button A: ")[1].split(", "));
        }
        if (line.indexOf("Button B: ") != -1) {
            machine["B"] = extractMoves(line.split("Button B: ")[1].split(", "));
        }
        if (line.indexOf("Prize: ") != -1) {
            machine["Prize"] = extractMoves(line.split("Prize: ")[1].split(", "));
        }
    })
    machines.push(machine);
    
    console.log(machines);

    solve(machines);
    solve2(machines);

});

function solve (machines) {
    var total=0;

    machines.forEach(machine =>{

        const solution = movesToPrize(machine);
        if (solution.valid) {
            total+=solution.pressA*3+solution.pressB*1
        }
    });
  
    console.log("-------- Part 1:" + total);
}

function solve2 (machines) {
    var total = 0;
    machines.forEach(machine=> {
        //add 10000000000000 to prize location
        machine["Prize"][0] += 10000000000000;
        machine["Prize"][1] += 10000000000000;

        const solution = movesToPrize(machine);
        if (solution.valid) {
            total+=solution.pressA*3+solution.pressB*1
        }

    })
    console.log("-------- Part 2:" + total);
}

function extractMoves(arr) {
    const moves = [0,0];
    if (arr[0].indexOf("+") != -1) {
        moves[0] = parseInt(arr[0].split("X+")[1]);
        moves[1] = parseInt(arr[1].split("Y+")[1]);
    } else {
        moves[0] = parseInt(arr[0].split("X=")[1]);
        moves[1] = parseInt(arr[1].split("Y=")[1]);
    }
    return moves;
}

// solve the linear system
function movesToPrize(machine) {

    var a,b;
    var valid = false;

    b = (machine["A"][0]*machine["Prize"][1] - machine["Prize"][0]*machine["A"][1]) / 
        (machine["B"][1]*machine["A"][0] - machine["A"][1]*machine["B"][0]);

    a = (machine["Prize"][0] - b*machine["B"][0])/machine["A"][0];
     
    if (Number.isInteger(a) && Number.isInteger(b)) {
        valid = true;
    }
    return {valid : valid, pressA:a,pressB:b};
}