const fs = require('fs')

const maxX=101;
const maxY=103;

fs.readFile('day14.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const robots1 = [];
    const robots2 = [];


    lines.forEach(line => {

        const r = line.split(" ");
        const v = r[1].substring(2).split(",").map(x=>parseInt(x));
        const p = r[0].substring(2).split(",").map(x=>parseInt(x));
        
        var robot1 = {
            pos : {
                x:p[0],
                y:p[1]
            },
            velocity :
            {
                x:v[0],
                y:v[1]
            }
        };
        var robot2 = {
            pos : {
                x:p[0],
                y:p[1]
            },
            velocity :
            {
                x:v[0],
                y:v[1]
            }
        };
        robots1.push(robot1);
        robots2.push(robot2);
    })
    
    solve(robots1);
    solve2(robots2);

});

function solve (robots) {
    var total=0;

    robots.forEach(robot => {
        move(robot,100);
    });
    
    total = countRobots(robots);
    console.log("-------- Part 1:" + total);
}

function solve2 (robots) {
    var total = 0;

    while (total < 100000) {
       robots.forEach(robot => {
            move(robot,1);
        });
        total++;
        // if density below an arbirtrary number (found by looping through the first 1000s)
        if (density(robots) < 40) {
            printMap(robots);
            break;
        }
    }
    
    console.log("-------- Part 2:" + total);
}

function move(robot,steps) {
    robot.pos.x = (robot.pos.x + steps*(robot.velocity.x+maxX))%maxX;
    robot.pos.y = (robot.pos.y + steps*(robot.velocity.y+maxY))%maxY;
}

function printMap(robots) {
    console.log();
    for (var y=0;y<maxY;y++) {
        for (var x=0;x<maxX;x++) {      
            const robotsAtPos = robots.filter(r => r.pos.x==x && r.pos.y==y).length;
            if (robotsAtPos == 0) {
                process. stdout. write(".");
            } else {
                process. stdout. write(robotsAtPos.toString());
            }
        }
        console.log();
    }
    console.log();
}

// count the robots per quadrant
function countRobots(robots) {
    const midX = Math.floor(maxX/2);
    const midY = Math.floor(maxY/2);

    const topLeft = robots.filter(r => r.pos.x<midX && r.pos.y<midY).length;
    const topRight = robots.filter(r => r.pos.x>midX && r.pos.y<midY).length;
    const bottomLeft = robots.filter(r => r.pos.x<midX && r.pos.y>midY).length;
    const bottomRight = robots.filter(r => r.pos.x>midX && r.pos.y>midY).length;

    return topLeft*topRight*bottomLeft*bottomRight;

}

// return the average distance between all robots
function density(robots) {

    const dists = [];
    for (var i=0; i<robots.length; i++) {
        const a = robots[i];
        for (var j=i+1; j<robots.length;j++) {
            const b = robots[j];
            const dist=Math.sqrt(Math.pow(b.pos.x-a.pos.x,2)+Math.pow(b.pos.y-a.pos.y,2));

            dists.push(dist);
        }
    }
    var sum=0;
    dists.forEach(d => {
        sum+=d;
    })
    return sum/dists.length;
    
}