const fs = require('fs')

const moves = {
    "^":[-1,0],
    "v":[1,0],
    "<":[0,-1],
    ">":[0,1]
}

fs.readFile('day15.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const map = [];
    const mapWide = [];

    var i=0;
    for (i=0;i<lines.length && lines[i] != "";i++) {
        map.push(lines[i].split(""));
    }

    // expand map
    map.forEach(l => {
        const line = [];
        l.forEach(c => {
            if (c == "#") {
                line.push(...["#","#"]);
            }
            if (c == ".") {
                line.push(...[".","."]);
            }
            if (c == "@") {
                line.push(...["@","."]);
            }
            if (c == "O") {
                line.push(...["[","]"]);
            }
        });
        mapWide.push(line);
    })


    var robotMoves = [];
    for (j=i+1; j<lines.length;j++) {
        robotMoves.push(...lines[j].split("")) 
    }

    const input = {
        map : map,
        robot : getRobotPosition(map),
        moves: robotMoves
    }

    const inputWide = {
        map : mapWide,
        robot : getRobotPosition(mapWide),
        moves : robotMoves
    }
    
    solve(input);
    solve2(inputWide);

});

function solve (input) {
    var total=0;
    input.moves.forEach(move => {
            if (canMove(input.map,input.robot,move)) {
                processMove(input.map,input.robot,move)
                input.robot = [input.robot[0]+moves[move][0],input.robot[1]+moves[move][1]];
            }   
    })

    print(input.map);
    console.log("-------- Part 1:" + getSumGPS(input.map));
}

function solve2 (input) {
    var total = 0;

    input.moves.forEach(move => {
            if (canMove(input.map,input.robot,move)) {
                processMove(input.map,input.robot,move)
                input.robot = [input.robot[0]+moves[move][0],input.robot[1]+moves[move][1]];
            } 
    })

    print(input.map);
    console.log("-------- Part 2:" + getSumGPS(input.map));
}



function print(map) {

    for (var i=0;i<map.length;i++) {
        for (var j=0;j<map[0].length;j++) {
            process.stdout.write(map[i][j]);
        }
        console.log();
    }
    console.log();
}

function getSumGPS(map) {

    var sum=0;
    for (var i=0;i<map.length;i++) {
        for (var j=0;j<map[0].length;j++) {
            if (map[i][j] == "O" || map[i][j] == "[") {
                sum += i*100 +j
            }
        }
    }
    return sum;
}

function getRobotPosition(map) {
    for (var i=0;i<map.length;i++) {
        for (var j=0;j<map[0].length;j++) {
            if (map[i][j] == "@") {
                return [i,j];
            }
        }
    }
}

function canMove(map,robot,move) {
    
    const next = [robot[0]+moves[move][0],robot[1]+moves[move][1]];
    // if wall
    if (map[next[0]][next[1]] == "#") {
        return false;
    }
    // if space
    if (map[next[0]][next[1]] == ".") {
        return true;
    }
    if (moves[move][0] == 0) {
        // lateral move
        return canMove(map,next,move);
    } else {
        // vertical move 
        if (map[next[0]][next[1]] == "[") {
            return canMove(map,next,move) && canMove(map,[next[0],next[1]+1],move);
        } else if (map[next[0]][next[1]] == "]") {
            return canMove(map,next,move) && canMove(map,[next[0],next[1]-1],move);
        } else {
            return canMove(map,next,move);
        }
    }
}

function processMove(map,pos,move) {
    
    const next = [pos[0]+moves[move][0],pos[1]+moves[move][1]];
    // if box
    if (map[next[0]][next[1]] == "O") {
        processMove(map,next,move);
    }
    // if wide box
    if (map[next[0]][next[1]] == "[" || map[next[0]][next[1]] == "]") {
        if (moves[move][0] == 0) {
                processMove(map,next,move);
        } else {
            if (map[next[0]][next[1]] == "[") {
                processMove(map,next,move);
                processMove(map,[next[0],next[1]+1],move);
            } else if (map[next[0]][next[1]] == "]"){
                processMove(map,next,move);
                processMove(map,[next[0],next[1]-1],move);
            }
        }
    }
    if (map[next[0]][next[1]] == "#") {
        return;
    }
    if (map[next[0]][next[1]] == ".") {
        map[next[0]][next[1]] = map[pos[0]][pos[1]];
        map[pos[0]][pos[1]] = ".";
        return;
    }
}
