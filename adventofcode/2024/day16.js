const fs = require('fs')

const moves = [
    [-1,0],
    [1,0],
    [0,-1],
    [0,1]
]

fs.readFile('day16.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const maze = [];
    
    lines.forEach(line => {
        maze.push(line.split(""));
    })

    solve(maze);
    solve2(maze);

});

function solve (maze) {
    const startTime = Date.now(); 
    const start = {
        row : maze.length-2,
        col : 1,
        dir : [0,1],
        score : 0
    }
    const end = {
        row : 1,
        col : maze[0].length-2,
        dir : [0,1],
        score : Infinity
    }
        
    const score = bestPathScore(maze,start,end);
    const endTime = Date.now(); 
    console.log("-------- Part 1("+ (endTime-startTime) +"ms):" + score);
}

function solve2 (maze) {
    const startTime = Date.now(); 
    const start = {
        row : maze.length-2,
        col : 1,
        dir : [0,1],
        score : 0
    }
    const end = {
        row : 1,
        col : maze[0].length-2,
        dir : [0,1],
        score : Infinity
    }
    
    const target = bestPathScore(maze,start,end);
    const sits = bestSits(maze,start,end,target);
    const endTime = Date.now(); 
    console.log("-------- Part 2("+ (endTime-startTime) +"ms):" + sits);
}

function bestPathScore(maze,start,end) {

    const queue = [];
    const visited = [];

    queue.push(start);

    while (queue.length > 0) {

        const curr = queue.shift();
        visited.push(curr);
    
        if (curr.row == end.row && curr.col == end.col) {
            break;
        }

        // add neighboors to queue and prioritise
        const next = neighboors(maze,curr,visited);
        next.forEach(n => {
            const i = queue.findIndex(x => x.score > n.score);
            queue.splice(i==-1?queue.length:i,0,n);
        });
    }

    const destination = visited.filter(x => x.row == end.row && x.col == end.col)
    if (destination.length == 1) {
        return destination[0].score
    } else {
        return Infinity
    }
}

function bestSits(maze,start,end,target) {
  
    const queue = [];
    const bestSits = [];
    const visited = [];
    
    queue.push(start);

    while (queue.length > 0) {
        const curr = queue.shift();
        
        if (curr.score > target || bestPathScore(maze,curr,end) > target) {
            continue;
        } else {
            // sit is part of a best path, add it to the list if doesn't exist
            if (bestSits.findIndex(x=> x.row == curr.row && x.col == curr.col) == -1) {
                bestSits.push(curr);
            }
        }
        visited.push(curr);
     
        // add neighboors to queue
        const next = neighboors(maze,curr,visited);
        next.forEach(n => {
            const i = queue.findIndex(x => x.score > n.score);
            queue.splice(i==-1?queue.length:i,0,n);
        });

    }

    return bestSits.length;
}

function neighboors(maze,curr,visited) {
    const n = [];

    moves.forEach(move => {
        // don't move back
        if (!(move[0]+curr.dir[0] == 0 && move[1]+curr.dir[1] == 0)) {
            const next = maze[curr.row + move[0]][curr.col + move[1]];

            if (next != "#" && visited.findIndex(x=> x.row == curr.row + move[0] && x.col == curr.col + move[1]) == -1) {
                n.push({
                    row :  curr.row + move[0],
                    col : curr.col + move[1],
                    dir : move,
                    score : curr.score + (curr.dir[0] == move[0] && curr.dir[1] == move[1]?1:1001)
                });
            }
        }   
    })

    return n;

}