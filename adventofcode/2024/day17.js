const fs = require('fs')

const opCodes = ['adv','bxl','bst','jnz','bxc','out','bdv','cdv'];

fs.readFile('day17.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const registers = [0,0,0];
    const program = [];

    lines.forEach(l => {
        if (l.indexOf("A") != -1) {
            registers[0] = parseInt(l.split(" A: ")[1]);
        }
        if (l.indexOf("B") != -1) {
            registers[1] = parseInt(l.split(" B: ")[1]);
        }
        if (l.indexOf("C") != -1) {
            registers[2] = parseInt(l.split(" C: ")[1]);
        }
        if (l.indexOf("Program") != -1) {
            program.push(...l.split(": ")[1].split(",").map(x => parseInt(x)));
        }

    })
    
    const input = {
        registers : [...registers],
        program : program
    }
    solve(input);
    input.registers = [...registers];
    solve2(input);

});

// /Just run
function solve (input) {

    console.log("-------- Part 1 : " + run(input.program,input.registers))

}
// Brute forvce doesn't work....
// For this specific program each loop print b%8 and divides a / 8
// start from the end and crunch the smallest a that will match the subset of the program end

function solve2 (input) {

    var total = 0;
    var regA = 0;

    for (var i=input.program.length-1;i>=0;i--) {

        const test = input.program.slice(i).toString();
        total=regA;
        
        for (var a=0; a<Math.pow(8,input.program.length-i)-total*8; a++) {
            regA = total*8+a
            input.registers = [regA,0,0];
            if (run(input.program,input.registers) === test) {
                break;
            }
        }
    }
    total = regA;
    
    console.log("-------- Part 2 : " + total);
}

function run(program,registers) {
   
    var idx = 0;
    var out = [];
    // main loop
    while(true) {

        if (idx + 1 > program.length) {
            // can't process -> halt
            break;
        } 

        const opCode = program[idx];
        const operand = program[idx+1];

        // process instruction and return instruction pointer
        const p = process(opCode,operand,registers,idx,out);

        //move
        idx=p;
    }
    return out.toString();
}

function process (opCode,operand,registers,idx,out) {

    // return value for instruction pointer
    var next=idx+2;

    switch (opCode) {
        case 0:
            // adv : division using combo
            registers[0] = parseInt(Math.trunc(registers[0]/Math.pow(2,comboOperand(operand,registers))));        
            break;
        case 1:
            // bxl : bitwise xor (forcing unsigned int)
            registers[1] = parseInt((registers[1]^operand) >>> 0);
            break;
        case 2:
            // bst : mod 8
            registers[1] = comboOperand(operand,registers)%8;
            break;
        case 3:
            // jnz : jump
            if (registers[0] != 0) {
                next = operand;
            }
            break;
        case 4:
            // bxc : bitwise xor on registers (forcing unsigned int)
            registers[1] = parseInt((registers[1]^registers[2]) >>> 0);
            break;
        case 5:
            // out : store output in out
            out.push(comboOperand(operand,registers)%8);
            break;
        case 6:
            // adv : division using combo to register B
            registers[1] = parseInt(Math.trunc(registers[0]/Math.pow(2,comboOperand(operand,registers))));
            break;
        case 7:
            // adv division using combo to register C
            registers[2] = parseInt(Math.trunc(registers[0]/Math.pow(2,comboOperand(operand,registers))));
            break;            
    }
    return next;
}

function comboOperand(op,registers) {

    if (op <= 3) {
        return op;
    } else {
        return registers[op-4];
    }
}