const fs = require('fs')

const moves = [
    [-1,0],
    [1,0],
    [0,-1],
    [0,1]
]
const maxRow = 71;
const maxCol = 71;

const maxBytes = 1024;

fs.readFile('day18.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const map = [];
    const bytes = [];
    


    lines.forEach(line => {
        bytes.push(line.split(",").map(x=> parseInt(x)));
    })
    
    console.log(bytes);

    solve(bytes.slice(0,maxBytes));
    solve2(bytes);

});

function solve (bytes) {
    const startTime = Date.now(); 
    const start = {
        row : 0,
        col : 0,
        path : [],
        dist : 0
    }
    const end = {
        row : maxRow-1,
        col : maxCol-1,
        path : [],
        dist : Infinity
    }
        
    print(bytes);
    const dist = bestPath(bytes,start,end,true);
    const endTime = Date.now(); 
    console.log("-------- Part 1("+ (endTime-startTime) +"ms):" + dist);
}

function solve2 (bytes) {
    const startTime = Date.now(); 
    const start = {
        row : 0,
        col : 0,
        path : [],
        dist : 0
    }
    const end = {
        row : maxRow-1,
        col : maxCol-1,
        path : [],
        dist : Infinity
    }
    
    var firstByte = 0;
    for (var i=maxBytes+1; i<bytes.length;i++) {
        if (bestPath(bytes.slice(0,i),start,end) == Infinity) {
            firstByte = i-1;
            break;
        }
    }
    const endTime = Date.now(); 
    
    console.log("-------- Part 2("+ (endTime-startTime) +"ms):" + bytes[firstByte]);
}

function bestPath(bytes,start,end,display) {

    const queue = [];
    const visited = [];

    queue.push(start);

    while (queue.length > 0) {

        
        const curr = queue.shift();
    
        if (visited.findIndex(x=> x.row == curr.row && x.col == curr.col) != -1) {
            continue;
        }
        visited.push(curr);
    
        if (curr.row == end.row && curr.col == end.col) {
            break;
        }

        // add neighboors to queue and prioritise
        const next = neighboors(bytes,curr,visited);
        next.forEach(n => {
            queue.push(n);
        })
        queue.sort((a,b) => {return b-a});
       
    }

    const destination = visited.filter(x => x.row == end.row && x.col == end.col)
    if (destination.length == 1) {
        if (display) {
            print(bytes,destination[0].path)
        }
        return destination[0].dist
    } else {
        return Infinity
    }
}

function neighboors(bytes,curr,visited) {
    const n = [];

    moves.forEach(move => {
        nextCol = move[1] + curr.col;
        nextRow = move[0] + curr.row;

        if (inBounds(nextRow,nextCol) && bytes.findIndex(x => x[0] == nextCol && x[1] == nextRow) == -1) {
            if (visited.findIndex(x=> x.row == nextRow && x.col == nextCol) == -1) {
                n.push({
                    row :  nextRow,
                    col : nextCol,
                    path : [...curr.path,curr],
                    dist : curr.dist+1
                });
            }
        }   
    })

    return n;

}


function print(bytes,path) {
    console.log(path);
    for (var i=0;i<maxRow;i++) {
        for (var j=0;j<maxCol;j++) {
            if (bytes.findIndex(x => x[0] == j && x[1] == i) != -1) {
                process.stdout.write("#");
            } else if (path && path.findIndex(x => x.col == j && x.row == i) != -1) {
                process.stdout.write("\x1b[97mO\x1b[39m");
            } else {
                process.stdout.write(".");
            }
        }
        console.log();
    }
    console.log();
}

function inBounds(r,c) {
    if (r < maxRow && r >=0 && c >=0 && c < maxCol) {
        return true;
    } else {
        return false;
    }
}