const fs = require('fs')
const cache = {};
const cache2 = {};
const pattern = {};
const solutions = {};
const paths = {};

fs.readFile('day19.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    var towels = [];
    const patterns = [];


    lines.forEach(line => {
        if (line.indexOf(",") != -1) {
            towels = line.split(", ");
        } else if (line != "") {
            patterns.push(line);
        }
    })
    
    input = {
        towels : towels,
        patterns : patterns
    }

    solve(input);
    solve2(input);

});

function solve (input) {
    var total = 0;
    const startTime = Date.now(); 
    input.patterns.forEach(pattern => {
        if (contains(pattern,input.towels)) {            
            total++;
        } 
    });
    const endTime = Date.now(); 
    console.log("-------- Part 1("+ (endTime-startTime) +"ms):" + total);
}

function solve2 (input) {
    var total = 0;

    const startTime = Date.now(); 
    console.log("total "  + total);
    input.patterns.forEach(pattern => {

        if (cache[pattern] == true) {
            const c = count(pattern,input.towels);
            total  += c;
            console.log("pattern " + pattern + " count " + c);
            // console.log("total "  + total);
            // console.log("checking " + pattern)
            // recordPaths(pattern,input.towels)
            
            // console.log(solutions);
            // console.log("got it");
            
            // const path = countArrangement(solutions[pattern]);
            // console.log("got it");
            // var dedup = [];
            // path.forEach(p => {
            //     dedup.push([...new Set(p)].sort());
            // });
            // dedup = dedup.map(x => x.toString());

            // const final = [...new Set(dedup)];
            // console.log(final);
            // console.log("count "  + final.length);
            // total += final.length;
        }
    });
    const endTime = Date.now(); 
    console.log("-------- Part 2("+ (endTime-startTime) +"ms):" + total);
}

function countPossibleMatches(pattern) {

    var total = 0;
    paths.filter(x => x.pattern == pattern).forEach(n => {
        console.log(n);
        console.log(countArrangement(n,[]));
    });
    return total;
    
};

function countArrangement(node) {
    
    if (Object.keys(node.towel).length == 0) {
            return [];
    }

    var newPath = [];
    if (paths[node.pattern]) {
        return paths[node.pattern];
    }

    Object.keys(node.towel).forEach(t => {
        var towelPath = [];
        var tmpPath = [];
        
        if (node.towel[t].length == 0) {
            towelPath.push([t]); 
        } else {
            node.towel[t].forEach(next => {   
                tmpPath = [];        
                countArrangement(solutions[next]).forEach(tp => {
                    if (towelPath.length == 0) {
                        tmpPath.push([...new Set([t,...tp])]);
                    } else {
                        towelPath.forEach(tpa => {
                            tmpPath.push([...new Set([...tpa,...tp])]);
                        })
                    }
                });
                towelPath = tmpPath;
            });
        }
        towelPath.forEach(p => {
            newPath.push(p);
        })              
    })
    
    paths[node.pattern] = newPath;

    return newPath;
}

// fast no recording of solutions
function contains(pattern,towels) {
    
    if (pattern == "") {
        return true;
    }
    if (cache[pattern] != undefined) {
        return cache[pattern];
    }
    var match = false;

    for (var i=0; i<towels.length; i++) {
        var towel = towels[i];
        if (pattern.indexOf(towel) != -1) {
            match = (contains(pattern.slice(0,pattern.indexOf(towel)),towels) && contains(pattern.slice(pattern.indexOf(towel)+towel.length),towels)) || match;        
        }
        // if (match) {
        //     break;
        // }
    }
    
    cache[pattern] = match;
    return match;
}

function count(pattern,towels) {
    
    if (pattern == "") {
        return 1;
    }
    if (cache2[pattern] != undefined) {
        return cache2[pattern];
    }
    var match = 0;

    for (var i=0; i<towels.length; i++) {
        var towel = towels[i];
        if (pattern.indexOf(towel) == 0) {
            match += count(pattern.slice(pattern.indexOf(towel)+towel.length),towels);        
        }
    }
    
    cache2[pattern] = match;
    return match;
}

// record path
function recordPaths(pattern,towels) {
    
    if (pattern == "") {
        return true;
    }
    var match = false;
    if (solutions[pattern] != undefined) {
        return solutions[pattern].match;
    }
    var newPattern = {
        pattern : pattern,
        match : false,
        towel:{}
    }

    for (var i=0; i<towels.length; i++) {
        var towel = towels[i];
     
        // var e = solutions[pattern+"-"+towel];
        // if (e) {
        //     match = (e.match || match);
        //     continue;
        // }
        if (pattern.indexOf(towel) != -1) {
            const left = pattern.slice(0,pattern.indexOf(towel));
            const right = pattern.slice(pattern.indexOf(towel)+towel.length);
            var possible = recordPaths(left,towels) && recordPaths(right,towels);

            if (possible) {
                newPattern.towel[towel] = [];
                if (left != "") {
                    newPattern.towel[towel].push(left);
                }
                if (right != "") {
                    newPattern.towel[towel].push(right);
                }
            }
            
            match = possible || match;        
        }
    }
    newPattern.match = match;
    solutions[pattern] = newPattern;
    
    return match;
}