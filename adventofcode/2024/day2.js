
const fs = require('fs')

fs.readFile('day2.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }

  const lines = data.split(/\r?\n/);
  const reports = [];

  lines.forEach((line) => {
    const rep = line.split(" ");
    reports.push(rep.map(x => parseInt(x)));
  });

  solve(reports);
  solve2(reports);

});

function solve (reports) {

    var total =0;
    reports.forEach(rep => {
      if (isSafe(rep,false)) {
        total++;
      }
    });
    
    console.log("-------- Part 1:" + total);
}

function solve2 (reports) {
  var total =0;

  reports.forEach(rep => {
    if (isSafe(rep,true)) {
      total++;
    }
  });
  
  console.log("-------- Part 2:" + total);

}

function isSafe(rep,dampener) {

  var asc;
  var safe = true;
  var problemIdx = 0;

  if (rep[rep.length-1]-rep[0] > 0) {
    asc = true;
  } else {
    asc = false;
  }

  for (var i=0; i<rep.length-1;i++) {

    problemIdx = i;
    if (asc && rep[i+1]-rep[i] <= 0) {
      safe = false;
      break;
    }
    if(!asc && rep[i+1]-rep[i] >= 0) {
      safe = false;
      break;
    }
    if (Math.abs(rep[i+1] - rep[i]) > 3) {
      safe = false;
      break;
    }
  }

  // check problem Dampener
  if (dampener && !safe) {
    if(!isSafe(rep.toSpliced(problemIdx,1),false) && !isSafe(rep.toSpliced(problemIdx+1,1),false)) {
      safe = false;
     } else {
      safe = true;
    }
  }
  return safe;

}
