const fs = require('fs')

const moves = [
    [-1,0],
    [1,0],
    [0,-1],
    [0,1]
]

var allCheats = [];
const results = {};

fs.readFile('day20.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const map = [];

    lines.forEach(line => {
        map.push(line.split(""));
    })

    solve(map);
    solve2(map);

});

function solve (map) {
    const startTime = Date.now(); 

    const pts = findPoints(map)
    const start = pts["start"];
    const end = pts["end"];

    const targetSaving = 100;
    const cheatTime = 2;
    
    const cheats = foundCheats(map,start,end,targetSaving,cheatTime);
    const endTime = Date.now(); 
    console.log("-------- Part 2("+ (endTime-startTime) +"ms):" + cheats);
}

function solve2 (map) {
    const startTime = Date.now(); 

    const pts = findPoints(map)
    const start = pts["start"];
    const end = pts["end"];

    const targetSaving = 100;
    const cheatTime = 20;
    
    const cheats = foundCheats(map,start,end,targetSaving,cheatTime);
    const endTime = Date.now(); 
    console.log("-------- Part 2("+ (endTime-startTime) +"ms):" + cheats);
}

function raceTime(map,start,end) {

    var pos = start;
    path = [start];
    var time = 0;

    while (pos && !(pos.row == end.row && pos.col == end.col)) {
        pos = move(pos,path,map);
        path.push(pos);
        time++;
    }
    return { time : time, path : path}
}

function foundCheats(map,start,end,targetSaving,cheatTime,debug) {

    const out = raceTime(map,start,end);
    const noCheatPath = out.path;
    const results = {};
    var cheats = 0;

    for (var i=0;i<noCheatPath.length;i++) {
        
        const curr = noCheatPath[i];
        
        // find all points between 2 and cheatTime manathan distance which would reduce the time by targetSaving
        noCheatPath.filter((x,index) => Math.abs(curr.row - x.row) + Math.abs(curr.col - x.col) <= cheatTime && 
                                        index - i - (Math.abs(curr.row - x.row) + Math.abs(curr.col - x.col)) >= targetSaving).forEach(m =>{
            if (debug) {
                const dist = Math.abs(curr.row - m.row) + Math.abs(curr.col - m.col);
                const idx = noCheatPath.findIndex(x => x.row == m.row && x.col == m.col);
                const saving = idx - i - dist;
            
                if (!results[saving]) {
                    results[saving] = 0;
                }
                results[saving]++;

            }
            cheats++
        });
    }
    if (debug) {
        console.log(results);
    }
    return cheats;
}

function move(pos,path,map) {

    for (i=0;i<moves.length;i++) {
        const nextRow = pos.row + moves[i][0];
        const nextCol = pos.col + moves[i][1];

        if (map[nextRow][nextCol] != "#" && path.findIndex(x => x.row == nextRow && x.col == nextCol) == -1) {
            return {
                row : nextRow,
                col : nextCol
            }
        }
    }

}


function findPoints(map) {
    const pts = {};

    for (var i=0;i<map.length;i++) {
        for (var j=0;j<map[0].length;j++) {
                if (map[i][j] == "S") {
                    pts["start"] = {
                        row : i,
                        col : j
                    }
                }
                if (map[i][j] == "E") {
                    pts["end"] = {
                        row : i,
                        col : j
                    }
                }
        } 
    }
    return pts;
}