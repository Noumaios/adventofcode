const fs = require('fs')

const moves = {
    "^" : [-1,0],
    "v" : [1,0],
    "<" : [0,-1],
    ">" : [0,1]
}
const disp = {
    "-1,0" : "^",
    "1,0" : "v" ,
    "0,-1" :"<" ,
    "0,1" : ">"
}

const cache = {};
const strCache = {};

fs.readFile('day21.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const codes = [];
    const k1 = [];
    const k2 = [];

    lines.forEach(line => {
        if (line.indexOf(" ") != -1) {
            k1.push(line.split(" "));
        } else if (line.indexOf(",")  != -1) {
            k2.push(line.split(","));
        } else if (line != "") {
            codes.push(line.split(""));
        }
    })

    // transform the keyboards in pair "key" : "coord"
    const digitKeyboard = mapToKeys(k1);
    const directionalKeyboard = mapToKeys(k2);
    
    const input = {
        codes : codes,
        digitKeyboard : digitKeyboard,
        directionalKeyboard : directionalKeyboard
    }

    console.log(input);
    solve(input);
    solve2(input);

});

function solve (input) {
    const startTime = Date.now(); 
    var total = 0;
    var final = [];
   

    // try with one keyboard
    input.codes.forEach(code => {  

        var min = Infinity;
       
        var num = parseInt(code.filter(x => x != "A").reduce((a,c) => a+c));
        min = minPathForString(code,input,2,2);
       // console.log(code + " ("+ num + "): " + final.reduce((a,c) => a+c) + ":"+final.length);
        console.log(code + " ("+ num + "): " + min);
       // console.log(cache);
        //console.log(code + " NEW ("+ num + "): " + minPathForString(code,input,3,3));
        total += num * min;
    });
    const endTime = Date.now(); 
    
    console.log("-------- Part 1("+ (endTime-startTime) +"ms):" + total);
}

function minPathForString(str,input,start,iteration) {
    
    //console.log(iteration);
    var min = Infinity;

    if (cache[str.reduce((a,c) => a+c) + ":"+iteration]) {
        return cache[str.reduce((a,c) => a+c) + ":"+iteration];
    }

    if (iteration == 0) {      
        pathFor(str,input.directionalKeyboard).forEach(d => {
            min = Math.min(d.length,min);
        })
        cache[str.reduce((a,c) => a+c) + ":"+iteration] = min;
        return min;
    }

    var keyb = input.directionalKeyboard;
    if (start == iteration) {
        keyb = input.digitKeyboard;
    }
   
    const splitForA = str.toString().split(",A,");
    var minForStr = 0;

    for (var i=0;i<splitForA.length;i++) {
        sub = splitForA[i];
        var localMin =Infinity;
        var local;

        if (i <splitForA.length-1) {
            local = sub.split(",").concat("A");
        } else {
            local = sub.split(",")
        }

        pathFor(local,keyb).forEach(d => {
            localMin = Math.min(minPathForString(d,input,start,iteration-1),localMin);
        });
        
        cache[local.reduce((a,c) => a+c) + ":"+iteration] = localMin;
        minForStr += localMin
    }

    cache[str.reduce((a,c) => a+c) + ":"+iteration] = minForStr;
    return minForStr;    
}

function pathFor(str,keyboard) {

    var pos = "A";
    var path = [];
   // console.log(str);
    str.forEach(c => {
        var cPath = [];
        if (path.length == 0) {
            cPath = shortestPath(keyboard,pos,c);
        } else {
            path.forEach(p => {
                shortestPath(keyboard,pos,c).forEach(s => {
                    cPath.push([...p,...s]);
                })
            })
        }
        
        path = cPath;
      
        pos = c;
    })
    
    return path;
}



function shortestPath(keyboard,pos,char) {
     
     var out = [];
     var path = [];
     const gap = keyboard['x'];
 
     const curr = keyboard[pos];
     const dest = keyboard[char];
    //console.log(pos + "-" + char);
     const rowMove = ((dest[0]-curr[0])/Math.abs(dest[0]-curr[0]))+","+0;
     const colMove = "0,"+(dest[1]-curr[1])/Math.abs(dest[1]-curr[1]);
     
     
     if (curr[0] == gap[0] && dest[1] == gap[1]) {
        // same row as gap go vertical first
        for (var i=0; i<Math.abs(dest[0]-curr[0]);i++) {
            path.push(disp[rowMove]);
        }
        for (var i=0; i<Math.abs(dest[1]-curr[1]);i++) {
            path.push(disp[colMove]);
        }
        path.push("A");
        out.push(path);
     } else if (curr[1] == gap[1] && dest[0] == gap[0]) {
        // same col as gap go horizontal first
        for (var i=0; i<Math.abs(dest[1]-curr[1]);i++) {
            path.push(disp[colMove]);
        }
        for (var i=0; i<Math.abs(dest[0]-curr[0]);i++) {
            path.push(disp[rowMove]);
        }
        path.push("A");
        out.push(path);
     } else if (dest[0] != curr[0] && dest[1] != curr[1] ){
        // if on different line / col
        for (var i=0; i<Math.abs(dest[0]-curr[0]);i++) {
            path.push(disp[rowMove]);
        }
        for (var i=0; i<Math.abs(dest[1]-curr[1]);i++) {
            path.push(disp[colMove]);
        }
        path.push("A");
        out.push(path);

        path = [];

        for (var i=0; i<Math.abs(dest[1]-curr[1]);i++) {
            path.push(disp[colMove]);
        }
        for (var i=0; i<Math.abs(dest[0]-curr[0]);i++) {
            path.push(disp[rowMove]);
        }
        path.push("A");
        out.push(path);
     } else {
        for (var i=0; i<Math.abs(dest[1]-curr[1]);i++) {
            path.push(disp[colMove]);
        }
        for (var i=0; i<Math.abs(dest[0]-curr[0]);i++) {
            path.push(disp[rowMove]);
        }
        path.push("A");
        out.push(path);
     }

     return out

}
function solve2 (input) {
    const startTime = Date.now(); 
    var total = 0;
   

     // try with one keyboard
     input.codes.forEach(code => {  

        var min = Infinity;

        var num = parseInt(code.filter(x => x != "A").reduce((a,c) => a+c));
        min = minPathForString(code,input,25,25);
        console.log(code + " ("+ num + "): " + min);

        total += num * min;
    });
    const endTime = Date.now(); 
    console.log("-------- Part 2("+ (endTime-startTime) +"ms):" + total);
}

function mapToKeys(keyboard) {
    const map = {};
    for (var i=0; i< keyboard.length; i++)  {
        for (var j=0; j< keyboard[0].length;j++) {
            map[keyboard[i][j]] = [i,j];
        }
    }
    return map;
}

function manathanDistance(a,b) {
    return Math.abs(b[0]-a[0]) + Math.abs(b[1]-a[1]);
}