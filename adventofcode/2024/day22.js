const fs = require('fs')
const cache = {};

fs.readFile('day22.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/).map(x => parseInt(x));
   
    solve(lines);
    solve2(lines);

});

function solve (input) {
    const startTime = Date.now(); 
    var total = 0;
   
    input.forEach(i=> {
        total += nextSecret(i,2000)
    })
    const endTime = Date.now(); 
    
    console.log("-------- Part 1("+ (endTime-startTime) +"ms):" + total);
}

function solve2 (input) {

    const startTime = Date.now(); 
    var total = 0;

    var prices = [];
    var priceChanges = [];
    input.forEach(i=> {
        prices.push(pricesList(i,2000));
    })

    prices.forEach(p => {
        const changes = [];
        for (var i=4;i<p.length;i++) {
            const c = (p[i-3]-p[i-4]) + "," + (p[i-2]-p[i-3]) + "," + (p[i-1]-p[i-2]) + "," + (p[i]-p[i-1]);
            changes.push(c);
        }
        priceChanges.push(changes);
    })

    var max = 0;
    priceChanges.forEach((pc,i)=> {
        console.log(i);
        // for each buyer pick every price changes list and retreive the prices from the others
        pc.forEach(change => {      
            if (!cache[change]) {
                
                cache[change] = priceForChanges(change,prices,priceChanges);
            } 
            
            max = Math.max(cache[change],max);
        });
        console.log("cache size " + Object.keys(cache).length);
    });

    total = max;

    const endTime = Date.now(); 
    console.log("-------- Part 2("+ (endTime-startTime) +"ms):" + total);
}

function priceForChanges(change,prices,priceChanges) {

    var tot = 0;
    for (var j=0;j<priceChanges.length;j++) {
        const idx = priceChanges[j].indexOf(change);
        if (idx != -1) {
            tot += prices[j][idx+4];
        }
    }
    return tot;
}

function nextSecret(num,iteration) {

    var secret = num;
    for (var i=0;i<iteration;i++) {
        var step1 = prune(mix(secret*64,secret));
        var step2 = prune(mix(parseInt(Math.floor(step1/32)),step1));
        var step3 = prune(mix(step2*2048,step2));
        secret = step3;
        //console.log(secret);
    }
    return secret;
}
function pricesList(num,iteration) {

    var prices = [];
    var next = num;
    prices.push(parseInt(num.toString().slice(num.toString().length-1)));

    for (var i=0;i<iteration;i++) {
        next = nextSecret(next,1);
        prices.push(parseInt(next.toString().slice(next.toString().length-1)));
    }
    return prices;
}
function mix(a,b) {
    return parseInt((a^b) >>> 0);
}

function prune(a) {
    return a%16777216;
}