const fs = require('fs')
const cache = {};

fs.readFile('day23.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const network = [];
    lines.forEach(l => {
        const pair = l.split("-").sort();
        network.push(pair);
    });
    solve(network);
    solve2(network);

});

function solve (input) {
    const startTime = Date.now(); 
    var total = 0;
    console.log(input);

    const networks = new Graph();

    input.forEach(pair => {
        
        // add vertices
        networks.addVertex(pair[0]);
        networks.addVertex(pair[1]);
        // and edges
        networks.addEdge(pair[0],pair[1]);

    });
    const match = networks.threeNodesNewtork("t");
    networks.printGraph();
    
    const endTime = Date.now(); 
    
    console.log("-------- Part 1("+ (endTime-startTime) +"ms):" + match.size);
}

function solve2 (input) {

    const startTime = Date.now(); 
    var total = 0;
    console.log(input);

    const networks = new Graph();

    input.forEach(pair => {
        
        // add vertices
        networks.addVertex(pair[0]);
        networks.addVertex(pair[1]);
        // and edges
        networks.addEdge(pair[0],pair[1]);

    });

    const match = networks.allNewtorks("t");
    console.log(match);
    const sortedNet = [];
    for (const net of match) {
        sortedNet.push(
        { length : net.length,
          network : net
         });
    }
    console.log(sortedNet.sort((a,b) => b.length - a.length)[0]);
   // networks.printGraph();
    
    const endTime = Date.now(); 
    console.log("-------- Part 2("+ (endTime-startTime) +"ms):" + total);
}

// create a graph class
class Graph {
    // defining vertex array and
    // adjacent list
    constructor(noOfVertices)
    {
        this.noOfVertices = noOfVertices;
        this.AdjList = new Map();
    }

    // add vertex to the graph
    addVertex(v) {
        // initialize the adjacent list with a
        // null array
        if (!this.AdjList.has(v)) {
            this.AdjList.set(v, []);
        }
    }
    addEdge(v, w) {
        // vertex w denoting edge between v and w
        this.AdjList.get(v).push(w);

        // Since graph is undirected,
        // add an edge from w to v also
        this.AdjList.get(w).push(v);
    }

    threeNodesNewtork(pattern) {
        const net = new Set();
        var nodes = [...this.AdjList];
        
        for (var i=0;i<nodes.length-1;i++) {
            const a = nodes[i];
            
            for (var j=i+1;j<nodes.length;j++) {
                const b = nodes[j];
                const matches = a[1].filter(x => b[1].includes(x) && b[1].includes(a[0]));
                console.log(a[0] + " " + b[0] + " : " + matches);
                
                matches.forEach(m => {
                    if (a[0].startsWith(pattern) || b[0].startsWith(pattern) || m.startsWith(pattern)) {
                        const n = [a[0],b[0],m].sort().toString();
                         net.add(n);
                    }
                })
            }
        }

        return net;
    }

    allNewtorks(pattern) {
        const net = new Set();
        var nodes = [...this.AdjList];
        
        for (var i=0;i<nodes.length-1;i++) {
            const a = nodes[i];
            console.log(a[0]);
            net.add(this.networksContaining([a[0]],nodes.toSpliced(i,1)).toString());
        }

        return net;
    }

    networksContaining(arr,remaining) {
        const net = new Set();
        var nodes = [...this.AdjList];
        
        if (remaining.length == 0) {
            return arr;
        }
        var match = false;
        for (var i=0;i<remaining.length-1;i++) {
            const a = remaining[i];
 
            const matches = arr.every(x => a[1].includes(x));
            if (matches) {
                match = true;
                return this.networksContaining(arr.concat(a[0]).sort(),remaining.toSpliced(i,1));
            }
            
        }
        if (!match) {
            return arr;
        }
    }

    printGraph() {
         // get all the vertices
    var get_keys = this.AdjList.keys();

    // iterate over the vertices
    for (var i of get_keys) 
{
        // get the corresponding adjacency list
        // for the vertex
        var get_values = this.AdjList.get(i);
        var conc = "";

        // iterate over the adjacency list
        // concatenate the values into a string
        for (var j of get_values)
            conc += j + " ";

        // print the vertex and its adjacency list
        console.log(i + " -> " + conc);
    }
    }

    // bfs(v)
    // dfs(v)
}