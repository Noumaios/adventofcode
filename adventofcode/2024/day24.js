const fs = require('fs')
const cache = {};

const [wiresRaw,gatesRaw] = fs
    .readFileSync("day24.txt","UTF-8")
    .trim()
    .split("\n\n")
    .map(n => n.split("\n"));


const wires = {};
const gates = [];

wiresRaw.forEach(w => {
    const [name, value] = w.split(": ");
    wires[name] = parseInt(value);
})

gatesRaw.forEach(g => {
    const [inputs,output] = g.split(" -> ");
    const [a,op,b] = inputs.split(" ");
    gates.push({a,op,b,output});

    // initialise missing wires
    if (wires[a] == undefined) wires[a] = null;
    if (wires[b] == undefined) wires[b] = null;
    if (wires[output] == undefined) wires[output] = null;    
})

// number of bits in the final result
const maxZ = Object.keys(wires).filter(x => x.startsWith("z")).length - 1;

solve(wires,gates);
solve2(wires,gates);

function solve (wires,gates) {
    const startTime = Date.now(); 

    const resultWires = execute(gates,{...wires});
    const out = getNumberFromWires("z",resultWires);
    const endTime = Date.now(); 
    
    console.log("-------- Part 1("+ (endTime-startTime) +"ms):" + out);
}

/*
* Ripple Carry Adder (daisy chaining Full Adder)
* Inputs :  A,B and Cin (carry from previous less significant bit)
* Outputs : Sum and Cout (carry for next bit)
* A    XOR  B    -> VAL0     <=     FAGate0
* A    AND  B    -> VAL1     <=     FAGate1
* VAL0 AND  Cin  -> VAL2     <=     FAGate2
* VAL0 XOR  Cin  -> Sum      <=     FAGate3
* VAL1 OR   VAL2 -> Cout     <=     FAGate4
*
* Now check the gates construction matches a FA logic and flag every potential issue
*
*/
function solve2 (wires,gates) {

    const startTime = Date.now(); 
    const issues = new Set();

    // Check FAGate0 construction
    // XOR gate with An,Bn input can't output to a Z wire (Except first one)
    const FAGates0 = gates.filter(x => x.op == "XOR" && (x.a.startsWith("x") || x.b.startsWith("x")));
    FAGates0.forEach(g => {
        if (g.output.startsWith("z") && !(g.output === "z00")) {
            issues.add(g.output);
        }
    })
    console.log("Issues with FAGates0 : ");
    console.log(issues);

    // Check FAGate3 construction
    // XOR gate no having An,Bn input need to output to a Z wire
    const FAGates3 = gates.filter(x => x.op == "XOR" && !x.a.startsWith("x") && !x.b.startsWith("x"));
    FAGates3.forEach(g => {
        if (!g.output.startsWith("z")) {
            issues.add(g.output);
        }
    });
    console.log("Issues with FAGates3 : ");
    console.log(issues);

    // Check that AND gates don't output to a z wire
    gates.filter(x => x.op == "AND").forEach(g => {
        if (g.output.startsWith("z")) {
            issues.add(g.output);
        }
    });
    console.log("Issues with AND gates (z output) : ");
    console.log(issues);

    // Check that AND gates output to an OR gate (Except first one which shoudl output to an XOR and AND gate)
    gates.filter(x => x.op == "AND" && !(x.a.endsWith("00") && x.b.endsWith("00"))).forEach(g => {
        gates.filter(x => x.op != "OR" && (x.a == g.output || x.b == g.output)).forEach(NotORGate => {  
            issues.add(g.output);
        });
    });
    console.log("Issues with AND gates (not OR output) : ");
    console.log(issues);


    // Check that OR gates don't output to a z wire Except for the last one
    gates.filter(x => x.op == "OR").forEach(g => {
        if (g.output.startsWith("z") && g.output != ("z" + maxZ)) {
            issues.add(g.output);
            
        }
    });
    console.log("Issues with OR gates (not OR output) : ");
    console.log(issues);

    // all FAGates0 must output to an FAGate3 (except first one)
    FAGates0.forEach(g=> {

        const n = FAGates3.filter(x => x.a == g.output || x.b == g.output).length;
        if (n == 0 && g.output != "z00") {
            issues.add(g.output);
        }
    });
    console.log("Issues with FAGates0 not outputing to FAGates3 : ");
    console.log(issues);

    const password = Array.from(issues).sort();

    const endTime = Date.now(); 

    console.log("-------- Part 2("+ (endTime-startTime) +"ms):" + password);
}

function executeGate(gate,wires) {
   
    if (gate.op == "AND") {
        wires[gate.output] = wires[gate.a] && wires[gate.b];
    }
    if (gate.op == "OR") {
        wires[gate.output] = wires[gate.a] || wires[gate.b];
    }
    if (gate.op == "XOR") {
        wires[gate.output] = wires[gate.a] ^ wires[gate.b] >>> 0;
    }
}

function execute(gates,wires) {
    var remaining = [...gates];

    while (remaining.length > 0) {
        const remain = [];
        remaining.forEach(gate => {

            if (wires[gate.a] != null && wires[gate.b] != null) {
                executeGate(gate,wires);
            } else {
                remain.push(gate);
            }
        });
        remaining = [...remain];
    }
    return wires;
}

function getNumberFromWires(pattern,wires) {
    var out = "";

    Object.keys(wires).filter(x => x.startsWith(pattern)).sort().forEach(w => {
        out = wires[w] + out
     })
     console.log(pattern + " : " + out);
     var digit = parseInt(out,2);
     return digit;
 }


 function wiresFromOutput(o,gates) {

    const gate = gates.find(x=> x.output == o);
    if (gate) { 
        console.log(gate);
        var count = 0;
        gate.input.forEach(i => { 
            count+=wiresFromOutput(i,gates);
        })
        return count;
    } else {
        return 1;
    }
    

 }