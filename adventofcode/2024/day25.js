const fs = require('fs')
const cache = {};

var height = 0;
var schemaHeight = 0;

fs.readFile('day25.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const schematics = [];

    var schematic = [];
    lines.forEach(line => {
        if (line == "") {
            schematics.push(schematic);
            schematic = [];
        } else {
            schematic.push(line.split(""));
        }
    });
    schemaHeight = schematics[0].length;
    height = schemaHeight -2;

    schematics.push(schematic);

    // schematics to array of keys and locks
    const keysAndLocks = processSchematics(schematics);


    // console.log(schematics);
    // keysAndLocks.locks.forEach(l => {
    //     console.log(l);
    // })
    // keysAndLocks.keys.forEach(k => {
    //     console.log(k);
    // })
    

    solve(keysAndLocks);
    solve2(lines);

});

function solve (input) {
    const startTime = Date.now(); 
    var total = 0;
   
    // schematics to heights
    input.locks.forEach(lock => {
        input.keys.forEach(key=> {
            var fit = match(lock,key);
            console.log("lock " + lock.heights + " key " + key.heights + " : " + fit);
            if (fit) {
                total++;
            }
        })
    })
    const endTime = Date.now(); 
    
    console.log("-------- Part 1("+ (endTime-startTime) +"ms):" + total);
}

function solve2 (input) {

    const startTime = Date.now(); 
    var total = 0;


    const endTime = Date.now(); 
    console.log("-------- Part 2("+ (endTime-startTime) +"ms):" + total);
}

function match(key,lock) {

    for (var i=0; i<key.heights.length;i++) {
        const k = key.heights[i];
        const l = lock.heights[i];
        if (l+k > height) {
            return false;
        }
    }
    return true;
}

function processSchematics(schematics) {

    const out = {
        keys : [],
        locks : []
    }

    schematics.forEach(s => {
       
        if (s[0].every(x => x == "#") && s[schemaHeight-1].every(x => x == ".")) {
            // lock, get the height of each pins
            var heights = [];
            for (var i=0; i<s[0].length;i++) {
                var h = height;
                for (var j=1;j<=height;j++) {
                    if (s[j][i] == ".") {
                        h = j-1;
                        break;
                    }
                }
                heights.push(h);
            }
            const lock = {
                schema : s,
                heights : heights
            }
            out.locks.push(lock);
        } else {
            var heights = [];
            for (var i=0; i<s[0].length;i++) { 
                var h = height;
                for (var j=height;j>=1;j--) {
                    if (s[j][i] == ".") {
                        h = height-j;
                        break;
                    }
                }
                heights.push(h);
            }
            const key = {
                schema : s,
                heights : heights
            }
            out.keys.push(key);
        }
    });

    return out;
}