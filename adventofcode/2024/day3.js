
const fs = require('fs')

fs.readFile('day3.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }

  solve(data);
  solve2(data);

});

function solve (data) {

    console.log("-------- Part 1:" + run(data));
}

function solve2 (data) {
    var total =0;
    
    const donts = data.split("don't()");
    
    for (var i=0; i <donts.length; i++) {
        // first index is valid
        if (i==0) {
            total += run(donts[i]);
        } else {     
            const dos = donts[i].split("do()");
            for (var j=1;j<dos.length;j++) {
                total+=run(dos[j]);
            } 
        }
    }
  
  console.log("-------- Part 2:" + total);

}

function run(prog) {
    var total = 0;

    const p = /mul\([0-9]+,[0-9]+\)/g;
    const n = /[0-9]+/g;
    const res = [...prog.matchAll(p)];

    // get the instructions;
    res.forEach(d => {
        const ops = [...d[0].matchAll(n)];
        
        total += parseInt(ops[0][0])*parseInt(ops[1][0]);
    });

    return total;
}
