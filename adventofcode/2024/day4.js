const fs = require('fs')

var maxX;
var maxY;
const xmas = ['X','M','A','S'];
const mas = ['M','A','S'];
const sam = ['S','A','M'];

fs.readFile('day4.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const puzzle = [];
    
    lines.forEach(line => {
        puzzle.push(line.split(""));
    });
    
    maxX = puzzle.length;
    maxY = puzzle[0].length;
    
    solve(puzzle);
    solve2(puzzle);

});

function solve (data) {
    var total =0;
    
    for (var i=0;i<maxX;i++) {
        for (var j=0;j<maxY;j++) {
            if (data[i][j]=="X") {
                total+= check(data,i,j);
            }
        }
    }

    console.log("-------- Part 1:" + total);
}

function solve2 (data) {
    var total =0;

    for (var i=1;i<maxX-1;i++) {
        for (var j=1;j<maxY-1;j++) {
            if (data[i][j]=="A") {
                if ((checkDirection(data,mas,i-1,j-1,1,1,0) || checkDirection(data,sam,i-1,j-1,1,1,0)) &&
                    (checkDirection(data,mas,i+1,j-1,-1,1,0) || checkDirection(data,sam,i+1,j-1,-1,1,0))) {
                        total++;
                    }       
            }
        }
    }

    console.log("-------- Part 2:" + total);

}

// check for XMAS in all direction from X and return the number
function check(arr,x,y) {
   
    var found = 0;
    
    for (var i=-1;i<2;i++) {
        for (var j=-1;j<2;j++) {
            if (checkDirection(arr,xmas,x,y,i,j,0)) {
                found++;
            }
        }
    }

    return found;
}

// check for a matching word from a given position and direction
function checkDirection(arr,word,x,y,xdir,ydir,step) {
    
    if (x+xdir*step==maxX || y+ydir*step == maxY || x+xdir*step==-1 || y+ydir*step==-1) {
        return false;
    }

    if (arr[x+xdir*step][y+ydir*step]==word[step]) {
        if (step==word.length-1) {
            return true;
        } else {
            return checkDirection(arr,word,x,y,xdir,ydir,step+1);
        }
    } else {
        return false;
    } 


}