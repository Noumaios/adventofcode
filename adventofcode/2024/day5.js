const fs = require('fs')

fs.readFile('day5.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const rules = {};
    const pages = [];
    
    lines.forEach(line => {
        if (line.includes("|")) {
            const rule = line.split("|");
            if (!rules[rule[0]]) {
                rules[rule[0]] = {
                    greaters : []
                }
            }
            rules[rule[0]].greaters.push(rule[1]);
        } else {
            if (line.includes(",")) {
                pages.push(line.split(","));
            }
        }
    });
    console.log(rules);
    
    solve(rules,pages);
    solve2(rules,pages);

});

function solve (rules,pages) {
    var total =0;
    var total2=0; 

    pages.forEach(p => {
        const sorted = quicksort(p,rules);
        if (equal(p,sorted)) {
            total += parseInt(p[Math.floor(p.length/2)]);
        } else {
            total2 += parseInt(sorted[Math.floor(sorted.length/2)]);
        }
    })
    console.log("-------- Part 1:" + total);
    console.log("-------- Part 2:" + total2);
}

function solve2 (rules,pages) {
    var total =0;
}

function quicksort(arr,rules) {
    if (arr.length <= 1 ) {
        return arr;
    }
    const arr1 =[];
    const arr2 = []; 
    const pivot = arr[0];

    for (var i=1;i<arr.length;i++) {
        if (lowerThan(arr[i],pivot,rules)) {
            arr1.push(arr[i]);
        } else {
            arr2.push(arr[i]);
        }
    }

    return [].concat(quicksort(arr1,rules),[pivot],quicksort(arr2,rules));
}


function lowerThan(a,b,rules) {

    if (rules[a] && rules[a].greaters.includes(b)) {
        return true;
    } else {
        return false;
    }
    
}

function equal(arr1,arr2) {

    for (var i=0;i<arr1.length;i++){
        if (arr1[i] != arr2[i]) {
            return false
        }
    }
    return true;
}