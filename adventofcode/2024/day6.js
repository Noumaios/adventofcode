const fs = require('fs')

var maxX;
var maxY;

fs.readFile('day6.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const map = [];
    const map2 =[];

    lines.forEach(line => {
        map.push(line.split(""));
        map2.push(line.split(""));
    });
    
    maxX = map[0].length;
    maxY = map.length;
    
    print(map);
    solve(map);
    solve2(map2);

});

function solve (map) {
    var total =1;
    
    const start = findStart(map);
    var direction = {x:0,y:-1};
    var position = start;

    while(move(position,direction,map)) {
        //mark the spot
        if (!(map[position.y][position.x] == 'X')) {
            map[position.y][position.x] = 'X';
            total++;
        }
    }

    print(map);
    console.log("-------- Part 1:" + total);
}

function solve2 (map) {

    var total =0;
    const start = findStart(map);
    var direction = {x:0,y:-1};
    
    for (var y=0;y<maxY;y++) {
        for (var x=0;x<maxX;x++) {
            if (map[y][x] == '#' || map[y][x] == '^') {
                continue;
            } else {
                map[y][x]="#";
                if (canLoop(start,direction,map)) {
                    total++;
                }   
                map[y][x]=".";
            }
        }
    }

    console.log("-------- Part 2:" + total);

}


function print(map) {

    for (var i=0; i<maxY;i++){
        for (var j=0;j<maxX;j++){
            process.stdout.write(map[i][j]);
        }
        console.log("");
    }
    console.log("");
}


function findStart(map) {

    for (var i=0; i<maxY;i++){
        for (var j=0;j<maxX;j++){
            if (map[i][j] == "^") {
                return {x:j,y:i};
            }
        }
    }
}

function move(position,direction,map) {

    var newx = position.x+direction.x;
    var newy = position.y+direction.y;

    if (newx < 0 || newx>=maxX || newy<0 || newy>=maxY) {
        // out of bound
        return false;
    }
    if (map[newy][newx] == "#") {
        // obstacle, turn right on the spot
        var x = direction.x;
        direction.x=direction.y*-1;
        direction.y=x;
    } else {
        // move 1 step ahead
        position.x=newx;
        position.y=newy;
    }

    return true;
}

function canLoop(start,dir,map) {
   
    var position = {
        x : start.x,
        y : start.y
    };
    var direction = {
        x : dir.x,
        y : dir.y 
    }
    const path = {};
    path[position.x+"-"+position.y+"-"+direction.x+"-"+direction.y];

    while(move(position,direction,map)) {
          
        if (path[position.x+"-"+position.y+"-"+direction.x+"-"+direction.y]) {
            return true;
        } else {
            path[position.x+"-"+position.y+"-"+direction.x+"-"+direction.y]=1;
        }

    }

    return false;
}