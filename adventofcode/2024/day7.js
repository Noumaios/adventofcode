
const fs = require('fs')

fs.readFile('day7.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }

  const lines = data.split(/\r?\n/);
  const calibrations = [];

  lines.forEach((line) => {
    const c = line.split(": ");
    const cal = {
        result : parseInt(c[0]),
        operands :c[1].split(" ").map(x => parseInt(x))
    }
    calibrations.push(cal);
  });

  solve(calibrations);
  solve2(calibrations);

});

function solve (cal) {

    var total =0;

    cal.forEach(c=> {
        if (test(c,false)) {
            total+=c.result;
        }
    });

    console.log("-------- part 1:" + total);
}

function solve2(cal) {

    var total =0;

    cal.forEach(c=> {
        if (test(c,true)) {
            total+=c.result;
        }
    });

    console.log("-------- part 2:" + total);
}
function test(cal,concat) {
   
    if (cal.operands.length == 2) {
        return ((cal.result == (cal.operands[0] + cal.operands[1])) || 
                (cal.result == (cal.operands[0] * cal.operands[1])) ||
                (concat && (cal.result == (parseInt(cal.operands[0].toString()+cal.operands[1].toString())))))
    } else {
        const ops = [...cal.operands];
        const c1 = ops.shift();
        const c2 = ops.shift();
        const cal1 = {
            result : cal.result,
            operands : [c1+c2,...ops]
        }
        const cal2 = {
            result : cal.result,
            operands : [c1*c2,...ops]
        }
        const cal3 = {
            result : cal.result,
            operands : [parseInt(c1.toString()+c2.toString()),...ops]
        }
        return (test(cal1,concat) || test(cal2,concat) || (concat && test(cal3,concat)))
    }
}