const fs = require('fs')

var maxLine;
var maxCol;

fs.readFile('day8.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const lines = data.split(/\r?\n/);
    const map = [];


    lines.forEach(line => {
        map.push(line.split(""));
    });
    
    maxLine = map[0].length;
    maxCol = map.length;
    
    const antennas = {} 
    for (var l=0;l<maxLine;l++) {
        for (var c=0;c<maxCol;c++){
            if (map[l][c] != ".") {
                // Antenna here
                if (!antennas[map[l][c]]) {
                    antennas[map[l][c]] = []
                }
                antennas[map[l][c]].push([l,c]);
            }
        }
    }

    solve(map,antennas);
    solve2(map,antennas);

});

function solve (map,antennas) {
    var total=0;
    const antiNodes = {};
    
    for (a in antennas) {
        antinodesForAntenna(antiNodes,antennas[a]);
    }

    // print map with antinodes
    for (let a in antiNodes) {
        const coord = a.split("-").map(x => parseInt(x));
        if (map[coord[0]][coord[1]] == ".") {
            map[coord[0]][coord[1]] ="#"
        }
    }
    print(map);

    console.log("-------- Part 1:" + Object.keys(antiNodes).length);
}

function solve2 (map,antennas) {

    const antiNodes = {};
    
    for (a in antennas) {
        antinodesForAntenna(antiNodes,antennas[a],true);
    }

    // now add the antennas if in pair
    for (a in antennas) {
        if (antennas[a].length >=2) {
            antennas[a].forEach(ant=> {
                addAntinode(antiNodes,ant[0],ant[1]);
            })
        }
    }
    
    // print map with antinodes
    for (let a in antiNodes) {
        const coord = a.split("-").map(x => parseInt(x));
        if (map[coord[0]][coord[1]] == ".") {
            map[coord[0]][coord[1]] ="#"
        }
    }
    print(map);
    
    console.log("-------- Part 2:" + (Object.keys(antiNodes).length));

}


function print(map) {

    for (var i=0; i<maxLine;i++){
        for (var j=0;j<maxCol;j++){
            process.stdout.write(map[i][j]);
        }
        console.log("");
    }
    console.log("");
}

function antinodesForAntenna(antiNodes,antennas,resonance) {

    var antinodes=0;
    
    for (var i=0;i<antennas.length;i++) {
        for (var j=i+1;j<antennas.length;j++) {
            
            const a = antennas[i];
            const b = antennas[j];

            lDist = a[0]-b[0];
            cDist = a[1]-b[1];
            
            if (resonance) {
                var h=1;
                while(inBounds(a[0]+h*lDist,a[1]+h*cDist)) {
                    addAntinode(antiNodes,a[0]+h*lDist,a[1]+h*cDist);
                    h++;
                }
                h=1;
                while(inBounds(b[0]-h*lDist,b[1]-h*cDist)) {
                    addAntinode(antiNodes,b[0]-h*lDist,b[1]-h*cDist);
                    h++;
                }
            } else {
                if (inBounds(a[0]+lDist,a[1]+cDist)) {
                    addAntinode(antiNodes,a[0]+lDist,a[1]+cDist);
                }
                if (inBounds(b[0]-lDist,b[1]-cDist)) {
                    addAntinode(antiNodes,b[0]-lDist,b[1]-cDist);
                }
            }

        }
    }
    
}

function inBounds(l,c) {
    if (l < maxLine && l >=0 && c >=0 && c < maxCol) {
        return true;
    } else {
        return false;
    }
}

function addAntinode(antinodes,l,c) {
    if (!antinodes[l+"-"+c]) {
        antinodes[l+"-"+c] = 0;
    }
    antinodes[l+"-"+c]++;
}