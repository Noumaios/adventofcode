const fs = require('fs')

fs.readFile('day9.txt', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    const disk = data.split("").map(x => parseInt(x));
    
    const files = [];

    const hdd = [];
    var inode = 0;

    for (var i=0;i<disk.length;i++) {
        if (i%2 == 0) {
            files.push({ inode : inode,free:false,id: i/2, size : disk[i]});
            for (var j=0;j<disk[i];j++) {
                hdd.push(i/2);
            }
        } else {
            files.push({ inode : inode,free : true,id:null, size : disk[i]});
            for (var j=0;j<disk[i];j++) {
                hdd.push(".");
            }
        }
        inode += disk[i];
    }

   
    solve(hdd);
    solve2(files);

});

function solve (hdd) {
   
    compact(hdd);
    console.log("-------- Part 1:" + checksum(hdd));

}

function solve2 (files,free) {
    
    defrag(files);
    console.log("-------- Part 2:" + checksum(files,true));

}

function compact(hdd) {

    var i = hdd.length-1;
    while(i > hdd.indexOf(".")) {

        if (hdd[i] != ".") {
            hdd[hdd.indexOf(".")] = hdd[i];
            hdd[i] = ".";
        }
        i--;
    }
}

function defrag(files) {

    var idx = files.length-1;
    while (idx > 0) {

        const f = files[idx];

        // if not free space move to leftmost
        if (!f.free) {
            const freespace = files.filter(x => x.free && x.size >= f.size && x.inode < f.inode);
            if (freespace.length > 0) {
                const o = f.inode;
                f.inode = freespace[0].inode;

                // move the free blocks
                freespace[0].inode += f.size;
                freespace[0].size -= f.size;

                const newFree = {
                    inode : o,
                    id : null,
                    free:true,
                    size:f.size
                }
                files.push(newFree);
            }
        }
        idx--;
    }
}



function checksum(hdd,part2) {

    var c = BigInt(0);
    if (!part2) {
        for (var i=0;i<hdd.length;i++) {
            c += BigInt(i)*(hdd[i]=="."?BigInt(0):BigInt(hdd[i]));
        }
    } else {
        hdd.filter(x => !x.free).forEach(f => {
            for (var i=0;i<f.size;i++) {
                c += BigInt(f.id)*BigInt(f.inode+i);
            }
        });
    }
    return c;
}

function print(hdd) {

    hdd.forEach(e => {
        process.stdout.write(e.toString());
    })

    console.log();
}

function printDisk(files) {
    
    const sorted = files.sort(function(a,b) {
        return a.inode - b.inode;
    });
    console.log(sorted);
    sorted.forEach(f => {
        for (var i=0;i<f.size;i++) {
            if (f.free) {
                process.stdout.write(".");
            } else {
                process.stdout.write(f.id.toString());
            }
        }
    });

    console.log();
}