
const fs = require('fs')

fs.readFile('day1.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  solve(data);
  solve2(data);
});

const frequency = ['E','T','A','O','I','N','S','R','H','D','L','U','C','M','F','W','Y','G','P','B','V','K','J','X','Q','Z'];
const alphabet = ['A','B','C']
function solve (data) {

    const dist = {};
    const message = data.split("-");

    var total=0;
    message.forEach(c => {
        if (!dist[c]) {
            dist[c] = 1;
        } else {
            dist[c]++;
        }
        total++;
    })
    const arr = [];
    Object.keys(dist).forEach(function(key,index) {
        //dist[key] = parseInt(dist[key])/total*100;
        arr.push([key,dist[key]])
    });
    arr.sort(function(a,b) { return b[1]-a[1]});

    console.log(arr);
    const map = {};
    for (var i=0; i<arr.length;i++) {
        map[arr[i][0]] = frequency[i];
    }
    
    
    
    console.log(arr);
    console.log(map);

    // decode
    var decoded = ""
    message.forEach(code => {
        decoded +=  String.fromCharCode(code%26+65);
    });
    console.log(decoded);
}

function solve2 (data) {

  const lines = data.split(/\r?\n/);
  
}

